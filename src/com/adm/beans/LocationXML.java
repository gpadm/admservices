package com.adm.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.adm.utils.LocationUtils;
import com.adm.webpart.webservices.BW_Location_Geo_Code.Location;
import com.fatwire.assetapi.data.AssetId;

public class LocationXML implements Comparable<LocationXML> {

	private Double distance;
	private String name;
	private String title;
	private String city;
	private String state;
	private String locationType;
	private String postalCode;
	private String addr;
	private String mailingAddr;
	private String phone;
	private Double latitude;
	private Double longitude;
	private AssetId locationPage;
	private String urlCaption;
	private List<String> commodities = new ArrayList<String>();
	
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getLocationType() {
		return locationType;
	}
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public String getMailingAddr() {
		return mailingAddr;
	}
	public void setMailingAddr(String mailingAddr) {
		this.mailingAddr = mailingAddr;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public AssetId getLocationPage() {
		return locationPage;
	}
	public void setLocationPage(AssetId locationPage) {
		this.locationPage = locationPage;
	}
	public String getUrlCaption() {
		return urlCaption;
	}
	public void setUrlCaption(String urlCaption) {
		this.urlCaption = urlCaption;
	}
	
	public List<String> getCommodities() {
		return commodities;
	}
	
	public void setCommodities(List<String> commodities) {
		this.commodities = commodities;
	}
	
	public static LocationXML buildLocationXML(Map<String, Object> loc, Location searched) {
		LocationXML xml = new LocationXML();

		xml.setName((String)loc.get("LocationName"));
		xml.setCity((String)loc.get("City"));
		xml.setState((String)loc.get("State"));
		xml.setLocationType((String)loc.get("LocationType"));
		xml.setPostalCode((String)loc.get("PostalCode"));
		xml.setAddr((String)loc.get("StreetAddress"));
		xml.setMailingAddr((String)loc.get("MailingAddress"));
		xml.setPhone((String)loc.get("Phone"));
		xml.setLatitude((Double)loc.get("Latitude"));
		xml.setLongitude((Double)loc.get("Longitude"));
		xml.setLocationPage((AssetId)loc.get("LocationPage"));
		xml.setUrlCaption((String)loc.get("UrlCaption"));
		
		double distance = LocationUtils.calculateDistance(
				xml.getLatitude(), xml.getLongitude(), 
				searched.getLatitude(), searched.getLongitude());
		xml.setDistance(distance);
		
		List<String> commodities = (List<String>)loc.get("Commodities");
		xml.setCommodities(commodities);
		return xml;		
	}
	
	@Override
	public int compareTo(LocationXML o) {
		return this.distance.compareTo(o.getDistance());
		
	}
}
