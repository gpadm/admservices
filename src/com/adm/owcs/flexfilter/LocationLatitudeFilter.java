/**
 * 
 */
package com.adm.owcs.flexfilter;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.adm.webpart.webservices.client.EGeoCodeClient;
import com.openmarket.basic.interfaces.AssetException;
import com.openmarket.basic.interfaces.IListBasic;
import com.openmarket.gator.flexfilters.AbstractFlexFilter;
import com.openmarket.gator.interfaces.IFilterEnvironment;
import com.openmarket.gator.interfaces.IFilterableAssetInstance;

import COM.FutureTense.Interfaces.FTValList;

/**
 * @author sameer.boddun
 *
 */
public class LocationLatitudeFilter extends AbstractFlexFilter {

	private static String[] ARG_CUSTOM = { "Input Zipcode Attribute", "Output Latitude Attribute" };
	private static final Log log = LogFactory.getLog("com.adm.owcs.flexfilter.LocationLatitudeFilter");
	private EGeoCodeClient wsClient = new EGeoCodeClient();
	
	public LocationLatitudeFilter(FTValList ftvallist) {
		super(ftvallist);
	}
	
	/* (non-Javadoc)
	 * @see com.openmarket.gator.flexfilters.AbstractFlexFilter#filterAsset(com.openmarket.gator.interfaces.IFilterEnvironment, java.lang.String, COM.FutureTense.Interfaces.FTValList, com.openmarket.gator.interfaces.IFilterableAssetInstance)
	 */
	@Override
	public void filterAsset(IFilterEnvironment env, String filterIdentifier, FTValList filterArguments, IFilterableAssetInstance thisAsset)
			throws AssetException {
		log.debug("filter Execution Started ");
		String zipcodeAttribute = getAttrID(env, filterArguments, ARG_CUSTOM[0]);
		log.debug("Zipcode Attribute id is " + zipcodeAttribute);
		if (StringUtils.isNotBlank(zipcodeAttribute)) {
			IListBasic zipcodeList = thisAsset.getAttribute(zipcodeAttribute);
			if (zipcodeList != null && zipcodeList.hasData()) {
				try {
					String zipcode = zipcodeList.getValue("value");
					log.debug("zipcode is: " + zipcode);
					
					
					Double latitude = wsClient.getLatiguteByPostalCode(zipcode);
					
					String latitudeAttribute = getAttrID(env, filterArguments, ARG_CUSTOM[1]);
					log.debug("Lattitude Attribute id is " + latitudeAttribute);

					if (StringUtils.isNotBlank(latitudeAttribute) && latitude != null) {
						thisAsset.addDerivedDataValue(filterIdentifier, latitudeAttribute, latitude.toString());
						log.debug("Dervied Attribute Set the value " + latitude + " from the zipCode : " + zipcode);
					} else {
						log.fatal("Output Attribute is null or Geo Code webservice not returing lattitude");
						throw new AssetException("Check validity of PostalCode");
					}

				} catch (NoSuchFieldException nsfe) {
					log.error("No SuchField  value in the list");
					throw new AssetException("Flex Filter not configured correctly, check if the attributes exists");
				}
			} else {
				log.fatal("Input Attribute Field is Empty Please check the date Field");
				throw new AssetException("PostalCode cannot be empty");
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.openmarket.gator.flexfilters.AbstractFlexFilter#getLegalArguments(com.openmarket.gator.interfaces.IFilterEnvironment, java.lang.String)
	 */
	@Override
	public FTValList getLegalArguments(IFilterEnvironment env, String filterIdentifier) throws AssetException {
		FTValList ftVal = new FTValList();
		log.debug("Retrieving Legel Arguments filterIdentifier" + filterIdentifier);
		for (int i = 0; i < ARG_CUSTOM.length; i++) {
			ftVal.setValString(ARG_CUSTOM[i], (ARG_CUSTOM[i]));
		}
		log.debug("Retrieving complete");
		return ftVal;
	}

}
