package com.adm.owcs.flexfilter;

import java.util.Calendar;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.openmarket.basic.interfaces.AssetException;
import com.openmarket.basic.interfaces.IListBasic;
import com.openmarket.gator.flexfilters.AbstractFlexFilter;
import com.openmarket.gator.interfaces.IFilterEnvironment;
import com.openmarket.gator.interfaces.IFilterableAssetInstance;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.Utilities;

public class GetYearFromDate extends AbstractFlexFilter {
	private static String[] ARG_CUSTOM = { "Input Date Attribute", "Output Year Attribute" };
	private static final Log log = LogFactory.getLog("com.adm.owcs.flexfilter.GetYearFromDate");

	public GetYearFromDate(FTValList ftvallist) {
		super(ftvallist);
	}

	public void filterAsset(IFilterEnvironment ife, String s, FTValList ftval, IFilterableAssetInstance thisasset)
			throws AssetException {
		log.debug("filter Execution Started ");
		String inputDatetattr = getAttrID(ife, ftval, ARG_CUSTOM[0]);
		log.debug("Input Date Attribute id is " + inputDatetattr);
		if (StringUtils.isNotBlank(inputDatetattr)) {
			IListBasic ilDateList = thisasset.getAttribute(inputDatetattr);
			if (ilDateList != null && ilDateList.hasData()) {
				try {
					String dateValue = ilDateList.getValue("value");
					log.debug("Date Value is " + dateValue);
					String year = getYearFromDate(dateValue);
					String outputAttribute = getAttrID(ife, ftval, ARG_CUSTOM[1]);
					log.debug("outputAttribute id is " + outputAttribute);

					if (StringUtils.isNotBlank(outputAttribute)) {
						thisasset.addDerivedDataValue(s, outputAttribute, year);
						log.debug("Dervied Attribute Set the value " + year + " from the Date : " + dateValue);
					}

				} catch (NoSuchFieldException nsfe) {
					log.error("No SuchField  value in the list");
				}
			} else {
				log.debug("Input Attribute Field is Empty Please check the date Field");
			}
		}
	}

	public String getYearFromDate(String strDate) {
		String year = "";
		if (StringUtils.isNotBlank(strDate)) {
			Calendar cal = Utilities.calendarFromJDBCString(strDate);
			log.debug("date here is " + cal);
			year = (new Integer(cal.get(Calendar.YEAR))).toString();
		}
		return year;
	}

	@Override
	public FTValList getLegalArguments(IFilterEnvironment ife, String s) throws AssetException {
		FTValList ftVal = new FTValList();
		for (int i = 0; i < ARG_CUSTOM.length; i++) {
			ftVal.setValString(ARG_CUSTOM[i], (ARG_CUSTOM[i]));
		}
		return ftVal;
	}
}