package com.adm.owcs.custom.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.adm.webpart.webservices.BW_Bids_Location_Info.LocationDocument;
import com.adm.webpart.webservices.client.EBidsLocationClient;
import com.adm.webpart.dao.TechnicalDocumentDAO;
import com.adm.webpart.vo.TechnicalDocumentationVO;

public class BinaryRenderer extends HttpServlet {

	private static final long serialVersionUID = -7104444970141988406L;
	EBidsLocationClient client = new EBidsLocationClient();
	
	private static Log log = LogFactory.getLog("com.adm.owcs.custom.servlet.BinaryRenderer");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String type = req.getParameter("t");
		
		if(type.equalsIgnoreCase("i")) {
			String staffId = req.getParameter("staffId");
			if(StringUtils.isNotEmpty(staffId)) {
				renderImage(resp, staffId);
			}
		} if(type.equalsIgnoreCase("p")) {
			String locId = req.getParameter("locId");
			String strDocId = req.getParameter("docId");
			if(StringUtils.isNotEmpty(locId) && StringUtils.isNotEmpty(strDocId)) {
				int docId = Integer.parseInt(req.getParameter("docId"));
				renderPdf(resp, locId, docId);
			}
		} if(type.equalsIgnoreCase("pdb")) {
			String src = req.getParameter("src");
			String strDocId = req.getParameter("docId");
			if(StringUtils.isNotEmpty(src) && StringUtils.isNotEmpty(strDocId)) {
				int docId = Integer.parseInt(req.getParameter("docId"));
				renderProductPdf(resp, src, docId);
			}
		}
		
	}
	
	private void renderProductPdf(HttpServletResponse resp, String src,
			int docId)  throws IOException {
		
		TechnicalDocumentDAO techDocDao = new TechnicalDocumentDAO();
		TechnicalDocumentationVO techdocvo = new TechnicalDocumentationVO();
		techdocvo =  techDocDao.getTechnicalDocument(docId, src);
		
		byte[] data = techdocvo.getPdfBlob();
		String fileName = techdocvo.getDocumentFileName().trim();
		log.debug("File Name->"+fileName +"|");
		resp.setHeader("Content-disposition", "attachment; filename=" + fileName);
		resp.setContentType("application/pdf");
		resp.setContentLength(data.length);
		ServletOutputStream sos = resp.getOutputStream();
		sos.write(data);		
		sos.flush();
		sos.close();
	}

	private void renderImage(HttpServletResponse resp, String staffId) throws IOException {
		byte[] imageData = client.getStaffImage(staffId);
		if(imageData != null) {
			resp.setContentType("image/jpeg");
			resp.setContentLength(imageData.length);
			ServletOutputStream sos = resp.getOutputStream();
			sos.write(imageData);
			sos.flush();
			sos.close();
		}		
	}

	private void renderPdf(HttpServletResponse resp, String locId, int docId) throws IOException {
		LocationDocument doc = client.getDocumentsByDocId(locId, docId);
		
		byte[] data = doc.getLD_FileData();
		resp.setHeader("Content-disposition", "inline; filename=\"" + doc.getLD_FileName() + "\"");
		resp.setContentType("application/pdf");
		resp.setContentLength(data.length);
		ServletOutputStream sos = resp.getOutputStream();
		sos.write(data);		
		sos.flush();
		sos.close();
	}
}
