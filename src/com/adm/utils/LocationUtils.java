package com.adm.utils;

import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LocationUtils {
	
	public static Boolean validate(String location) {
		String validationExpression = "^\\d{5}(?:[- ]\\d{4})?$|^[a-zA-Z]{1}\\d{1}[a-zA-Z]{1}[- ]?\\d{1}[a-zA-Z]{1}\\d{1}$|^[a-zA-Z\\s-]*,\\s*[a-zA-Z]{2}$"; 
		
		Pattern p = Pattern.compile(validationExpression);
		Matcher m = p.matcher(location);

		return m.matches();
	}
	
	public static Boolean isPostalCode(String location) {
		final String zipcodePattern="^\\d{5}(?:[- ]\\d{4})?$|^[a-zA-Z]{1}\\d{1}[a-zA-Z]{1}[- ]?\\d{1}[a-zA-Z]{1}\\d{1}$";
	 	Pattern pattern = Pattern.compile(zipcodePattern);
	 	Matcher matcher = pattern.matcher(location);
	 	return matcher.matches();
	}
	
	public static double calculateDistance(double AdmLat, double AdmLong, double SearchLat, double SearchLong) {
		double earthsphereradius = 6366.707019;
		double kmTomi = 0.621371;

		double AdmLatRadians = (AdmLat / 180) * Math.PI;
		double AdmLongRadians = (AdmLong / 180) * Math.PI;

		double SearchLatRadians = (SearchLat / 180) * Math.PI;
		double SearchLongRadians = (SearchLong / 180) * Math.PI;

		double dist = 2
				* Math.asin(Math.sqrt(Math.pow(Math.sin((AdmLatRadians - SearchLatRadians) / 2), 2)
						+ Math.cos(AdmLatRadians) * Math.cos(SearchLatRadians)
								* Math.pow(Math.sin((AdmLongRadians - SearchLongRadians) / 2), 2)))
				* (earthsphereradius * kmTomi);

		DecimalFormat f = new DecimalFormat("##.0000");
		String formattedValue = f.format(dist);

		return Double.parseDouble(formattedValue);
	}	
	
	public static void main(String args[]) {
		System.out.println("validate('36363')->" + validate("36363"));
		System.out.println("validate('36363-3733')->" + validate("36363-3733"));
		System.out.println("validate('36363 3733')->" + validate("36363 3733"));
		System.out.println("validate('363633733')->" + validate("363633733"));
		System.out.println("validate('F5T 5T6')->" + validate("F5T 5T6"));
		System.out.println("validate('F5T-5T6')->" + validate("F5T-5T6"));
		System.out.println("validate('F5T5T6')->" + validate("F5T5T6"));
		System.out.println("validate('F5G 6T')->" + validate("F5G 6T"));		
		System.out.println("validate('sjdk, jd')->" + validate("sjdk, jd"));		
		System.out.println("validate('sjkd,   dj')->" + validate("sjkd,   dj"));		
		System.out.println("validate('sjdk, jdd')->" + validate("sjdk, jdd"));		
		System.out.println("validate('sjdk-jd')->" + validate("sjdk-jd"));		
	}	
}
