package com.adm.migration.news;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;

import com.adm.migration.news.bean.NewsBean;
import com.fatwire.assetapi.data.AssetId;


public class CsvNewsReader {
	
	static String NEWS_DATA = "devnewsdata.csv";
	
	private static final Log log = LogFactory.getLog("com.adm.migration.news.CsvNewsReader");
	
	public List<NewsBean> read(String fileName) throws Exception {
		log.info("Starting to read: "+ fileName );
		List<NewsBean> newsList= new ArrayList<NewsBean>(632);

		String [] FILE_HEADER_MAPPING = {"id","cat","enddate","startdate"};
		CSVParser csvFileParser =  null;

		try {			
			CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(FILE_HEADER_MAPPING);
			csvFileParser = new CSVParser(getReader(fileName), csvFileFormat);
			List<CSVRecord> csvRecords = csvFileParser.getRecords();
			for (CSVRecord row : csvRecords) {
				newsList.add(getNewsData(row));
			} 
		} finally {
			if( csvFileParser != null)
				csvFileParser.close();
		}
		log.info("End File reading");
		return newsList;
	}
	
	private NewsBean getNewsData(CSVRecord row) throws Exception {
		NewsBean news = new NewsBean();
		
		news.id = Long.parseLong(row.get("id"));
		news.cat = row.get("cat");
		news.endDate = getDate(row.get("enddate"));
		news.startDate = getDate(row.get("startdate"));
		news.setAsset();
		log.debug("read news id: "+ news);
		return news;		
	}
	
	private Date getDate(String strDate) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		Date date = null;		
		if(StringUtils.isNotEmpty(strDate))
			date = sdf.parse(strDate);
		return date;
	}	
	
	private Reader getReader(String fileName) throws IOException {
		ClassLoader cl = this.getClass().getClassLoader();
		InputStream newsStream = cl.getResourceAsStream(fileName);
		
		return new BufferedReader(new InputStreamReader(newsStream));
	}
	
	public List<AssetId> getAssetList(List<NewsBean> newsList) {
		List<AssetId> assetList = new ArrayList<AssetId>(newsList.size());
		for(NewsBean news : newsList) {
			assetList.add(news.getAsset());
		}		
		return  assetList;		
	}
	
	public static void main(String args[]) throws Exception{
		CsvNewsReader newsReader = new CsvNewsReader();
		newsReader.read("devnewsdata.csv");		
	}
}
