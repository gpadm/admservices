package com.adm.migration.news.bean;

import java.util.Date;

import com.fatwire.assetapi.data.AssetId;
import com.openmarket.xcelerate.asset.AssetIdImpl;

public class NewsBean {	
	public Long id;
	public String cat;
	public Date startDate;
	public Date endDate;
	private AssetId asset;
	
	public void setAsset() {
		this.asset = new AssetIdImpl("ADMNewsAsset", id);
	}
	
	public AssetId getAsset() {
		return asset;
	}
	
	@Override
	public String toString() {
		return "NewsBean [id=" + id + ", cat=" + cat + ", startDate=" + startDate + ", endDate=" + endDate + "]\n";
	}	
}
