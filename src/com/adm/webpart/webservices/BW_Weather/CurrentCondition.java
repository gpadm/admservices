/**
 * CurrentCondition.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Weather;

public class CurrentCondition  implements java.io.Serializable {
    private java.lang.String displayName;

    private java.lang.String region;

    private java.lang.String country;

    private java.lang.String iconURL;

    private java.lang.String feelTemp;

    private java.lang.String currTemp;

    private java.lang.String highTemp;

    private java.lang.String lowTemp;

    private java.lang.String humidity;

    private java.lang.String windDirection;

    private java.lang.String windSpeed;

    private java.lang.String dewPoint;

    private java.lang.String pressure;

    private java.lang.String precipChance;

    private java.lang.String sunrise;

    private java.lang.String sunset;

    private java.lang.String weatherType;

    public CurrentCondition() {
    }

    public CurrentCondition(
           java.lang.String displayName,
           java.lang.String region,
           java.lang.String country,
           java.lang.String iconURL,
           java.lang.String feelTemp,
           java.lang.String currTemp,
           java.lang.String highTemp,
           java.lang.String lowTemp,
           java.lang.String humidity,
           java.lang.String windDirection,
           java.lang.String windSpeed,
           java.lang.String dewPoint,
           java.lang.String pressure,
           java.lang.String precipChance,
           java.lang.String sunrise,
           java.lang.String sunset,
           java.lang.String weatherType) {
           this.displayName = displayName;
           this.region = region;
           this.country = country;
           this.iconURL = iconURL;
           this.feelTemp = feelTemp;
           this.currTemp = currTemp;
           this.highTemp = highTemp;
           this.lowTemp = lowTemp;
           this.humidity = humidity;
           this.windDirection = windDirection;
           this.windSpeed = windSpeed;
           this.dewPoint = dewPoint;
           this.pressure = pressure;
           this.precipChance = precipChance;
           this.sunrise = sunrise;
           this.sunset = sunset;
           this.weatherType = weatherType;
    }


    /**
     * Gets the displayName value for this CurrentCondition.
     * 
     * @return displayName
     */
    public java.lang.String getDisplayName() {
        return displayName;
    }


    /**
     * Sets the displayName value for this CurrentCondition.
     * 
     * @param displayName
     */
    public void setDisplayName(java.lang.String displayName) {
        this.displayName = displayName;
    }


    /**
     * Gets the region value for this CurrentCondition.
     * 
     * @return region
     */
    public java.lang.String getRegion() {
        return region;
    }


    /**
     * Sets the region value for this CurrentCondition.
     * 
     * @param region
     */
    public void setRegion(java.lang.String region) {
        this.region = region;
    }


    /**
     * Gets the country value for this CurrentCondition.
     * 
     * @return country
     */
    public java.lang.String getCountry() {
        return country;
    }


    /**
     * Sets the country value for this CurrentCondition.
     * 
     * @param country
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }


    /**
     * Gets the iconURL value for this CurrentCondition.
     * 
     * @return iconURL
     */
    public java.lang.String getIconURL() {
        return iconURL;
    }


    /**
     * Sets the iconURL value for this CurrentCondition.
     * 
     * @param iconURL
     */
    public void setIconURL(java.lang.String iconURL) {
        this.iconURL = iconURL;
    }


    /**
     * Gets the feelTemp value for this CurrentCondition.
     * 
     * @return feelTemp
     */
    public java.lang.String getFeelTemp() {
        return feelTemp;
    }


    /**
     * Sets the feelTemp value for this CurrentCondition.
     * 
     * @param feelTemp
     */
    public void setFeelTemp(java.lang.String feelTemp) {
        this.feelTemp = feelTemp;
    }


    /**
     * Gets the currTemp value for this CurrentCondition.
     * 
     * @return currTemp
     */
    public java.lang.String getCurrTemp() {
        return currTemp;
    }


    /**
     * Sets the currTemp value for this CurrentCondition.
     * 
     * @param currTemp
     */
    public void setCurrTemp(java.lang.String currTemp) {
        this.currTemp = currTemp;
    }


    /**
     * Gets the highTemp value for this CurrentCondition.
     * 
     * @return highTemp
     */
    public java.lang.String getHighTemp() {
        return highTemp;
    }


    /**
     * Sets the highTemp value for this CurrentCondition.
     * 
     * @param highTemp
     */
    public void setHighTemp(java.lang.String highTemp) {
        this.highTemp = highTemp;
    }


    /**
     * Gets the lowTemp value for this CurrentCondition.
     * 
     * @return lowTemp
     */
    public java.lang.String getLowTemp() {
        return lowTemp;
    }


    /**
     * Sets the lowTemp value for this CurrentCondition.
     * 
     * @param lowTemp
     */
    public void setLowTemp(java.lang.String lowTemp) {
        this.lowTemp = lowTemp;
    }


    /**
     * Gets the humidity value for this CurrentCondition.
     * 
     * @return humidity
     */
    public java.lang.String getHumidity() {
        return humidity;
    }


    /**
     * Sets the humidity value for this CurrentCondition.
     * 
     * @param humidity
     */
    public void setHumidity(java.lang.String humidity) {
        this.humidity = humidity;
    }


    /**
     * Gets the windDirection value for this CurrentCondition.
     * 
     * @return windDirection
     */
    public java.lang.String getWindDirection() {
        return windDirection;
    }


    /**
     * Sets the windDirection value for this CurrentCondition.
     * 
     * @param windDirection
     */
    public void setWindDirection(java.lang.String windDirection) {
        this.windDirection = windDirection;
    }


    /**
     * Gets the windSpeed value for this CurrentCondition.
     * 
     * @return windSpeed
     */
    public java.lang.String getWindSpeed() {
        return windSpeed;
    }


    /**
     * Sets the windSpeed value for this CurrentCondition.
     * 
     * @param windSpeed
     */
    public void setWindSpeed(java.lang.String windSpeed) {
        this.windSpeed = windSpeed;
    }


    /**
     * Gets the dewPoint value for this CurrentCondition.
     * 
     * @return dewPoint
     */
    public java.lang.String getDewPoint() {
        return dewPoint;
    }


    /**
     * Sets the dewPoint value for this CurrentCondition.
     * 
     * @param dewPoint
     */
    public void setDewPoint(java.lang.String dewPoint) {
        this.dewPoint = dewPoint;
    }


    /**
     * Gets the pressure value for this CurrentCondition.
     * 
     * @return pressure
     */
    public java.lang.String getPressure() {
        return pressure;
    }


    /**
     * Sets the pressure value for this CurrentCondition.
     * 
     * @param pressure
     */
    public void setPressure(java.lang.String pressure) {
        this.pressure = pressure;
    }


    /**
     * Gets the precipChance value for this CurrentCondition.
     * 
     * @return precipChance
     */
    public java.lang.String getPrecipChance() {
        return precipChance;
    }


    /**
     * Sets the precipChance value for this CurrentCondition.
     * 
     * @param precipChance
     */
    public void setPrecipChance(java.lang.String precipChance) {
        this.precipChance = precipChance;
    }


    /**
     * Gets the sunrise value for this CurrentCondition.
     * 
     * @return sunrise
     */
    public java.lang.String getSunrise() {
        return sunrise;
    }


    /**
     * Sets the sunrise value for this CurrentCondition.
     * 
     * @param sunrise
     */
    public void setSunrise(java.lang.String sunrise) {
        this.sunrise = sunrise;
    }


    /**
     * Gets the sunset value for this CurrentCondition.
     * 
     * @return sunset
     */
    public java.lang.String getSunset() {
        return sunset;
    }


    /**
     * Sets the sunset value for this CurrentCondition.
     * 
     * @param sunset
     */
    public void setSunset(java.lang.String sunset) {
        this.sunset = sunset;
    }


    /**
     * Gets the weatherType value for this CurrentCondition.
     * 
     * @return weatherType
     */
    public java.lang.String getWeatherType() {
        return weatherType;
    }


    /**
     * Sets the weatherType value for this CurrentCondition.
     * 
     * @param weatherType
     */
    public void setWeatherType(java.lang.String weatherType) {
        this.weatherType = weatherType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CurrentCondition)) return false;
        CurrentCondition other = (CurrentCondition) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.displayName==null && other.getDisplayName()==null) || 
             (this.displayName!=null &&
              this.displayName.equals(other.getDisplayName()))) &&
            ((this.region==null && other.getRegion()==null) || 
             (this.region!=null &&
              this.region.equals(other.getRegion()))) &&
            ((this.country==null && other.getCountry()==null) || 
             (this.country!=null &&
              this.country.equals(other.getCountry()))) &&
            ((this.iconURL==null && other.getIconURL()==null) || 
             (this.iconURL!=null &&
              this.iconURL.equals(other.getIconURL()))) &&
            ((this.feelTemp==null && other.getFeelTemp()==null) || 
             (this.feelTemp!=null &&
              this.feelTemp.equals(other.getFeelTemp()))) &&
            ((this.currTemp==null && other.getCurrTemp()==null) || 
             (this.currTemp!=null &&
              this.currTemp.equals(other.getCurrTemp()))) &&
            ((this.highTemp==null && other.getHighTemp()==null) || 
             (this.highTemp!=null &&
              this.highTemp.equals(other.getHighTemp()))) &&
            ((this.lowTemp==null && other.getLowTemp()==null) || 
             (this.lowTemp!=null &&
              this.lowTemp.equals(other.getLowTemp()))) &&
            ((this.humidity==null && other.getHumidity()==null) || 
             (this.humidity!=null &&
              this.humidity.equals(other.getHumidity()))) &&
            ((this.windDirection==null && other.getWindDirection()==null) || 
             (this.windDirection!=null &&
              this.windDirection.equals(other.getWindDirection()))) &&
            ((this.windSpeed==null && other.getWindSpeed()==null) || 
             (this.windSpeed!=null &&
              this.windSpeed.equals(other.getWindSpeed()))) &&
            ((this.dewPoint==null && other.getDewPoint()==null) || 
             (this.dewPoint!=null &&
              this.dewPoint.equals(other.getDewPoint()))) &&
            ((this.pressure==null && other.getPressure()==null) || 
             (this.pressure!=null &&
              this.pressure.equals(other.getPressure()))) &&
            ((this.precipChance==null && other.getPrecipChance()==null) || 
             (this.precipChance!=null &&
              this.precipChance.equals(other.getPrecipChance()))) &&
            ((this.sunrise==null && other.getSunrise()==null) || 
             (this.sunrise!=null &&
              this.sunrise.equals(other.getSunrise()))) &&
            ((this.sunset==null && other.getSunset()==null) || 
             (this.sunset!=null &&
              this.sunset.equals(other.getSunset()))) &&
            ((this.weatherType==null && other.getWeatherType()==null) || 
             (this.weatherType!=null &&
              this.weatherType.equals(other.getWeatherType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDisplayName() != null) {
            _hashCode += getDisplayName().hashCode();
        }
        if (getRegion() != null) {
            _hashCode += getRegion().hashCode();
        }
        if (getCountry() != null) {
            _hashCode += getCountry().hashCode();
        }
        if (getIconURL() != null) {
            _hashCode += getIconURL().hashCode();
        }
        if (getFeelTemp() != null) {
            _hashCode += getFeelTemp().hashCode();
        }
        if (getCurrTemp() != null) {
            _hashCode += getCurrTemp().hashCode();
        }
        if (getHighTemp() != null) {
            _hashCode += getHighTemp().hashCode();
        }
        if (getLowTemp() != null) {
            _hashCode += getLowTemp().hashCode();
        }
        if (getHumidity() != null) {
            _hashCode += getHumidity().hashCode();
        }
        if (getWindDirection() != null) {
            _hashCode += getWindDirection().hashCode();
        }
        if (getWindSpeed() != null) {
            _hashCode += getWindSpeed().hashCode();
        }
        if (getDewPoint() != null) {
            _hashCode += getDewPoint().hashCode();
        }
        if (getPressure() != null) {
            _hashCode += getPressure().hashCode();
        }
        if (getPrecipChance() != null) {
            _hashCode += getPrecipChance().hashCode();
        }
        if (getSunrise() != null) {
            _hashCode += getSunrise().hashCode();
        }
        if (getSunset() != null) {
            _hashCode += getSunset().hashCode();
        }
        if (getWeatherType() != null) {
            _hashCode += getWeatherType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CurrentCondition.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "CurrentCondition"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "displayName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("region");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "region"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "country"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iconURL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "iconURL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feelTemp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "feelTemp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currTemp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "currTemp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("highTemp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "highTemp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lowTemp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "lowTemp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("humidity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "humidity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("windDirection");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "windDirection"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("windSpeed");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "windSpeed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dewPoint");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "dewPoint"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pressure");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "pressure"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("precipChance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "precipChance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sunrise");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "sunrise"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sunset");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "sunset"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("weatherType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "weatherType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
