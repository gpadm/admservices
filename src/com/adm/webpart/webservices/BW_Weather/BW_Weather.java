/**
 * BW_Weather.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Weather;

public interface BW_Weather extends javax.xml.rpc.Service {
    public java.lang.String getBW_WeatherSoapAddress();

    public com.adm.webpart.webservices.BW_Weather.BW_WeatherSoap getBW_WeatherSoap() throws javax.xml.rpc.ServiceException;

    public com.adm.webpart.webservices.BW_Weather.BW_WeatherSoap getBW_WeatherSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
