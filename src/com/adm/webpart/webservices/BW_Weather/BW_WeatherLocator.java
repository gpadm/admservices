/**
 * BW_WeatherLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Weather;

public class BW_WeatherLocator extends org.apache.axis.client.Service implements com.adm.webpart.webservices.BW_Weather.BW_Weather {

    public BW_WeatherLocator() {
    }


    public BW_WeatherLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BW_WeatherLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BW_WeatherSoap
    private java.lang.String BW_WeatherSoap_address = "http://webservices.adm.com/BW_Weather/BW_Weather.asmx";

    public java.lang.String getBW_WeatherSoapAddress() {
        return BW_WeatherSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BW_WeatherSoapWSDDServiceName = "BW_WeatherSoap";

    public java.lang.String getBW_WeatherSoapWSDDServiceName() {
        return BW_WeatherSoapWSDDServiceName;
    }

    public void setBW_WeatherSoapWSDDServiceName(java.lang.String name) {
        BW_WeatherSoapWSDDServiceName = name;
    }

    public com.adm.webpart.webservices.BW_Weather.BW_WeatherSoap getBW_WeatherSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BW_WeatherSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBW_WeatherSoap(endpoint);
    }

    public com.adm.webpart.webservices.BW_Weather.BW_WeatherSoap getBW_WeatherSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.adm.webpart.webservices.BW_Weather.BW_WeatherSoapStub _stub = new com.adm.webpart.webservices.BW_Weather.BW_WeatherSoapStub(portAddress, this);
            _stub.setPortName(getBW_WeatherSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBW_WeatherSoapEndpointAddress(java.lang.String address) {
        BW_WeatherSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.adm.webpart.webservices.BW_Weather.BW_WeatherSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.adm.webpart.webservices.BW_Weather.BW_WeatherSoapStub _stub = new com.adm.webpart.webservices.BW_Weather.BW_WeatherSoapStub(new java.net.URL(BW_WeatherSoap_address), this);
                _stub.setPortName(getBW_WeatherSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BW_WeatherSoap".equals(inputPortName)) {
            return getBW_WeatherSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "BW_Weather");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "BW_WeatherSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BW_WeatherSoap".equals(portName)) {
            setBW_WeatherSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
