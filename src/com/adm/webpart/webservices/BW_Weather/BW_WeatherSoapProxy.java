package com.adm.webpart.webservices.BW_Weather;

public class BW_WeatherSoapProxy implements com.adm.webpart.webservices.BW_Weather.BW_WeatherSoap {
  private String _endpoint = null;
  private com.adm.webpart.webservices.BW_Weather.BW_WeatherSoap bW_WeatherSoap = null;
  
  public BW_WeatherSoapProxy() {
    _initBW_WeatherSoapProxy();
  }
  
  public BW_WeatherSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initBW_WeatherSoapProxy();
  }
  
  private void _initBW_WeatherSoapProxy() {
    try {
      bW_WeatherSoap = (new com.adm.webpart.webservices.BW_Weather.BW_WeatherLocator()).getBW_WeatherSoap();
      if (bW_WeatherSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bW_WeatherSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bW_WeatherSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bW_WeatherSoap != null)
      ((javax.xml.rpc.Stub)bW_WeatherSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.adm.webpart.webservices.BW_Weather.BW_WeatherSoap getBW_WeatherSoap() {
    if (bW_WeatherSoap == null)
      _initBW_WeatherSoapProxy();
    return bW_WeatherSoap;
  }
  
  public java.lang.String getRadarMapURL(java.lang.String location, int zoomLevel, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_WeatherSoap == null)
      _initBW_WeatherSoapProxy();
    return bW_WeatherSoap.getRadarMapURL(location, zoomLevel, subscriptionID);
  }
  
  public java.lang.String getRadarMapURLByLatitudeLongitude(java.lang.String latitude, java.lang.String longitude, int zoomLevel, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_WeatherSoap == null)
      _initBW_WeatherSoapProxy();
    return bW_WeatherSoap.getRadarMapURLByLatitudeLongitude(latitude, longitude, zoomLevel, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Weather.CurrentCondition getCurrentConditions(java.lang.String location, java.lang.String tempUnit, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_WeatherSoap == null)
      _initBW_WeatherSoapProxy();
    return bW_WeatherSoap.getCurrentConditions(location, tempUnit, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Weather.ForecastDaySummary[] getForecastSummary(java.lang.String location, java.lang.String tempUnit, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_WeatherSoap == null)
      _initBW_WeatherSoapProxy();
    return bW_WeatherSoap.getForecastSummary(location, tempUnit, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Weather.ForecastDayExtended[] getExtendedForecast(java.lang.String location, java.lang.String tempUnit, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_WeatherSoap == null)
      _initBW_WeatherSoapProxy();
    return bW_WeatherSoap.getExtendedForecast(location, tempUnit, subscriptionID);
  }
  
  public boolean isLocationValid(java.lang.String location, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_WeatherSoap == null)
      _initBW_WeatherSoapProxy();
    return bW_WeatherSoap.isLocationValid(location, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Weather.ForecastDayExtended[] getFiveDayForecast(java.lang.String location, java.lang.String tempUnit, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_WeatherSoap == null)
      _initBW_WeatherSoapProxy();
    return bW_WeatherSoap.getFiveDayForecast(location, tempUnit, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Weather.ForecastDayExtended[] getFiveDayForecastByLatitudeLongitude(java.lang.String latitude, java.lang.String longitude, java.lang.String tempUnit, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_WeatherSoap == null)
      _initBW_WeatherSoapProxy();
    return bW_WeatherSoap.getFiveDayForecastByLatitudeLongitude(latitude, longitude, tempUnit, subscriptionID);
  }
  
  
}