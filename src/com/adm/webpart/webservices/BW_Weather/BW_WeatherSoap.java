/**
 * BW_WeatherSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Weather;

public interface BW_WeatherSoap extends java.rmi.Remote {

    /**
     * Valid zoomLevels are 0,1,2,3,4,5,6
     */
    public java.lang.String getRadarMapURL(java.lang.String location, int zoomLevel, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Valid zoomLevels are 0,1,2,3,4,5,6
     */
    public java.lang.String getRadarMapURLByLatitudeLongitude(java.lang.String latitude, java.lang.String longitude, int zoomLevel, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Valid tempUnits are 'C' and 'F'
     */
    public com.adm.webpart.webservices.BW_Weather.CurrentCondition getCurrentConditions(java.lang.String location, java.lang.String tempUnit, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Valid tempUnits are 'C' and 'F'
     */
    public com.adm.webpart.webservices.BW_Weather.ForecastDaySummary[] getForecastSummary(java.lang.String location, java.lang.String tempUnit, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Valid tempUnits are 'C' and 'F'
     */
    public com.adm.webpart.webservices.BW_Weather.ForecastDayExtended[] getExtendedForecast(java.lang.String location, java.lang.String tempUnit, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Check Location for validity
     */
    public boolean isLocationValid(java.lang.String location, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Valid tempUnits are 'C' and 'F'
     */
    public com.adm.webpart.webservices.BW_Weather.ForecastDayExtended[] getFiveDayForecast(java.lang.String location, java.lang.String tempUnit, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Valid tempUnits are 'C' and 'F'
     */
    public com.adm.webpart.webservices.BW_Weather.ForecastDayExtended[] getFiveDayForecastByLatitudeLongitude(java.lang.String latitude, java.lang.String longitude, java.lang.String tempUnit, java.lang.String subscriptionID) throws java.rmi.RemoteException;
}
