/**
 * ForecastDaySummary.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Weather;

public class ForecastDaySummary  implements java.io.Serializable {
    private int orderNumber;

    private java.lang.String country;

    private java.lang.String region;

    private java.lang.String displayName;

    private java.lang.String day;

    private java.lang.String date;

    private java.lang.String highTemp;

    private java.lang.String lowTemp;

    private java.lang.String iconURL;

    private java.lang.String weatherType;

    private java.lang.String feelHighTemp;

    private java.lang.String feelLowTemp;

    private java.lang.String dayKey;

    private java.lang.String mapKey;

    public ForecastDaySummary() {
    }

    public ForecastDaySummary(
           int orderNumber,
           java.lang.String country,
           java.lang.String region,
           java.lang.String displayName,
           java.lang.String day,
           java.lang.String date,
           java.lang.String highTemp,
           java.lang.String lowTemp,
           java.lang.String iconURL,
           java.lang.String weatherType,
           java.lang.String feelHighTemp,
           java.lang.String feelLowTemp,
           java.lang.String dayKey,
           java.lang.String mapKey) {
           this.orderNumber = orderNumber;
           this.country = country;
           this.region = region;
           this.displayName = displayName;
           this.day = day;
           this.date = date;
           this.highTemp = highTemp;
           this.lowTemp = lowTemp;
           this.iconURL = iconURL;
           this.weatherType = weatherType;
           this.feelHighTemp = feelHighTemp;
           this.feelLowTemp = feelLowTemp;
           this.dayKey = dayKey;
           this.mapKey = mapKey;
    }


    /**
     * Gets the orderNumber value for this ForecastDaySummary.
     * 
     * @return orderNumber
     */
    public int getOrderNumber() {
        return orderNumber;
    }


    /**
     * Sets the orderNumber value for this ForecastDaySummary.
     * 
     * @param orderNumber
     */
    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }


    /**
     * Gets the country value for this ForecastDaySummary.
     * 
     * @return country
     */
    public java.lang.String getCountry() {
        return country;
    }


    /**
     * Sets the country value for this ForecastDaySummary.
     * 
     * @param country
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }


    /**
     * Gets the region value for this ForecastDaySummary.
     * 
     * @return region
     */
    public java.lang.String getRegion() {
        return region;
    }


    /**
     * Sets the region value for this ForecastDaySummary.
     * 
     * @param region
     */
    public void setRegion(java.lang.String region) {
        this.region = region;
    }


    /**
     * Gets the displayName value for this ForecastDaySummary.
     * 
     * @return displayName
     */
    public java.lang.String getDisplayName() {
        return displayName;
    }


    /**
     * Sets the displayName value for this ForecastDaySummary.
     * 
     * @param displayName
     */
    public void setDisplayName(java.lang.String displayName) {
        this.displayName = displayName;
    }


    /**
     * Gets the day value for this ForecastDaySummary.
     * 
     * @return day
     */
    public java.lang.String getDay() {
        return day;
    }


    /**
     * Sets the day value for this ForecastDaySummary.
     * 
     * @param day
     */
    public void setDay(java.lang.String day) {
        this.day = day;
    }


    /**
     * Gets the date value for this ForecastDaySummary.
     * 
     * @return date
     */
    public java.lang.String getDate() {
        return date;
    }


    /**
     * Sets the date value for this ForecastDaySummary.
     * 
     * @param date
     */
    public void setDate(java.lang.String date) {
        this.date = date;
    }


    /**
     * Gets the highTemp value for this ForecastDaySummary.
     * 
     * @return highTemp
     */
    public java.lang.String getHighTemp() {
        return highTemp;
    }


    /**
     * Sets the highTemp value for this ForecastDaySummary.
     * 
     * @param highTemp
     */
    public void setHighTemp(java.lang.String highTemp) {
        this.highTemp = highTemp;
    }


    /**
     * Gets the lowTemp value for this ForecastDaySummary.
     * 
     * @return lowTemp
     */
    public java.lang.String getLowTemp() {
        return lowTemp;
    }


    /**
     * Sets the lowTemp value for this ForecastDaySummary.
     * 
     * @param lowTemp
     */
    public void setLowTemp(java.lang.String lowTemp) {
        this.lowTemp = lowTemp;
    }


    /**
     * Gets the iconURL value for this ForecastDaySummary.
     * 
     * @return iconURL
     */
    public java.lang.String getIconURL() {
        return iconURL;
    }


    /**
     * Sets the iconURL value for this ForecastDaySummary.
     * 
     * @param iconURL
     */
    public void setIconURL(java.lang.String iconURL) {
        this.iconURL = iconURL;
    }


    /**
     * Gets the weatherType value for this ForecastDaySummary.
     * 
     * @return weatherType
     */
    public java.lang.String getWeatherType() {
        return weatherType;
    }


    /**
     * Sets the weatherType value for this ForecastDaySummary.
     * 
     * @param weatherType
     */
    public void setWeatherType(java.lang.String weatherType) {
        this.weatherType = weatherType;
    }


    /**
     * Gets the feelHighTemp value for this ForecastDaySummary.
     * 
     * @return feelHighTemp
     */
    public java.lang.String getFeelHighTemp() {
        return feelHighTemp;
    }


    /**
     * Sets the feelHighTemp value for this ForecastDaySummary.
     * 
     * @param feelHighTemp
     */
    public void setFeelHighTemp(java.lang.String feelHighTemp) {
        this.feelHighTemp = feelHighTemp;
    }


    /**
     * Gets the feelLowTemp value for this ForecastDaySummary.
     * 
     * @return feelLowTemp
     */
    public java.lang.String getFeelLowTemp() {
        return feelLowTemp;
    }


    /**
     * Sets the feelLowTemp value for this ForecastDaySummary.
     * 
     * @param feelLowTemp
     */
    public void setFeelLowTemp(java.lang.String feelLowTemp) {
        this.feelLowTemp = feelLowTemp;
    }


    /**
     * Gets the dayKey value for this ForecastDaySummary.
     * 
     * @return dayKey
     */
    public java.lang.String getDayKey() {
        return dayKey;
    }


    /**
     * Sets the dayKey value for this ForecastDaySummary.
     * 
     * @param dayKey
     */
    public void setDayKey(java.lang.String dayKey) {
        this.dayKey = dayKey;
    }


    /**
     * Gets the mapKey value for this ForecastDaySummary.
     * 
     * @return mapKey
     */
    public java.lang.String getMapKey() {
        return mapKey;
    }


    /**
     * Sets the mapKey value for this ForecastDaySummary.
     * 
     * @param mapKey
     */
    public void setMapKey(java.lang.String mapKey) {
        this.mapKey = mapKey;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ForecastDaySummary)) return false;
        ForecastDaySummary other = (ForecastDaySummary) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.orderNumber == other.getOrderNumber() &&
            ((this.country==null && other.getCountry()==null) || 
             (this.country!=null &&
              this.country.equals(other.getCountry()))) &&
            ((this.region==null && other.getRegion()==null) || 
             (this.region!=null &&
              this.region.equals(other.getRegion()))) &&
            ((this.displayName==null && other.getDisplayName()==null) || 
             (this.displayName!=null &&
              this.displayName.equals(other.getDisplayName()))) &&
            ((this.day==null && other.getDay()==null) || 
             (this.day!=null &&
              this.day.equals(other.getDay()))) &&
            ((this.date==null && other.getDate()==null) || 
             (this.date!=null &&
              this.date.equals(other.getDate()))) &&
            ((this.highTemp==null && other.getHighTemp()==null) || 
             (this.highTemp!=null &&
              this.highTemp.equals(other.getHighTemp()))) &&
            ((this.lowTemp==null && other.getLowTemp()==null) || 
             (this.lowTemp!=null &&
              this.lowTemp.equals(other.getLowTemp()))) &&
            ((this.iconURL==null && other.getIconURL()==null) || 
             (this.iconURL!=null &&
              this.iconURL.equals(other.getIconURL()))) &&
            ((this.weatherType==null && other.getWeatherType()==null) || 
             (this.weatherType!=null &&
              this.weatherType.equals(other.getWeatherType()))) &&
            ((this.feelHighTemp==null && other.getFeelHighTemp()==null) || 
             (this.feelHighTemp!=null &&
              this.feelHighTemp.equals(other.getFeelHighTemp()))) &&
            ((this.feelLowTemp==null && other.getFeelLowTemp()==null) || 
             (this.feelLowTemp!=null &&
              this.feelLowTemp.equals(other.getFeelLowTemp()))) &&
            ((this.dayKey==null && other.getDayKey()==null) || 
             (this.dayKey!=null &&
              this.dayKey.equals(other.getDayKey()))) &&
            ((this.mapKey==null && other.getMapKey()==null) || 
             (this.mapKey!=null &&
              this.mapKey.equals(other.getMapKey())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getOrderNumber();
        if (getCountry() != null) {
            _hashCode += getCountry().hashCode();
        }
        if (getRegion() != null) {
            _hashCode += getRegion().hashCode();
        }
        if (getDisplayName() != null) {
            _hashCode += getDisplayName().hashCode();
        }
        if (getDay() != null) {
            _hashCode += getDay().hashCode();
        }
        if (getDate() != null) {
            _hashCode += getDate().hashCode();
        }
        if (getHighTemp() != null) {
            _hashCode += getHighTemp().hashCode();
        }
        if (getLowTemp() != null) {
            _hashCode += getLowTemp().hashCode();
        }
        if (getIconURL() != null) {
            _hashCode += getIconURL().hashCode();
        }
        if (getWeatherType() != null) {
            _hashCode += getWeatherType().hashCode();
        }
        if (getFeelHighTemp() != null) {
            _hashCode += getFeelHighTemp().hashCode();
        }
        if (getFeelLowTemp() != null) {
            _hashCode += getFeelLowTemp().hashCode();
        }
        if (getDayKey() != null) {
            _hashCode += getDayKey().hashCode();
        }
        if (getMapKey() != null) {
            _hashCode += getMapKey().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ForecastDaySummary.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "ForecastDaySummary"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "orderNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "country"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("region");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "region"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "displayName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("day");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "day"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("date");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("highTemp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "highTemp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lowTemp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "lowTemp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iconURL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "iconURL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("weatherType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "weatherType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feelHighTemp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "feelHighTemp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feelLowTemp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "feelLowTemp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dayKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "dayKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mapKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "mapKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
