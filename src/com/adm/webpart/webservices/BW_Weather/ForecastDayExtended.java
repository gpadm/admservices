/**
 * ForecastDayExtended.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Weather;

public class ForecastDayExtended  implements java.io.Serializable {
    private int orderNumber;

    private java.lang.String displayName;

    private java.lang.String day;

    private java.lang.String highTemp;

    private java.lang.String lowTemp;

    private java.lang.String iconURL;

    private java.lang.String date;

    private java.lang.String currentTemp;

    private java.lang.String feelTemp;

    private java.lang.String precipChance;

    private java.lang.String windSpeed;

    private java.lang.String humidity;

    private java.lang.String panEvaporation;

    private java.lang.String precipAmount;

    private java.lang.String growingDegreeDays50;

    private java.lang.String growingDegreeDays60;

    private java.lang.String sunrise;

    private java.lang.String sunset;

    private java.lang.String country;

    private java.lang.String region;

    private java.lang.String dewPoint;

    private java.lang.String evaporation;

    private java.lang.String pressure;

    private java.lang.String windDirection;

    private java.lang.String weatherType;

    private java.lang.String feelsLikeHigh;

    private java.lang.String feelsLikeLow;

    private java.lang.String postalCode;

    private boolean isToday;

    public ForecastDayExtended() {
    }

    public ForecastDayExtended(
           int orderNumber,
           java.lang.String displayName,
           java.lang.String day,
           java.lang.String highTemp,
           java.lang.String lowTemp,
           java.lang.String iconURL,
           java.lang.String date,
           java.lang.String currentTemp,
           java.lang.String feelTemp,
           java.lang.String precipChance,
           java.lang.String windSpeed,
           java.lang.String humidity,
           java.lang.String panEvaporation,
           java.lang.String precipAmount,
           java.lang.String growingDegreeDays50,
           java.lang.String growingDegreeDays60,
           java.lang.String sunrise,
           java.lang.String sunset,
           java.lang.String country,
           java.lang.String region,
           java.lang.String dewPoint,
           java.lang.String evaporation,
           java.lang.String pressure,
           java.lang.String windDirection,
           java.lang.String weatherType,
           java.lang.String feelsLikeHigh,
           java.lang.String feelsLikeLow,
           java.lang.String postalCode,
           boolean isToday) {
           this.orderNumber = orderNumber;
           this.displayName = displayName;
           this.day = day;
           this.highTemp = highTemp;
           this.lowTemp = lowTemp;
           this.iconURL = iconURL;
           this.date = date;
           this.currentTemp = currentTemp;
           this.feelTemp = feelTemp;
           this.precipChance = precipChance;
           this.windSpeed = windSpeed;
           this.humidity = humidity;
           this.panEvaporation = panEvaporation;
           this.precipAmount = precipAmount;
           this.growingDegreeDays50 = growingDegreeDays50;
           this.growingDegreeDays60 = growingDegreeDays60;
           this.sunrise = sunrise;
           this.sunset = sunset;
           this.country = country;
           this.region = region;
           this.dewPoint = dewPoint;
           this.evaporation = evaporation;
           this.pressure = pressure;
           this.windDirection = windDirection;
           this.weatherType = weatherType;
           this.feelsLikeHigh = feelsLikeHigh;
           this.feelsLikeLow = feelsLikeLow;
           this.postalCode = postalCode;
           this.isToday = isToday;
    }


    /**
     * Gets the orderNumber value for this ForecastDayExtended.
     * 
     * @return orderNumber
     */
    public int getOrderNumber() {
        return orderNumber;
    }


    /**
     * Sets the orderNumber value for this ForecastDayExtended.
     * 
     * @param orderNumber
     */
    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }


    /**
     * Gets the displayName value for this ForecastDayExtended.
     * 
     * @return displayName
     */
    public java.lang.String getDisplayName() {
        return displayName;
    }


    /**
     * Sets the displayName value for this ForecastDayExtended.
     * 
     * @param displayName
     */
    public void setDisplayName(java.lang.String displayName) {
        this.displayName = displayName;
    }


    /**
     * Gets the day value for this ForecastDayExtended.
     * 
     * @return day
     */
    public java.lang.String getDay() {
        return day;
    }


    /**
     * Sets the day value for this ForecastDayExtended.
     * 
     * @param day
     */
    public void setDay(java.lang.String day) {
        this.day = day;
    }


    /**
     * Gets the highTemp value for this ForecastDayExtended.
     * 
     * @return highTemp
     */
    public java.lang.String getHighTemp() {
        return highTemp;
    }


    /**
     * Sets the highTemp value for this ForecastDayExtended.
     * 
     * @param highTemp
     */
    public void setHighTemp(java.lang.String highTemp) {
        this.highTemp = highTemp;
    }


    /**
     * Gets the lowTemp value for this ForecastDayExtended.
     * 
     * @return lowTemp
     */
    public java.lang.String getLowTemp() {
        return lowTemp;
    }


    /**
     * Sets the lowTemp value for this ForecastDayExtended.
     * 
     * @param lowTemp
     */
    public void setLowTemp(java.lang.String lowTemp) {
        this.lowTemp = lowTemp;
    }


    /**
     * Gets the iconURL value for this ForecastDayExtended.
     * 
     * @return iconURL
     */
    public java.lang.String getIconURL() {
        return iconURL;
    }


    /**
     * Sets the iconURL value for this ForecastDayExtended.
     * 
     * @param iconURL
     */
    public void setIconURL(java.lang.String iconURL) {
        this.iconURL = iconURL;
    }


    /**
     * Gets the date value for this ForecastDayExtended.
     * 
     * @return date
     */
    public java.lang.String getDate() {
        return date;
    }


    /**
     * Sets the date value for this ForecastDayExtended.
     * 
     * @param date
     */
    public void setDate(java.lang.String date) {
        this.date = date;
    }


    /**
     * Gets the currentTemp value for this ForecastDayExtended.
     * 
     * @return currentTemp
     */
    public java.lang.String getCurrentTemp() {
        return currentTemp;
    }


    /**
     * Sets the currentTemp value for this ForecastDayExtended.
     * 
     * @param currentTemp
     */
    public void setCurrentTemp(java.lang.String currentTemp) {
        this.currentTemp = currentTemp;
    }


    /**
     * Gets the feelTemp value for this ForecastDayExtended.
     * 
     * @return feelTemp
     */
    public java.lang.String getFeelTemp() {
        return feelTemp;
    }


    /**
     * Sets the feelTemp value for this ForecastDayExtended.
     * 
     * @param feelTemp
     */
    public void setFeelTemp(java.lang.String feelTemp) {
        this.feelTemp = feelTemp;
    }


    /**
     * Gets the precipChance value for this ForecastDayExtended.
     * 
     * @return precipChance
     */
    public java.lang.String getPrecipChance() {
        return precipChance;
    }


    /**
     * Sets the precipChance value for this ForecastDayExtended.
     * 
     * @param precipChance
     */
    public void setPrecipChance(java.lang.String precipChance) {
        this.precipChance = precipChance;
    }


    /**
     * Gets the windSpeed value for this ForecastDayExtended.
     * 
     * @return windSpeed
     */
    public java.lang.String getWindSpeed() {
        return windSpeed;
    }


    /**
     * Sets the windSpeed value for this ForecastDayExtended.
     * 
     * @param windSpeed
     */
    public void setWindSpeed(java.lang.String windSpeed) {
        this.windSpeed = windSpeed;
    }


    /**
     * Gets the humidity value for this ForecastDayExtended.
     * 
     * @return humidity
     */
    public java.lang.String getHumidity() {
        return humidity;
    }


    /**
     * Sets the humidity value for this ForecastDayExtended.
     * 
     * @param humidity
     */
    public void setHumidity(java.lang.String humidity) {
        this.humidity = humidity;
    }


    /**
     * Gets the panEvaporation value for this ForecastDayExtended.
     * 
     * @return panEvaporation
     */
    public java.lang.String getPanEvaporation() {
        return panEvaporation;
    }


    /**
     * Sets the panEvaporation value for this ForecastDayExtended.
     * 
     * @param panEvaporation
     */
    public void setPanEvaporation(java.lang.String panEvaporation) {
        this.panEvaporation = panEvaporation;
    }


    /**
     * Gets the precipAmount value for this ForecastDayExtended.
     * 
     * @return precipAmount
     */
    public java.lang.String getPrecipAmount() {
        return precipAmount;
    }


    /**
     * Sets the precipAmount value for this ForecastDayExtended.
     * 
     * @param precipAmount
     */
    public void setPrecipAmount(java.lang.String precipAmount) {
        this.precipAmount = precipAmount;
    }


    /**
     * Gets the growingDegreeDays50 value for this ForecastDayExtended.
     * 
     * @return growingDegreeDays50
     */
    public java.lang.String getGrowingDegreeDays50() {
        return growingDegreeDays50;
    }


    /**
     * Sets the growingDegreeDays50 value for this ForecastDayExtended.
     * 
     * @param growingDegreeDays50
     */
    public void setGrowingDegreeDays50(java.lang.String growingDegreeDays50) {
        this.growingDegreeDays50 = growingDegreeDays50;
    }


    /**
     * Gets the growingDegreeDays60 value for this ForecastDayExtended.
     * 
     * @return growingDegreeDays60
     */
    public java.lang.String getGrowingDegreeDays60() {
        return growingDegreeDays60;
    }


    /**
     * Sets the growingDegreeDays60 value for this ForecastDayExtended.
     * 
     * @param growingDegreeDays60
     */
    public void setGrowingDegreeDays60(java.lang.String growingDegreeDays60) {
        this.growingDegreeDays60 = growingDegreeDays60;
    }


    /**
     * Gets the sunrise value for this ForecastDayExtended.
     * 
     * @return sunrise
     */
    public java.lang.String getSunrise() {
        return sunrise;
    }


    /**
     * Sets the sunrise value for this ForecastDayExtended.
     * 
     * @param sunrise
     */
    public void setSunrise(java.lang.String sunrise) {
        this.sunrise = sunrise;
    }


    /**
     * Gets the sunset value for this ForecastDayExtended.
     * 
     * @return sunset
     */
    public java.lang.String getSunset() {
        return sunset;
    }


    /**
     * Sets the sunset value for this ForecastDayExtended.
     * 
     * @param sunset
     */
    public void setSunset(java.lang.String sunset) {
        this.sunset = sunset;
    }


    /**
     * Gets the country value for this ForecastDayExtended.
     * 
     * @return country
     */
    public java.lang.String getCountry() {
        return country;
    }


    /**
     * Sets the country value for this ForecastDayExtended.
     * 
     * @param country
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }


    /**
     * Gets the region value for this ForecastDayExtended.
     * 
     * @return region
     */
    public java.lang.String getRegion() {
        return region;
    }


    /**
     * Sets the region value for this ForecastDayExtended.
     * 
     * @param region
     */
    public void setRegion(java.lang.String region) {
        this.region = region;
    }


    /**
     * Gets the dewPoint value for this ForecastDayExtended.
     * 
     * @return dewPoint
     */
    public java.lang.String getDewPoint() {
        return dewPoint;
    }


    /**
     * Sets the dewPoint value for this ForecastDayExtended.
     * 
     * @param dewPoint
     */
    public void setDewPoint(java.lang.String dewPoint) {
        this.dewPoint = dewPoint;
    }


    /**
     * Gets the evaporation value for this ForecastDayExtended.
     * 
     * @return evaporation
     */
    public java.lang.String getEvaporation() {
        return evaporation;
    }


    /**
     * Sets the evaporation value for this ForecastDayExtended.
     * 
     * @param evaporation
     */
    public void setEvaporation(java.lang.String evaporation) {
        this.evaporation = evaporation;
    }


    /**
     * Gets the pressure value for this ForecastDayExtended.
     * 
     * @return pressure
     */
    public java.lang.String getPressure() {
        return pressure;
    }


    /**
     * Sets the pressure value for this ForecastDayExtended.
     * 
     * @param pressure
     */
    public void setPressure(java.lang.String pressure) {
        this.pressure = pressure;
    }


    /**
     * Gets the windDirection value for this ForecastDayExtended.
     * 
     * @return windDirection
     */
    public java.lang.String getWindDirection() {
        return windDirection;
    }


    /**
     * Sets the windDirection value for this ForecastDayExtended.
     * 
     * @param windDirection
     */
    public void setWindDirection(java.lang.String windDirection) {
        this.windDirection = windDirection;
    }


    /**
     * Gets the weatherType value for this ForecastDayExtended.
     * 
     * @return weatherType
     */
    public java.lang.String getWeatherType() {
        return weatherType;
    }


    /**
     * Sets the weatherType value for this ForecastDayExtended.
     * 
     * @param weatherType
     */
    public void setWeatherType(java.lang.String weatherType) {
        this.weatherType = weatherType;
    }


    /**
     * Gets the feelsLikeHigh value for this ForecastDayExtended.
     * 
     * @return feelsLikeHigh
     */
    public java.lang.String getFeelsLikeHigh() {
        return feelsLikeHigh;
    }


    /**
     * Sets the feelsLikeHigh value for this ForecastDayExtended.
     * 
     * @param feelsLikeHigh
     */
    public void setFeelsLikeHigh(java.lang.String feelsLikeHigh) {
        this.feelsLikeHigh = feelsLikeHigh;
    }


    /**
     * Gets the feelsLikeLow value for this ForecastDayExtended.
     * 
     * @return feelsLikeLow
     */
    public java.lang.String getFeelsLikeLow() {
        return feelsLikeLow;
    }


    /**
     * Sets the feelsLikeLow value for this ForecastDayExtended.
     * 
     * @param feelsLikeLow
     */
    public void setFeelsLikeLow(java.lang.String feelsLikeLow) {
        this.feelsLikeLow = feelsLikeLow;
    }


    /**
     * Gets the postalCode value for this ForecastDayExtended.
     * 
     * @return postalCode
     */
    public java.lang.String getPostalCode() {
        return postalCode;
    }


    /**
     * Sets the postalCode value for this ForecastDayExtended.
     * 
     * @param postalCode
     */
    public void setPostalCode(java.lang.String postalCode) {
        this.postalCode = postalCode;
    }


    /**
     * Gets the isToday value for this ForecastDayExtended.
     * 
     * @return isToday
     */
    public boolean isIsToday() {
        return isToday;
    }


    /**
     * Sets the isToday value for this ForecastDayExtended.
     * 
     * @param isToday
     */
    public void setIsToday(boolean isToday) {
        this.isToday = isToday;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ForecastDayExtended)) return false;
        ForecastDayExtended other = (ForecastDayExtended) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.orderNumber == other.getOrderNumber() &&
            ((this.displayName==null && other.getDisplayName()==null) || 
             (this.displayName!=null &&
              this.displayName.equals(other.getDisplayName()))) &&
            ((this.day==null && other.getDay()==null) || 
             (this.day!=null &&
              this.day.equals(other.getDay()))) &&
            ((this.highTemp==null && other.getHighTemp()==null) || 
             (this.highTemp!=null &&
              this.highTemp.equals(other.getHighTemp()))) &&
            ((this.lowTemp==null && other.getLowTemp()==null) || 
             (this.lowTemp!=null &&
              this.lowTemp.equals(other.getLowTemp()))) &&
            ((this.iconURL==null && other.getIconURL()==null) || 
             (this.iconURL!=null &&
              this.iconURL.equals(other.getIconURL()))) &&
            ((this.date==null && other.getDate()==null) || 
             (this.date!=null &&
              this.date.equals(other.getDate()))) &&
            ((this.currentTemp==null && other.getCurrentTemp()==null) || 
             (this.currentTemp!=null &&
              this.currentTemp.equals(other.getCurrentTemp()))) &&
            ((this.feelTemp==null && other.getFeelTemp()==null) || 
             (this.feelTemp!=null &&
              this.feelTemp.equals(other.getFeelTemp()))) &&
            ((this.precipChance==null && other.getPrecipChance()==null) || 
             (this.precipChance!=null &&
              this.precipChance.equals(other.getPrecipChance()))) &&
            ((this.windSpeed==null && other.getWindSpeed()==null) || 
             (this.windSpeed!=null &&
              this.windSpeed.equals(other.getWindSpeed()))) &&
            ((this.humidity==null && other.getHumidity()==null) || 
             (this.humidity!=null &&
              this.humidity.equals(other.getHumidity()))) &&
            ((this.panEvaporation==null && other.getPanEvaporation()==null) || 
             (this.panEvaporation!=null &&
              this.panEvaporation.equals(other.getPanEvaporation()))) &&
            ((this.precipAmount==null && other.getPrecipAmount()==null) || 
             (this.precipAmount!=null &&
              this.precipAmount.equals(other.getPrecipAmount()))) &&
            ((this.growingDegreeDays50==null && other.getGrowingDegreeDays50()==null) || 
             (this.growingDegreeDays50!=null &&
              this.growingDegreeDays50.equals(other.getGrowingDegreeDays50()))) &&
            ((this.growingDegreeDays60==null && other.getGrowingDegreeDays60()==null) || 
             (this.growingDegreeDays60!=null &&
              this.growingDegreeDays60.equals(other.getGrowingDegreeDays60()))) &&
            ((this.sunrise==null && other.getSunrise()==null) || 
             (this.sunrise!=null &&
              this.sunrise.equals(other.getSunrise()))) &&
            ((this.sunset==null && other.getSunset()==null) || 
             (this.sunset!=null &&
              this.sunset.equals(other.getSunset()))) &&
            ((this.country==null && other.getCountry()==null) || 
             (this.country!=null &&
              this.country.equals(other.getCountry()))) &&
            ((this.region==null && other.getRegion()==null) || 
             (this.region!=null &&
              this.region.equals(other.getRegion()))) &&
            ((this.dewPoint==null && other.getDewPoint()==null) || 
             (this.dewPoint!=null &&
              this.dewPoint.equals(other.getDewPoint()))) &&
            ((this.evaporation==null && other.getEvaporation()==null) || 
             (this.evaporation!=null &&
              this.evaporation.equals(other.getEvaporation()))) &&
            ((this.pressure==null && other.getPressure()==null) || 
             (this.pressure!=null &&
              this.pressure.equals(other.getPressure()))) &&
            ((this.windDirection==null && other.getWindDirection()==null) || 
             (this.windDirection!=null &&
              this.windDirection.equals(other.getWindDirection()))) &&
            ((this.weatherType==null && other.getWeatherType()==null) || 
             (this.weatherType!=null &&
              this.weatherType.equals(other.getWeatherType()))) &&
            ((this.feelsLikeHigh==null && other.getFeelsLikeHigh()==null) || 
             (this.feelsLikeHigh!=null &&
              this.feelsLikeHigh.equals(other.getFeelsLikeHigh()))) &&
            ((this.feelsLikeLow==null && other.getFeelsLikeLow()==null) || 
             (this.feelsLikeLow!=null &&
              this.feelsLikeLow.equals(other.getFeelsLikeLow()))) &&
            ((this.postalCode==null && other.getPostalCode()==null) || 
             (this.postalCode!=null &&
              this.postalCode.equals(other.getPostalCode()))) &&
            this.isToday == other.isIsToday();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getOrderNumber();
        if (getDisplayName() != null) {
            _hashCode += getDisplayName().hashCode();
        }
        if (getDay() != null) {
            _hashCode += getDay().hashCode();
        }
        if (getHighTemp() != null) {
            _hashCode += getHighTemp().hashCode();
        }
        if (getLowTemp() != null) {
            _hashCode += getLowTemp().hashCode();
        }
        if (getIconURL() != null) {
            _hashCode += getIconURL().hashCode();
        }
        if (getDate() != null) {
            _hashCode += getDate().hashCode();
        }
        if (getCurrentTemp() != null) {
            _hashCode += getCurrentTemp().hashCode();
        }
        if (getFeelTemp() != null) {
            _hashCode += getFeelTemp().hashCode();
        }
        if (getPrecipChance() != null) {
            _hashCode += getPrecipChance().hashCode();
        }
        if (getWindSpeed() != null) {
            _hashCode += getWindSpeed().hashCode();
        }
        if (getHumidity() != null) {
            _hashCode += getHumidity().hashCode();
        }
        if (getPanEvaporation() != null) {
            _hashCode += getPanEvaporation().hashCode();
        }
        if (getPrecipAmount() != null) {
            _hashCode += getPrecipAmount().hashCode();
        }
        if (getGrowingDegreeDays50() != null) {
            _hashCode += getGrowingDegreeDays50().hashCode();
        }
        if (getGrowingDegreeDays60() != null) {
            _hashCode += getGrowingDegreeDays60().hashCode();
        }
        if (getSunrise() != null) {
            _hashCode += getSunrise().hashCode();
        }
        if (getSunset() != null) {
            _hashCode += getSunset().hashCode();
        }
        if (getCountry() != null) {
            _hashCode += getCountry().hashCode();
        }
        if (getRegion() != null) {
            _hashCode += getRegion().hashCode();
        }
        if (getDewPoint() != null) {
            _hashCode += getDewPoint().hashCode();
        }
        if (getEvaporation() != null) {
            _hashCode += getEvaporation().hashCode();
        }
        if (getPressure() != null) {
            _hashCode += getPressure().hashCode();
        }
        if (getWindDirection() != null) {
            _hashCode += getWindDirection().hashCode();
        }
        if (getWeatherType() != null) {
            _hashCode += getWeatherType().hashCode();
        }
        if (getFeelsLikeHigh() != null) {
            _hashCode += getFeelsLikeHigh().hashCode();
        }
        if (getFeelsLikeLow() != null) {
            _hashCode += getFeelsLikeLow().hashCode();
        }
        if (getPostalCode() != null) {
            _hashCode += getPostalCode().hashCode();
        }
        _hashCode += (isIsToday() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ForecastDayExtended.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "ForecastDayExtended"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "orderNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "displayName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("day");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "day"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("highTemp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "highTemp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lowTemp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "lowTemp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iconURL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "iconURL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("date");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currentTemp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "currentTemp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feelTemp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "feelTemp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("precipChance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "precipChance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("windSpeed");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "windSpeed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("humidity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "humidity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("panEvaporation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "panEvaporation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("precipAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "precipAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("growingDegreeDays50");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "growingDegreeDays50"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("growingDegreeDays60");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "growingDegreeDays60"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sunrise");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "sunrise"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sunset");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "sunset"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "country"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("region");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "region"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dewPoint");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "dewPoint"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("evaporation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "evaporation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pressure");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "pressure"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("windDirection");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "windDirection"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("weatherType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "weatherType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feelsLikeHigh");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "feelsLikeHigh"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feelsLikeLow");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "feelsLikeLow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "postalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isToday");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Weather", "isToday"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
