/**
 * MarketIntell.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Market_Intelligence;

public class MarketIntell  implements java.io.Serializable {
    private int commentaryID;

    private java.lang.String articleTitle;

    private java.lang.String detailText;

    private java.lang.String shortPreviewText;

    private java.lang.String longPreviewText;

    private java.lang.String PDFLink;

    private int PDFSize;

    private java.lang.String PDFStartText;

    private java.util.Calendar fileDate;

    private int ID;

    private byte[] audioFile;

    private int audioSortIndex;

    private int audioSize;

    private java.lang.String videoURL;

    private int videoSortIndex;

    private com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image[] commentary_Images;

    private com.adm.webpart.webservices.BW_Market_Intelligence.MI_Additional_Text[] commentary_Texts;

    private com.adm.webpart.webservices.BW_Market_Intelligence.ControlType previewControl;

    private com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image firstImage;

    private com.adm.webpart.webservices.BW_Market_Intelligence.MI_Additional_Text firstAdditionalText;

    private java.lang.String videoEmbedCode;

    private java.lang.String videoIFrameEmbedCode;

    private int controlCount;

    private boolean hasDetail;

    public MarketIntell() {
    }

    public MarketIntell(
           int commentaryID,
           java.lang.String articleTitle,
           java.lang.String detailText,
           java.lang.String shortPreviewText,
           java.lang.String longPreviewText,
           java.lang.String PDFLink,
           int PDFSize,
           java.lang.String PDFStartText,
           java.util.Calendar fileDate,
           int ID,
           byte[] audioFile,
           int audioSortIndex,
           int audioSize,
           java.lang.String videoURL,
           int videoSortIndex,
           com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image[] commentary_Images,
           com.adm.webpart.webservices.BW_Market_Intelligence.MI_Additional_Text[] commentary_Texts,
           com.adm.webpart.webservices.BW_Market_Intelligence.ControlType previewControl,
           com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image firstImage,
           com.adm.webpart.webservices.BW_Market_Intelligence.MI_Additional_Text firstAdditionalText,
           java.lang.String videoEmbedCode,
           java.lang.String videoIFrameEmbedCode,
           int controlCount,
           boolean hasDetail) {
           this.commentaryID = commentaryID;
           this.articleTitle = articleTitle;
           this.detailText = detailText;
           this.shortPreviewText = shortPreviewText;
           this.longPreviewText = longPreviewText;
           this.PDFLink = PDFLink;
           this.PDFSize = PDFSize;
           this.PDFStartText = PDFStartText;
           this.fileDate = fileDate;
           this.ID = ID;
           this.audioFile = audioFile;
           this.audioSortIndex = audioSortIndex;
           this.audioSize = audioSize;
           this.videoURL = videoURL;
           this.videoSortIndex = videoSortIndex;
           this.commentary_Images = commentary_Images;
           this.commentary_Texts = commentary_Texts;
           this.previewControl = previewControl;
           this.firstImage = firstImage;
           this.firstAdditionalText = firstAdditionalText;
           this.videoEmbedCode = videoEmbedCode;
           this.videoIFrameEmbedCode = videoIFrameEmbedCode;
           this.controlCount = controlCount;
           this.hasDetail = hasDetail;
    }


    /**
     * Gets the commentaryID value for this MarketIntell.
     * 
     * @return commentaryID
     */
    public int getCommentaryID() {
        return commentaryID;
    }


    /**
     * Sets the commentaryID value for this MarketIntell.
     * 
     * @param commentaryID
     */
    public void setCommentaryID(int commentaryID) {
        this.commentaryID = commentaryID;
    }


    /**
     * Gets the articleTitle value for this MarketIntell.
     * 
     * @return articleTitle
     */
    public java.lang.String getArticleTitle() {
        return articleTitle;
    }


    /**
     * Sets the articleTitle value for this MarketIntell.
     * 
     * @param articleTitle
     */
    public void setArticleTitle(java.lang.String articleTitle) {
        this.articleTitle = articleTitle;
    }


    /**
     * Gets the detailText value for this MarketIntell.
     * 
     * @return detailText
     */
    public java.lang.String getDetailText() {
        return detailText;
    }


    /**
     * Sets the detailText value for this MarketIntell.
     * 
     * @param detailText
     */
    public void setDetailText(java.lang.String detailText) {
        this.detailText = detailText;
    }


    /**
     * Gets the shortPreviewText value for this MarketIntell.
     * 
     * @return shortPreviewText
     */
    public java.lang.String getShortPreviewText() {
        return shortPreviewText;
    }


    /**
     * Sets the shortPreviewText value for this MarketIntell.
     * 
     * @param shortPreviewText
     */
    public void setShortPreviewText(java.lang.String shortPreviewText) {
        this.shortPreviewText = shortPreviewText;
    }


    /**
     * Gets the longPreviewText value for this MarketIntell.
     * 
     * @return longPreviewText
     */
    public java.lang.String getLongPreviewText() {
        return longPreviewText;
    }


    /**
     * Sets the longPreviewText value for this MarketIntell.
     * 
     * @param longPreviewText
     */
    public void setLongPreviewText(java.lang.String longPreviewText) {
        this.longPreviewText = longPreviewText;
    }


    /**
     * Gets the PDFLink value for this MarketIntell.
     * 
     * @return PDFLink
     */
    public java.lang.String getPDFLink() {
        return PDFLink;
    }


    /**
     * Sets the PDFLink value for this MarketIntell.
     * 
     * @param PDFLink
     */
    public void setPDFLink(java.lang.String PDFLink) {
        this.PDFLink = PDFLink;
    }


    /**
     * Gets the PDFSize value for this MarketIntell.
     * 
     * @return PDFSize
     */
    public int getPDFSize() {
        return PDFSize;
    }


    /**
     * Sets the PDFSize value for this MarketIntell.
     * 
     * @param PDFSize
     */
    public void setPDFSize(int PDFSize) {
        this.PDFSize = PDFSize;
    }


    /**
     * Gets the PDFStartText value for this MarketIntell.
     * 
     * @return PDFStartText
     */
    public java.lang.String getPDFStartText() {
        return PDFStartText;
    }


    /**
     * Sets the PDFStartText value for this MarketIntell.
     * 
     * @param PDFStartText
     */
    public void setPDFStartText(java.lang.String PDFStartText) {
        this.PDFStartText = PDFStartText;
    }


    /**
     * Gets the fileDate value for this MarketIntell.
     * 
     * @return fileDate
     */
    public java.util.Calendar getFileDate() {
        return fileDate;
    }


    /**
     * Sets the fileDate value for this MarketIntell.
     * 
     * @param fileDate
     */
    public void setFileDate(java.util.Calendar fileDate) {
        this.fileDate = fileDate;
    }


    /**
     * Gets the ID value for this MarketIntell.
     * 
     * @return ID
     */
    public int getID() {
        return ID;
    }


    /**
     * Sets the ID value for this MarketIntell.
     * 
     * @param ID
     */
    public void setID(int ID) {
        this.ID = ID;
    }


    /**
     * Gets the audioFile value for this MarketIntell.
     * 
     * @return audioFile
     */
    public byte[] getAudioFile() {
        return audioFile;
    }


    /**
     * Sets the audioFile value for this MarketIntell.
     * 
     * @param audioFile
     */
    public void setAudioFile(byte[] audioFile) {
        this.audioFile = audioFile;
    }


    /**
     * Gets the audioSortIndex value for this MarketIntell.
     * 
     * @return audioSortIndex
     */
    public int getAudioSortIndex() {
        return audioSortIndex;
    }


    /**
     * Sets the audioSortIndex value for this MarketIntell.
     * 
     * @param audioSortIndex
     */
    public void setAudioSortIndex(int audioSortIndex) {
        this.audioSortIndex = audioSortIndex;
    }


    /**
     * Gets the audioSize value for this MarketIntell.
     * 
     * @return audioSize
     */
    public int getAudioSize() {
        return audioSize;
    }


    /**
     * Sets the audioSize value for this MarketIntell.
     * 
     * @param audioSize
     */
    public void setAudioSize(int audioSize) {
        this.audioSize = audioSize;
    }


    /**
     * Gets the videoURL value for this MarketIntell.
     * 
     * @return videoURL
     */
    public java.lang.String getVideoURL() {
        return videoURL;
    }


    /**
     * Sets the videoURL value for this MarketIntell.
     * 
     * @param videoURL
     */
    public void setVideoURL(java.lang.String videoURL) {
        this.videoURL = videoURL;
    }


    /**
     * Gets the videoSortIndex value for this MarketIntell.
     * 
     * @return videoSortIndex
     */
    public int getVideoSortIndex() {
        return videoSortIndex;
    }


    /**
     * Sets the videoSortIndex value for this MarketIntell.
     * 
     * @param videoSortIndex
     */
    public void setVideoSortIndex(int videoSortIndex) {
        this.videoSortIndex = videoSortIndex;
    }


    /**
     * Gets the commentary_Images value for this MarketIntell.
     * 
     * @return commentary_Images
     */
    public com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image[] getCommentary_Images() {
        return commentary_Images;
    }


    /**
     * Sets the commentary_Images value for this MarketIntell.
     * 
     * @param commentary_Images
     */
    public void setCommentary_Images(com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image[] commentary_Images) {
        this.commentary_Images = commentary_Images;
    }


    /**
     * Gets the commentary_Texts value for this MarketIntell.
     * 
     * @return commentary_Texts
     */
    public com.adm.webpart.webservices.BW_Market_Intelligence.MI_Additional_Text[] getCommentary_Texts() {
        return commentary_Texts;
    }


    /**
     * Sets the commentary_Texts value for this MarketIntell.
     * 
     * @param commentary_Texts
     */
    public void setCommentary_Texts(com.adm.webpart.webservices.BW_Market_Intelligence.MI_Additional_Text[] commentary_Texts) {
        this.commentary_Texts = commentary_Texts;
    }


    /**
     * Gets the previewControl value for this MarketIntell.
     * 
     * @return previewControl
     */
    public com.adm.webpart.webservices.BW_Market_Intelligence.ControlType getPreviewControl() {
        return previewControl;
    }


    /**
     * Sets the previewControl value for this MarketIntell.
     * 
     * @param previewControl
     */
    public void setPreviewControl(com.adm.webpart.webservices.BW_Market_Intelligence.ControlType previewControl) {
        this.previewControl = previewControl;
    }


    /**
     * Gets the firstImage value for this MarketIntell.
     * 
     * @return firstImage
     */
    public com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image getFirstImage() {
        return firstImage;
    }


    /**
     * Sets the firstImage value for this MarketIntell.
     * 
     * @param firstImage
     */
    public void setFirstImage(com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image firstImage) {
        this.firstImage = firstImage;
    }


    /**
     * Gets the firstAdditionalText value for this MarketIntell.
     * 
     * @return firstAdditionalText
     */
    public com.adm.webpart.webservices.BW_Market_Intelligence.MI_Additional_Text getFirstAdditionalText() {
        return firstAdditionalText;
    }


    /**
     * Sets the firstAdditionalText value for this MarketIntell.
     * 
     * @param firstAdditionalText
     */
    public void setFirstAdditionalText(com.adm.webpart.webservices.BW_Market_Intelligence.MI_Additional_Text firstAdditionalText) {
        this.firstAdditionalText = firstAdditionalText;
    }


    /**
     * Gets the videoEmbedCode value for this MarketIntell.
     * 
     * @return videoEmbedCode
     */
    public java.lang.String getVideoEmbedCode() {
        return videoEmbedCode;
    }


    /**
     * Sets the videoEmbedCode value for this MarketIntell.
     * 
     * @param videoEmbedCode
     */
    public void setVideoEmbedCode(java.lang.String videoEmbedCode) {
        this.videoEmbedCode = videoEmbedCode;
    }


    /**
     * Gets the videoIFrameEmbedCode value for this MarketIntell.
     * 
     * @return videoIFrameEmbedCode
     */
    public java.lang.String getVideoIFrameEmbedCode() {
        return videoIFrameEmbedCode;
    }


    /**
     * Sets the videoIFrameEmbedCode value for this MarketIntell.
     * 
     * @param videoIFrameEmbedCode
     */
    public void setVideoIFrameEmbedCode(java.lang.String videoIFrameEmbedCode) {
        this.videoIFrameEmbedCode = videoIFrameEmbedCode;
    }


    /**
     * Gets the controlCount value for this MarketIntell.
     * 
     * @return controlCount
     */
    public int getControlCount() {
        return controlCount;
    }


    /**
     * Sets the controlCount value for this MarketIntell.
     * 
     * @param controlCount
     */
    public void setControlCount(int controlCount) {
        this.controlCount = controlCount;
    }


    /**
     * Gets the hasDetail value for this MarketIntell.
     * 
     * @return hasDetail
     */
    public boolean isHasDetail() {
        return hasDetail;
    }


    /**
     * Sets the hasDetail value for this MarketIntell.
     * 
     * @param hasDetail
     */
    public void setHasDetail(boolean hasDetail) {
        this.hasDetail = hasDetail;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MarketIntell)) return false;
        MarketIntell other = (MarketIntell) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.commentaryID == other.getCommentaryID() &&
            ((this.articleTitle==null && other.getArticleTitle()==null) || 
             (this.articleTitle!=null &&
              this.articleTitle.equals(other.getArticleTitle()))) &&
            ((this.detailText==null && other.getDetailText()==null) || 
             (this.detailText!=null &&
              this.detailText.equals(other.getDetailText()))) &&
            ((this.shortPreviewText==null && other.getShortPreviewText()==null) || 
             (this.shortPreviewText!=null &&
              this.shortPreviewText.equals(other.getShortPreviewText()))) &&
            ((this.longPreviewText==null && other.getLongPreviewText()==null) || 
             (this.longPreviewText!=null &&
              this.longPreviewText.equals(other.getLongPreviewText()))) &&
            ((this.PDFLink==null && other.getPDFLink()==null) || 
             (this.PDFLink!=null &&
              this.PDFLink.equals(other.getPDFLink()))) &&
            this.PDFSize == other.getPDFSize() &&
            ((this.PDFStartText==null && other.getPDFStartText()==null) || 
             (this.PDFStartText!=null &&
              this.PDFStartText.equals(other.getPDFStartText()))) &&
            ((this.fileDate==null && other.getFileDate()==null) || 
             (this.fileDate!=null &&
              this.fileDate.equals(other.getFileDate()))) &&
            this.ID == other.getID() &&
            ((this.audioFile==null && other.getAudioFile()==null) || 
             (this.audioFile!=null &&
              java.util.Arrays.equals(this.audioFile, other.getAudioFile()))) &&
            this.audioSortIndex == other.getAudioSortIndex() &&
            this.audioSize == other.getAudioSize() &&
            ((this.videoURL==null && other.getVideoURL()==null) || 
             (this.videoURL!=null &&
              this.videoURL.equals(other.getVideoURL()))) &&
            this.videoSortIndex == other.getVideoSortIndex() &&
            ((this.commentary_Images==null && other.getCommentary_Images()==null) || 
             (this.commentary_Images!=null &&
              java.util.Arrays.equals(this.commentary_Images, other.getCommentary_Images()))) &&
            ((this.commentary_Texts==null && other.getCommentary_Texts()==null) || 
             (this.commentary_Texts!=null &&
              java.util.Arrays.equals(this.commentary_Texts, other.getCommentary_Texts()))) &&
            ((this.previewControl==null && other.getPreviewControl()==null) || 
             (this.previewControl!=null &&
              this.previewControl.equals(other.getPreviewControl()))) &&
            ((this.firstImage==null && other.getFirstImage()==null) || 
             (this.firstImage!=null &&
              this.firstImage.equals(other.getFirstImage()))) &&
            ((this.firstAdditionalText==null && other.getFirstAdditionalText()==null) || 
             (this.firstAdditionalText!=null &&
              this.firstAdditionalText.equals(other.getFirstAdditionalText()))) &&
            ((this.videoEmbedCode==null && other.getVideoEmbedCode()==null) || 
             (this.videoEmbedCode!=null &&
              this.videoEmbedCode.equals(other.getVideoEmbedCode()))) &&
            ((this.videoIFrameEmbedCode==null && other.getVideoIFrameEmbedCode()==null) || 
             (this.videoIFrameEmbedCode!=null &&
              this.videoIFrameEmbedCode.equals(other.getVideoIFrameEmbedCode()))) &&
            this.controlCount == other.getControlCount() &&
            this.hasDetail == other.isHasDetail();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getCommentaryID();
        if (getArticleTitle() != null) {
            _hashCode += getArticleTitle().hashCode();
        }
        if (getDetailText() != null) {
            _hashCode += getDetailText().hashCode();
        }
        if (getShortPreviewText() != null) {
            _hashCode += getShortPreviewText().hashCode();
        }
        if (getLongPreviewText() != null) {
            _hashCode += getLongPreviewText().hashCode();
        }
        if (getPDFLink() != null) {
            _hashCode += getPDFLink().hashCode();
        }
        _hashCode += getPDFSize();
        if (getPDFStartText() != null) {
            _hashCode += getPDFStartText().hashCode();
        }
        if (getFileDate() != null) {
            _hashCode += getFileDate().hashCode();
        }
        _hashCode += getID();
        if (getAudioFile() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAudioFile());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAudioFile(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getAudioSortIndex();
        _hashCode += getAudioSize();
        if (getVideoURL() != null) {
            _hashCode += getVideoURL().hashCode();
        }
        _hashCode += getVideoSortIndex();
        if (getCommentary_Images() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCommentary_Images());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCommentary_Images(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCommentary_Texts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCommentary_Texts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCommentary_Texts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPreviewControl() != null) {
            _hashCode += getPreviewControl().hashCode();
        }
        if (getFirstImage() != null) {
            _hashCode += getFirstImage().hashCode();
        }
        if (getFirstAdditionalText() != null) {
            _hashCode += getFirstAdditionalText().hashCode();
        }
        if (getVideoEmbedCode() != null) {
            _hashCode += getVideoEmbedCode().hashCode();
        }
        if (getVideoIFrameEmbedCode() != null) {
            _hashCode += getVideoIFrameEmbedCode().hashCode();
        }
        _hashCode += getControlCount();
        _hashCode += (isHasDetail() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MarketIntell.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MarketIntell"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentaryID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "CommentaryID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("articleTitle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "articleTitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("detailText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "detailText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shortPreviewText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "shortPreviewText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longPreviewText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "longPreviewText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PDFLink");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "PDFLink"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PDFSize");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "PDFSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PDFStartText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "PDFStartText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fileDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "fileDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("audioFile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "AudioFile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("audioSortIndex");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "AudioSortIndex"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("audioSize");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "AudioSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("videoURL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "VideoURL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("videoSortIndex");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "VideoSortIndex"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentary_Images");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "Commentary_Images"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MI_Image"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MI_Image"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentary_Texts");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "Commentary_Texts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MI_Additional_Text"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MI_Additional_Text"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("previewControl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "PreviewControl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "ControlType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstImage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "FirstImage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MI_Image"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstAdditionalText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "FirstAdditionalText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MI_Additional_Text"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("videoEmbedCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "VideoEmbedCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("videoIFrameEmbedCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "VideoIFrameEmbedCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("controlCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "controlCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hasDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "hasDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
