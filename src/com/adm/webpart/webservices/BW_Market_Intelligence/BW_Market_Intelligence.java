/**
 * BW_Market_Intelligence.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Market_Intelligence;

public interface BW_Market_Intelligence extends javax.xml.rpc.Service {
    public java.lang.String getBW_Market_IntelligenceSoapAddress();

    public com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_IntelligenceSoap getBW_Market_IntelligenceSoap() throws javax.xml.rpc.ServiceException;

    public com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_IntelligenceSoap getBW_Market_IntelligenceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
