/**
 * BW_Market_IntelligenceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Market_Intelligence;

public interface BW_Market_IntelligenceSoap extends java.rmi.Remote {
    public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell getADMISCommentary(java.lang.String subscriptionID) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell getTechComments(java.lang.String subscriptionID) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell getMarketingPartnersAdvisoryContent(java.lang.String subscriptionID) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[] getBensonQuinnContent(java.lang.String subscriptionID) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[] getMarketIntelContentByID(int commentatorID, java.lang.String subscriptionID) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell getMarketIntelCommentaryByID(int commentatorID, int commentaryID, java.lang.String subscriptionID) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[] getMarketIntelContentByName(java.lang.String commentatorName, java.lang.String subscriptionID) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.BW_Market_Intelligence.Commentator[] getCommentators(java.lang.String subscriptionID) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.BW_Market_Intelligence.Commentator getCommentatorByID(int commentatorID, java.lang.String subscriptionID) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image getCommentaryImage(int imageID, java.lang.String subscriptionID) throws java.rmi.RemoteException;
}
