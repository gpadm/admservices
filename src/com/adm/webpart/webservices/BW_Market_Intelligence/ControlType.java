/**
 * ControlType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Market_Intelligence;

public class ControlType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ControlType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _MainTextbox = "MainTextbox";
    public static final java.lang.String _Youtube = "Youtube";
    public static final java.lang.String _Audio = "Audio";
    public static final java.lang.String _Image = "Image";
    public static final java.lang.String _AdditionalTextbox = "AdditionalTextbox";
    public static final java.lang.String _PDF = "PDF";
    public static final java.lang.String _None = "None";
    public static final ControlType MainTextbox = new ControlType(_MainTextbox);
    public static final ControlType Youtube = new ControlType(_Youtube);
    public static final ControlType Audio = new ControlType(_Audio);
    public static final ControlType Image = new ControlType(_Image);
    public static final ControlType AdditionalTextbox = new ControlType(_AdditionalTextbox);
    public static final ControlType PDF = new ControlType(_PDF);
    public static final ControlType None = new ControlType(_None);
    public java.lang.String getValue() { return _value_;}
    public static ControlType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ControlType enumeration = (ControlType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ControlType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ControlType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "ControlType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
