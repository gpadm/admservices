/**
 * Commentator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Market_Intelligence;

public class Commentator  implements java.io.Serializable {
    private int ID;

    private java.lang.String title;

    private java.lang.String description;

    private com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image commentator_Image;

    private int sortIndex;

    private java.util.Calendar lastModifiedDate;

    private java.lang.String lastModifiedUser;

    private int commentaryCount;

    private com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[] commentarys;

    public Commentator() {
    }

    public Commentator(
           int ID,
           java.lang.String title,
           java.lang.String description,
           com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image commentator_Image,
           int sortIndex,
           java.util.Calendar lastModifiedDate,
           java.lang.String lastModifiedUser,
           int commentaryCount,
           com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[] commentarys) {
           this.ID = ID;
           this.title = title;
           this.description = description;
           this.commentator_Image = commentator_Image;
           this.sortIndex = sortIndex;
           this.lastModifiedDate = lastModifiedDate;
           this.lastModifiedUser = lastModifiedUser;
           this.commentaryCount = commentaryCount;
           this.commentarys = commentarys;
    }


    /**
     * Gets the ID value for this Commentator.
     * 
     * @return ID
     */
    public int getID() {
        return ID;
    }


    /**
     * Sets the ID value for this Commentator.
     * 
     * @param ID
     */
    public void setID(int ID) {
        this.ID = ID;
    }


    /**
     * Gets the title value for this Commentator.
     * 
     * @return title
     */
    public java.lang.String getTitle() {
        return title;
    }


    /**
     * Sets the title value for this Commentator.
     * 
     * @param title
     */
    public void setTitle(java.lang.String title) {
        this.title = title;
    }


    /**
     * Gets the description value for this Commentator.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this Commentator.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the commentator_Image value for this Commentator.
     * 
     * @return commentator_Image
     */
    public com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image getCommentator_Image() {
        return commentator_Image;
    }


    /**
     * Sets the commentator_Image value for this Commentator.
     * 
     * @param commentator_Image
     */
    public void setCommentator_Image(com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image commentator_Image) {
        this.commentator_Image = commentator_Image;
    }


    /**
     * Gets the sortIndex value for this Commentator.
     * 
     * @return sortIndex
     */
    public int getSortIndex() {
        return sortIndex;
    }


    /**
     * Sets the sortIndex value for this Commentator.
     * 
     * @param sortIndex
     */
    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }


    /**
     * Gets the lastModifiedDate value for this Commentator.
     * 
     * @return lastModifiedDate
     */
    public java.util.Calendar getLastModifiedDate() {
        return lastModifiedDate;
    }


    /**
     * Sets the lastModifiedDate value for this Commentator.
     * 
     * @param lastModifiedDate
     */
    public void setLastModifiedDate(java.util.Calendar lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }


    /**
     * Gets the lastModifiedUser value for this Commentator.
     * 
     * @return lastModifiedUser
     */
    public java.lang.String getLastModifiedUser() {
        return lastModifiedUser;
    }


    /**
     * Sets the lastModifiedUser value for this Commentator.
     * 
     * @param lastModifiedUser
     */
    public void setLastModifiedUser(java.lang.String lastModifiedUser) {
        this.lastModifiedUser = lastModifiedUser;
    }


    /**
     * Gets the commentaryCount value for this Commentator.
     * 
     * @return commentaryCount
     */
    public int getCommentaryCount() {
        return commentaryCount;
    }


    /**
     * Sets the commentaryCount value for this Commentator.
     * 
     * @param commentaryCount
     */
    public void setCommentaryCount(int commentaryCount) {
        this.commentaryCount = commentaryCount;
    }


    /**
     * Gets the commentarys value for this Commentator.
     * 
     * @return commentarys
     */
    public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[] getCommentarys() {
        return commentarys;
    }


    /**
     * Sets the commentarys value for this Commentator.
     * 
     * @param commentarys
     */
    public void setCommentarys(com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[] commentarys) {
        this.commentarys = commentarys;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Commentator)) return false;
        Commentator other = (Commentator) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.ID == other.getID() &&
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.commentator_Image==null && other.getCommentator_Image()==null) || 
             (this.commentator_Image!=null &&
              this.commentator_Image.equals(other.getCommentator_Image()))) &&
            this.sortIndex == other.getSortIndex() &&
            ((this.lastModifiedDate==null && other.getLastModifiedDate()==null) || 
             (this.lastModifiedDate!=null &&
              this.lastModifiedDate.equals(other.getLastModifiedDate()))) &&
            ((this.lastModifiedUser==null && other.getLastModifiedUser()==null) || 
             (this.lastModifiedUser!=null &&
              this.lastModifiedUser.equals(other.getLastModifiedUser()))) &&
            this.commentaryCount == other.getCommentaryCount() &&
            ((this.commentarys==null && other.getCommentarys()==null) || 
             (this.commentarys!=null &&
              java.util.Arrays.equals(this.commentarys, other.getCommentarys())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getID();
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getCommentator_Image() != null) {
            _hashCode += getCommentator_Image().hashCode();
        }
        _hashCode += getSortIndex();
        if (getLastModifiedDate() != null) {
            _hashCode += getLastModifiedDate().hashCode();
        }
        if (getLastModifiedUser() != null) {
            _hashCode += getLastModifiedUser().hashCode();
        }
        _hashCode += getCommentaryCount();
        if (getCommentarys() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCommentarys());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCommentarys(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Commentator.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "Commentator"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "Title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentator_Image");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "Commentator_Image"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MI_Image"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sortIndex");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "SortIndex"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastModifiedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "LastModifiedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastModifiedUser");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "LastModifiedUser"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentaryCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "CommentaryCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentarys");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "Commentarys"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MarketIntell"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MarketIntell"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
