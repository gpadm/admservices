/**
 * BW_Market_IntelligenceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Market_Intelligence;

public class BW_Market_IntelligenceLocator extends org.apache.axis.client.Service implements com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_Intelligence {

    public BW_Market_IntelligenceLocator() {
    }


    public BW_Market_IntelligenceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BW_Market_IntelligenceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BW_Market_IntelligenceSoap
    private java.lang.String BW_Market_IntelligenceSoap_address = "http://webservices.adm.com/BW_Market_Intelligence/BW_Market_Intelligence.asmx";

    public java.lang.String getBW_Market_IntelligenceSoapAddress() {
        return BW_Market_IntelligenceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BW_Market_IntelligenceSoapWSDDServiceName = "BW_Market_IntelligenceSoap";

    public java.lang.String getBW_Market_IntelligenceSoapWSDDServiceName() {
        return BW_Market_IntelligenceSoapWSDDServiceName;
    }

    public void setBW_Market_IntelligenceSoapWSDDServiceName(java.lang.String name) {
        BW_Market_IntelligenceSoapWSDDServiceName = name;
    }

    public com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_IntelligenceSoap getBW_Market_IntelligenceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BW_Market_IntelligenceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBW_Market_IntelligenceSoap(endpoint);
    }

    public com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_IntelligenceSoap getBW_Market_IntelligenceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_IntelligenceSoapStub _stub = new com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_IntelligenceSoapStub(portAddress, this);
            _stub.setPortName(getBW_Market_IntelligenceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBW_Market_IntelligenceSoapEndpointAddress(java.lang.String address) {
        BW_Market_IntelligenceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_IntelligenceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_IntelligenceSoapStub _stub = new com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_IntelligenceSoapStub(new java.net.URL(BW_Market_IntelligenceSoap_address), this);
                _stub.setPortName(getBW_Market_IntelligenceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BW_Market_IntelligenceSoap".equals(inputPortName)) {
            return getBW_Market_IntelligenceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "BW_Market_Intelligence");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "BW_Market_IntelligenceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BW_Market_IntelligenceSoap".equals(portName)) {
            setBW_Market_IntelligenceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
