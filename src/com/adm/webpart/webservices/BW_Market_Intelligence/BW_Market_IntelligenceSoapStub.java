/**
 * BW_Market_IntelligenceSoapStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Market_Intelligence;

public class BW_Market_IntelligenceSoapStub extends org.apache.axis.client.Stub implements com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_IntelligenceSoap {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[10];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetADMISCommentary");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MarketIntell"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetADMISCommentaryResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetTechComments");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MarketIntell"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetTechCommentsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetMarketingPartnersAdvisoryContent");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MarketIntell"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetMarketingPartnersAdvisoryContentResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBensonQuinnContent");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "ArrayOfMarketIntell"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetBensonQuinnContentResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MarketIntell"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetMarketIntelContentByID");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "CommentatorID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "ArrayOfMarketIntell"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetMarketIntelContentByIDResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MarketIntell"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetMarketIntelCommentaryByID");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "CommentatorID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "CommentaryID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MarketIntell"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetMarketIntelCommentaryByIDResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetMarketIntelContentByName");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "CommentatorName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "ArrayOfMarketIntell"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetMarketIntelContentByNameResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MarketIntell"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetCommentators");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "ArrayOfCommentator"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Market_Intelligence.Commentator[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetCommentatorsResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "Commentator"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetCommentatorByID");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "CommentatorID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "Commentator"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Market_Intelligence.Commentator.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetCommentatorByIDResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetCommentaryImage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "ImageID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MI_Image"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetCommentaryImageResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    public BW_Market_IntelligenceSoapStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public BW_Market_IntelligenceSoapStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public BW_Market_IntelligenceSoapStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "ArrayOfCommentator");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Market_Intelligence.Commentator[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "Commentator");
            qName2 = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "Commentator");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "ArrayOfMarketIntell");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MarketIntell");
            qName2 = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MarketIntell");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "ArrayOfMI_Additional_Text");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Market_Intelligence.MI_Additional_Text[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MI_Additional_Text");
            qName2 = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MI_Additional_Text");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "ArrayOfMI_Image");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MI_Image");
            qName2 = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MI_Image");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "Commentator");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Market_Intelligence.Commentator.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "ControlType");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Market_Intelligence.ControlType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MarketIntell");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MI_Additional_Text");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Market_Intelligence.MI_Additional_Text.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "MI_Image");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell getADMISCommentary(java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Market_Intelligence/GetADMISCommentary");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetADMISCommentary"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell getTechComments(java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Market_Intelligence/GetTechComments");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetTechComments"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell getMarketingPartnersAdvisoryContent(java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Market_Intelligence/GetMarketingPartnersAdvisoryContent");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetMarketingPartnersAdvisoryContent"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[] getBensonQuinnContent(java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Market_Intelligence/GetBensonQuinnContent");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetBensonQuinnContent"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[] getMarketIntelContentByID(int commentatorID, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Market_Intelligence/GetMarketIntelContentByID");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetMarketIntelContentByID"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(commentatorID), subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell getMarketIntelCommentaryByID(int commentatorID, int commentaryID, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Market_Intelligence/GetMarketIntelCommentaryByID");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetMarketIntelCommentaryByID"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(commentatorID), new java.lang.Integer(commentaryID), subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[] getMarketIntelContentByName(java.lang.String commentatorName, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Market_Intelligence/GetMarketIntelContentByName");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetMarketIntelContentByName"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {commentatorName, subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Market_Intelligence.Commentator[] getCommentators(java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Market_Intelligence/GetCommentators");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetCommentators"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.Commentator[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.Commentator[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Market_Intelligence.Commentator[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Market_Intelligence.Commentator getCommentatorByID(int commentatorID, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Market_Intelligence/GetCommentatorByID");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetCommentatorByID"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(commentatorID), subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.Commentator) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.Commentator) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Market_Intelligence.Commentator.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image getCommentaryImage(int imageID, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Market_Intelligence/GetCommentaryImage");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Market_Intelligence", "GetCommentaryImage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(imageID), subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
