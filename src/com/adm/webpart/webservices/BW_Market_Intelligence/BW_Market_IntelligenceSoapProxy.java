package com.adm.webpart.webservices.BW_Market_Intelligence;

public class BW_Market_IntelligenceSoapProxy implements com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_IntelligenceSoap {
  private String _endpoint = null;
  private com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_IntelligenceSoap bW_Market_IntelligenceSoap = null;
  
  public BW_Market_IntelligenceSoapProxy() {
    _initBW_Market_IntelligenceSoapProxy();
  }
  
  public BW_Market_IntelligenceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initBW_Market_IntelligenceSoapProxy();
  }
  
  private void _initBW_Market_IntelligenceSoapProxy() {
    try {
      bW_Market_IntelligenceSoap = (new com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_IntelligenceLocator()).getBW_Market_IntelligenceSoap();
      if (bW_Market_IntelligenceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bW_Market_IntelligenceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bW_Market_IntelligenceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bW_Market_IntelligenceSoap != null)
      ((javax.xml.rpc.Stub)bW_Market_IntelligenceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_IntelligenceSoap getBW_Market_IntelligenceSoap() {
    if (bW_Market_IntelligenceSoap == null)
      _initBW_Market_IntelligenceSoapProxy();
    return bW_Market_IntelligenceSoap;
  }
  
  public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell getADMISCommentary(java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Market_IntelligenceSoap == null)
      _initBW_Market_IntelligenceSoapProxy();
    return bW_Market_IntelligenceSoap.getADMISCommentary(subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell getTechComments(java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Market_IntelligenceSoap == null)
      _initBW_Market_IntelligenceSoapProxy();
    return bW_Market_IntelligenceSoap.getTechComments(subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell getMarketingPartnersAdvisoryContent(java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Market_IntelligenceSoap == null)
      _initBW_Market_IntelligenceSoapProxy();
    return bW_Market_IntelligenceSoap.getMarketingPartnersAdvisoryContent(subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[] getBensonQuinnContent(java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Market_IntelligenceSoap == null)
      _initBW_Market_IntelligenceSoapProxy();
    return bW_Market_IntelligenceSoap.getBensonQuinnContent(subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[] getMarketIntelContentByID(int commentatorID, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Market_IntelligenceSoap == null)
      _initBW_Market_IntelligenceSoapProxy();
    return bW_Market_IntelligenceSoap.getMarketIntelContentByID(commentatorID, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell getMarketIntelCommentaryByID(int commentatorID, int commentaryID, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Market_IntelligenceSoap == null)
      _initBW_Market_IntelligenceSoapProxy();
    return bW_Market_IntelligenceSoap.getMarketIntelCommentaryByID(commentatorID, commentaryID, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell[] getMarketIntelContentByName(java.lang.String commentatorName, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Market_IntelligenceSoap == null)
      _initBW_Market_IntelligenceSoapProxy();
    return bW_Market_IntelligenceSoap.getMarketIntelContentByName(commentatorName, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Market_Intelligence.Commentator[] getCommentators(java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Market_IntelligenceSoap == null)
      _initBW_Market_IntelligenceSoapProxy();
    return bW_Market_IntelligenceSoap.getCommentators(subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Market_Intelligence.Commentator getCommentatorByID(int commentatorID, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Market_IntelligenceSoap == null)
      _initBW_Market_IntelligenceSoapProxy();
    return bW_Market_IntelligenceSoap.getCommentatorByID(commentatorID, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Market_Intelligence.MI_Image getCommentaryImage(int imageID, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Market_IntelligenceSoap == null)
      _initBW_Market_IntelligenceSoapProxy();
    return bW_Market_IntelligenceSoap.getCommentaryImage(imageID, subscriptionID);
  }
  
  
}