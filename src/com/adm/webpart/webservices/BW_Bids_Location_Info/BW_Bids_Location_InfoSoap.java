/**
 * BW_Bids_Location_InfoSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Bids_Location_Info;

public interface BW_Bids_Location_InfoSoap extends java.rmi.Remote {

    /**
     * <b>Global Bids</b> - Returns all bids for a specific location
     * ID and groups them by commodity.  This method was created as part
     * of the global bids project (Jim Cosgrave, Drew Schweinfurth, Jared
     * Stephenson).
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBidCommodity[] getCommodityBidsForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException;

    /**
     * <b>Global Bids</b> - Returns all bids and offers for a specific
     * location ID.  This method was created as part of the global bids project
     * (Jim Cosgrave, Drew Schweinfurth, Jared Stephenson).
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getBidsAndOffersForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException;

    /**
     * <b>Global Bids</b> - Returns all bids and offers for a specific
     * location ID.  This method was created as part of the global bids project
     * (Jim Cosgrave, Drew Schweinfurth, Jared Stephenson).
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getDisplayBidsAndOffersForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException;

    /**
     * <b>Global Bids</b> - Returns all bids for a specific location
     * ID.  This method was created as part of the global bids project (Jim
     * Cosgrave, Drew Schweinfurth, Jared Stephenson).
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getBidsForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException;

    /**
     * <b>Global Bids</b> - Returns all bids that are marked to display
     * for a specific location ID.  This method was created as part of the
     * global bids project (Jim Cosgrave, Drew Schweinfurth, Jared Stephenson).
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getDisplayBidsForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException;

    /**
     * <b>Global Bids</b> - Returns all offers for a specific location
     * ID.  This method was created as part of the global bids project (Jim
     * Cosgrave, Drew Schweinfurth, Jared Stephenson).
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getOffersForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException;

    /**
     * <b>Global Bids</b> - Returns all offers for a specific location
     * ID.  This method was created as part of the global bids project (Jim
     * Cosgrave, Drew Schweinfurth, Jared Stephenson).
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getDisplayOffersForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException;

    /**
     * <b>Global Bids</b> - Returns all bids for a specific location
     * ID.  This method was created as part of the global bids project (Jim
     * Cosgrave, Drew Schweinfurth, Jared Stephenson).
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getBidsForLocations(java.lang.String locationIds, java.lang.String languageCode) throws java.rmi.RemoteException;

    /**
     * <b>Global Bids</b> - Returns a list of locations and the number
     * of bids they have entered.
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.LocationBidCount[] getBidCountForLocations(java.lang.String locationIds) throws java.rmi.RemoteException;

    /**
     * <b>Global Bids</b> - Returns all historical bids and offers
     * for a specific location ID.  This method was created as part of the
     * global bids project (Jim Cosgrave, Drew Schweinfurth, Jared Stephenson).
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getHistoricalBidsAndOffersForLocation(java.lang.String locationId, java.lang.String languageCode, java.lang.String date) throws java.rmi.RemoteException;

    /**
     * <b>Global Bids</b> - Returns all historical bids for a specific
     * location ID.  This method was created as part of the global bids project
     * (Jim Cosgrave, Drew Schweinfurth, Jared Stephenson).
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getHistoricalBidsForLocation(java.lang.String locationId, java.lang.String languageCode, java.lang.String date) throws java.rmi.RemoteException;

    /**
     * <b>Global Bids</b> - Returns all historical offers for a specific
     * location ID.  This method was created as part of the global bids project
     * (Jim Cosgrave, Drew Schweinfurth, Jared Stephenson).
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getHistoricalOffersForLocation(java.lang.String locationId, java.lang.String languageCode, java.lang.String date) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator getDetailedElevatorInformation(java.lang.String elevatorID) throws java.rmi.RemoteException;

    /**
     * <b>Global Bids</b> - Returns all bids and offers for a specific
     * location ID that are set up to go to DTN.  This method was created
     * as part of the DTN bids project by Jim Cosgrave.
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getDTNBidsAndOffersForLocation(java.lang.String locationId) throws java.rmi.RemoteException;

    /**
     * Retrieves a List of Bids and Offers by Location ID, and Dealer
     * Producer Code.<br><b>Parameters:</b><br>&nbsp;&nbsp;&bull;&nbsp;Location
     * ID: The Location Id for the Location you are looking up.<br>&nbsp;&nbsp;&bull;&nbsp;Dealer/Producer
     * Code: 'P' = Producer, 'D' = Dealer, 'A' = All, 'O' = Other<br>&nbsp;&nbsp;&bull;&nbsp;Subscription
     * ID: Currently Unused<br>
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getBidsAndOffersByLocation(int locationID, java.lang.String dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Retrieves a List of Bids by Location ID, and Dealer Producer
     * Code.<br><b>Parameters:</b><br>&nbsp;&nbsp;&bull;&nbsp;Location ID:
     * The Location Id for the Location you are looking up.<br>&nbsp;&nbsp;&bull;&nbsp;Dealer/Producer
     * Code: 'P' = Producer, 'D' = Dealer, 'A' = All, 'O' = Other<br>&nbsp;&nbsp;&bull;&nbsp;Subscription
     * ID: Currently Unused<br>
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getBidsByLocation(int locationID, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Retrieves a List of Offers by Location ID, and Dealer Producer
     * Code.<br><b>Parameters:</b><br>&nbsp;&nbsp;&bull;&nbsp;Location ID:
     * The Location Id for the Location you are looking up.<br>&nbsp;&nbsp;&bull;&nbsp;Dealer/Producer
     * Code: 'P' = Producer, 'D' = Dealer, 'A' = All, 'O' = Other<br>&nbsp;&nbsp;&bull;&nbsp;Subscription
     * ID: Currently Unused<br>
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getOffersByLocation(int locationID, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Retrieves a List of Historical bids and offers by location
     * ID, and Dealer Producer Code.
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getHistoricBidsAndOffersByLocation(int locationID, java.lang.String date, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Retrieves a List of Historical bids by location ID, and Dealer
     * Producer Code.
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getHistoricBidsByLocation(int locationID, java.lang.String date, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Retrieves a List of Historical offers by location ID, and Dealer
     * Producer Code.
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getHistoricOffersByLocation(int locationID, java.lang.String date, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Retrieves a List of Farmer View Bids by Location ID, and Dealer
     * Producer Code.<br><b>Parameters:</b><br>&nbsp;&nbsp;&bull;&nbsp;Location
     * ID: The Location Id for the Location you are looking up.<br>&nbsp;&nbsp;&bull;&nbsp;Dealer/Producer
     * Code: 'P' = Producer, 'D' = Dealer, 'A' = All, 'O' = Other<br>&nbsp;&nbsp;&bull;&nbsp;Subscription
     * ID: Currently Unused<br>
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getFarmerViewBidsByLocation(int locationID, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Retrieves a List of Farmer View Offers by Location ID, and
     * Dealer Producer Code.<br><b>Parameters:</b><br>&nbsp;&nbsp;&bull;&nbsp;Location
     * ID: The Location Id for the Location you are looking up.<br>&nbsp;&nbsp;&bull;&nbsp;Dealer/Producer
     * Code: 'P' = Producer, 'D' = Dealer, 'A' = All, 'O' = Other<br>&nbsp;&nbsp;&bull;&nbsp;Subscription
     * ID: Currently Unused<br>
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getFarmerViewOffersByLocation(int locationID, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Retrieves a list of BidCommodity objects for display on the
     * new FarmerView Mobile site.  Each BidCommodity object contains a list
     * of bids that a location his for a commodity.
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.BidCommodity[] getLocationBids(java.lang.String locationID, org.apache.axis.types.UnsignedShort dealerProducerCode) throws java.rmi.RemoteException;

    /**
     * Retrieves a list of States/Provinces that have ADM locations.<br><b>Parameters:</b><br>&nbsp;&nbsp;&bull;&nbsp;Subscription
     * ID: Currently Unused<br>
     */
    public java.lang.String[] getStateProvinceWithLocations(java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Retrieves a list of Locations from a particular State/Province.<br><b>Parameters:</b><br>&nbsp;&nbsp;&bull;&nbsp;State/Province:&nbsp;The
     * State or Province you are currently looking in.<br>&nbsp;&nbsp;&bull;&nbsp;Subscription
     * ID: Currently Unused<br>
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator[] getADMLocationsByState(java.lang.String stateProvince, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Retrieve a specific location.<br><b>Parameters:</b><br>&nbsp;&nbsp;&bull;&nbsp;Location
     * ID:&nbsp;The id of the Location you are looking for.<br>&nbsp;&nbsp;&bull;&nbsp;Subscription
     * ID: Currently Unused<br>
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator getLocationByID(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Retrieves the Bid Page Reply-To Email Address for a specific
     * location.<br><b>Parameters:</b><br>&nbsp;&nbsp;&bull;&nbsp;Location
     * ID:&nbsp;The id of the Location you are looking for.<br>&nbsp;&nbsp;&bull;&nbsp;Subscription
     * ID: Currently Unused<br>
     */
    public java.lang.String getBidReplyToEmail(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Retrieves the Hours of operation for a specific location.<br><b>Parameters:</b><br>&nbsp;&nbsp;&bull;&nbsp;Location
     * ID: The Location Id for the Location you are looking up.<br>&nbsp;&nbsp;&bull;&nbsp;Dealer/Producer
     * Code: 'P' = Producer, 'D' = Dealer, 'A' = All, 'O' = Other<br>&nbsp;&nbsp;&bull;&nbsp;Subscription
     * ID: Currently Unused<br>
     */
    public java.lang.String getLocationHours(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Retrieves information about a location.  This will get the
     * location name, address, phone number, and also the manager name
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.BidLocation getBidLocationInfo(java.lang.String locationID) throws java.rmi.RemoteException;

    /**
     * Retrieves the Manager Email Address for a specific location.<br><b>Parameters:</b><br>&nbsp;&nbsp;&bull;&nbsp;Location
     * ID:&nbsp;The id of the Location you are looking for.<br>&nbsp;&nbsp;&bull;&nbsp;Subscription
     * ID: Currently Unused<br>
     */
    public java.lang.String getLocationManagerEmail(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Returns all employees that are registered as admins for an
     * elevator.
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.Admin[] getElevatorAdmins(java.lang.String locationId) throws java.rmi.RemoteException;

    /**
     * Retrieves the List of Contacts for a specific location.<br><b>Parameters:</b><br>&nbsp;&nbsp;&bull;&nbsp;Location
     * ID: The Location Id for the Location you are looking up.<br>&nbsp;&nbsp;&bull;&nbsp;Subscription
     * ID: Currently Unused<br>
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.Contact[] getLocationContacts(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Retrieves the contact photo for a given staff ID<br><b>Parameters:</b><br>&nbsp;&nbsp;&bull;&nbsp;Staff
     * ID: The ID of the staff member who's photo is being retrieved<br>
     */
    public byte[] getContactPhoto(java.lang.String staffIDs) throws java.rmi.RemoteException;

    /**
     * Retrieves information about a location DS Files(Discount/Schedule).
     * This will get the DS file ID, name, LoadDate, Size
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.DiscountSchedule[] getDSFileInfo(java.lang.String locationID) throws java.rmi.RemoteException;

    /**
     * Retrieves information about a location Document File.  This
     * will get the LocationDocument file
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.LocationDocument[] getLocationDocumentFile(java.lang.String locationID) throws java.rmi.RemoteException;

    /**
     * Retrieves a list of BLI_Announcement objects that represent
     * announcements posted through location page admin.
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Announcement[] getAllLocationAnnouncements(java.lang.String locationID) throws java.rmi.RemoteException;

    /**
     * Retrieves the Announcements for a specific location.<br><b>Parameters:</b><br>&nbsp;&nbsp;&bull;&nbsp;Location
     * ID: The Location Id for the Location you are looking up.<br>&nbsp;&nbsp;&bull;&nbsp;Dealer/Producer
     * Code: 'P' = Producer, 'D' = Dealer, 'A' = All, 'O' = Other<br>&nbsp;&nbsp;&bull;&nbsp;Subscription
     * ID: Currently Unused<br>
     */
    public java.lang.String getLocationAnnouncements(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException;

    /**
     * Retrieves the Dealer or Producer Comments for a specific location.<br><b>Parameters:</b><br>&nbsp;&nbsp;&bull;&nbsp;Location
     * ID: The Location Id for the Location you are looking up.<br>&nbsp;&nbsp;&bull;&nbsp;Dealer/Producer
     * Code: 'P' = Producer, 'D' = Dealer, 'A' = All, 'O' = Other<br>&nbsp;&nbsp;&bull;&nbsp;Subscription
     * ID: Currently Unused<br>
     */
    public java.lang.String getLocationComments(int locationID, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException;
}
