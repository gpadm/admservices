/**
 * BLI_Group.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Bids_Location_Info;

public class BLI_Group  implements java.io.Serializable {
    private java.lang.String ID;

    private java.lang.String name;

    private java.lang.String companyID;

    private java.lang.String strIsFOBGroup;

    private boolean isFOBGroup;

    public BLI_Group() {
    }

    public BLI_Group(
           java.lang.String ID,
           java.lang.String name,
           java.lang.String companyID,
           java.lang.String strIsFOBGroup,
           boolean isFOBGroup) {
           this.ID = ID;
           this.name = name;
           this.companyID = companyID;
           this.strIsFOBGroup = strIsFOBGroup;
           this.isFOBGroup = isFOBGroup;
    }


    /**
     * Gets the ID value for this BLI_Group.
     * 
     * @return ID
     */
    public java.lang.String getID() {
        return ID;
    }


    /**
     * Sets the ID value for this BLI_Group.
     * 
     * @param ID
     */
    public void setID(java.lang.String ID) {
        this.ID = ID;
    }


    /**
     * Gets the name value for this BLI_Group.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this BLI_Group.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the companyID value for this BLI_Group.
     * 
     * @return companyID
     */
    public java.lang.String getCompanyID() {
        return companyID;
    }


    /**
     * Sets the companyID value for this BLI_Group.
     * 
     * @param companyID
     */
    public void setCompanyID(java.lang.String companyID) {
        this.companyID = companyID;
    }


    /**
     * Gets the strIsFOBGroup value for this BLI_Group.
     * 
     * @return strIsFOBGroup
     */
    public java.lang.String getStrIsFOBGroup() {
        return strIsFOBGroup;
    }


    /**
     * Sets the strIsFOBGroup value for this BLI_Group.
     * 
     * @param strIsFOBGroup
     */
    public void setStrIsFOBGroup(java.lang.String strIsFOBGroup) {
        this.strIsFOBGroup = strIsFOBGroup;
    }


    /**
     * Gets the isFOBGroup value for this BLI_Group.
     * 
     * @return isFOBGroup
     */
    public boolean isIsFOBGroup() {
        return isFOBGroup;
    }


    /**
     * Sets the isFOBGroup value for this BLI_Group.
     * 
     * @param isFOBGroup
     */
    public void setIsFOBGroup(boolean isFOBGroup) {
        this.isFOBGroup = isFOBGroup;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BLI_Group)) return false;
        BLI_Group other = (BLI_Group) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ID==null && other.getID()==null) || 
             (this.ID!=null &&
              this.ID.equals(other.getID()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.companyID==null && other.getCompanyID()==null) || 
             (this.companyID!=null &&
              this.companyID.equals(other.getCompanyID()))) &&
            ((this.strIsFOBGroup==null && other.getStrIsFOBGroup()==null) || 
             (this.strIsFOBGroup!=null &&
              this.strIsFOBGroup.equals(other.getStrIsFOBGroup()))) &&
            this.isFOBGroup == other.isIsFOBGroup();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getID() != null) {
            _hashCode += getID().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getCompanyID() != null) {
            _hashCode += getCompanyID().hashCode();
        }
        if (getStrIsFOBGroup() != null) {
            _hashCode += getStrIsFOBGroup().hashCode();
        }
        _hashCode += (isIsFOBGroup() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BLI_Group.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Group"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "CompanyID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strIsFOBGroup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "StrIsFOBGroup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isFOBGroup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "IsFOBGroup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
