/**
 * BLI_Commodity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Bids_Location_Info;

public class BLI_Commodity  implements java.io.Serializable {
    private java.lang.String id;

    private java.lang.String code;

    private java.lang.String description;

    private java.lang.String futuresSymbol;

    private java.lang.String UOMId;

    private java.lang.String currencyId;

    private java.lang.String marketId;

    private java.lang.String lbsPerBushel;

    private java.util.Calendar lastUpdate;

    private java.lang.String lastUpdateBy;

    private com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Currency currency;

    private com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_UnitOfMeasure UOM;

    private double basisDivisor;

    private int DTNCommodityID;

    public BLI_Commodity() {
    }

    public BLI_Commodity(
           java.lang.String id,
           java.lang.String code,
           java.lang.String description,
           java.lang.String futuresSymbol,
           java.lang.String UOMId,
           java.lang.String currencyId,
           java.lang.String marketId,
           java.lang.String lbsPerBushel,
           java.util.Calendar lastUpdate,
           java.lang.String lastUpdateBy,
           com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Currency currency,
           com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_UnitOfMeasure UOM,
           double basisDivisor,
           int DTNCommodityID) {
           this.id = id;
           this.code = code;
           this.description = description;
           this.futuresSymbol = futuresSymbol;
           this.UOMId = UOMId;
           this.currencyId = currencyId;
           this.marketId = marketId;
           this.lbsPerBushel = lbsPerBushel;
           this.lastUpdate = lastUpdate;
           this.lastUpdateBy = lastUpdateBy;
           this.currency = currency;
           this.UOM = UOM;
           this.basisDivisor = basisDivisor;
           this.DTNCommodityID = DTNCommodityID;
    }


    /**
     * Gets the id value for this BLI_Commodity.
     * 
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }


    /**
     * Sets the id value for this BLI_Commodity.
     * 
     * @param id
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }


    /**
     * Gets the code value for this BLI_Commodity.
     * 
     * @return code
     */
    public java.lang.String getCode() {
        return code;
    }


    /**
     * Sets the code value for this BLI_Commodity.
     * 
     * @param code
     */
    public void setCode(java.lang.String code) {
        this.code = code;
    }


    /**
     * Gets the description value for this BLI_Commodity.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this BLI_Commodity.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the futuresSymbol value for this BLI_Commodity.
     * 
     * @return futuresSymbol
     */
    public java.lang.String getFuturesSymbol() {
        return futuresSymbol;
    }


    /**
     * Sets the futuresSymbol value for this BLI_Commodity.
     * 
     * @param futuresSymbol
     */
    public void setFuturesSymbol(java.lang.String futuresSymbol) {
        this.futuresSymbol = futuresSymbol;
    }


    /**
     * Gets the UOMId value for this BLI_Commodity.
     * 
     * @return UOMId
     */
    public java.lang.String getUOMId() {
        return UOMId;
    }


    /**
     * Sets the UOMId value for this BLI_Commodity.
     * 
     * @param UOMId
     */
    public void setUOMId(java.lang.String UOMId) {
        this.UOMId = UOMId;
    }


    /**
     * Gets the currencyId value for this BLI_Commodity.
     * 
     * @return currencyId
     */
    public java.lang.String getCurrencyId() {
        return currencyId;
    }


    /**
     * Sets the currencyId value for this BLI_Commodity.
     * 
     * @param currencyId
     */
    public void setCurrencyId(java.lang.String currencyId) {
        this.currencyId = currencyId;
    }


    /**
     * Gets the marketId value for this BLI_Commodity.
     * 
     * @return marketId
     */
    public java.lang.String getMarketId() {
        return marketId;
    }


    /**
     * Sets the marketId value for this BLI_Commodity.
     * 
     * @param marketId
     */
    public void setMarketId(java.lang.String marketId) {
        this.marketId = marketId;
    }


    /**
     * Gets the lbsPerBushel value for this BLI_Commodity.
     * 
     * @return lbsPerBushel
     */
    public java.lang.String getLbsPerBushel() {
        return lbsPerBushel;
    }


    /**
     * Sets the lbsPerBushel value for this BLI_Commodity.
     * 
     * @param lbsPerBushel
     */
    public void setLbsPerBushel(java.lang.String lbsPerBushel) {
        this.lbsPerBushel = lbsPerBushel;
    }


    /**
     * Gets the lastUpdate value for this BLI_Commodity.
     * 
     * @return lastUpdate
     */
    public java.util.Calendar getLastUpdate() {
        return lastUpdate;
    }


    /**
     * Sets the lastUpdate value for this BLI_Commodity.
     * 
     * @param lastUpdate
     */
    public void setLastUpdate(java.util.Calendar lastUpdate) {
        this.lastUpdate = lastUpdate;
    }


    /**
     * Gets the lastUpdateBy value for this BLI_Commodity.
     * 
     * @return lastUpdateBy
     */
    public java.lang.String getLastUpdateBy() {
        return lastUpdateBy;
    }


    /**
     * Sets the lastUpdateBy value for this BLI_Commodity.
     * 
     * @param lastUpdateBy
     */
    public void setLastUpdateBy(java.lang.String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }


    /**
     * Gets the currency value for this BLI_Commodity.
     * 
     * @return currency
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Currency getCurrency() {
        return currency;
    }


    /**
     * Sets the currency value for this BLI_Commodity.
     * 
     * @param currency
     */
    public void setCurrency(com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Currency currency) {
        this.currency = currency;
    }


    /**
     * Gets the UOM value for this BLI_Commodity.
     * 
     * @return UOM
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_UnitOfMeasure getUOM() {
        return UOM;
    }


    /**
     * Sets the UOM value for this BLI_Commodity.
     * 
     * @param UOM
     */
    public void setUOM(com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_UnitOfMeasure UOM) {
        this.UOM = UOM;
    }


    /**
     * Gets the basisDivisor value for this BLI_Commodity.
     * 
     * @return basisDivisor
     */
    public double getBasisDivisor() {
        return basisDivisor;
    }


    /**
     * Sets the basisDivisor value for this BLI_Commodity.
     * 
     * @param basisDivisor
     */
    public void setBasisDivisor(double basisDivisor) {
        this.basisDivisor = basisDivisor;
    }


    /**
     * Gets the DTNCommodityID value for this BLI_Commodity.
     * 
     * @return DTNCommodityID
     */
    public int getDTNCommodityID() {
        return DTNCommodityID;
    }


    /**
     * Sets the DTNCommodityID value for this BLI_Commodity.
     * 
     * @param DTNCommodityID
     */
    public void setDTNCommodityID(int DTNCommodityID) {
        this.DTNCommodityID = DTNCommodityID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BLI_Commodity)) return false;
        BLI_Commodity other = (BLI_Commodity) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.futuresSymbol==null && other.getFuturesSymbol()==null) || 
             (this.futuresSymbol!=null &&
              this.futuresSymbol.equals(other.getFuturesSymbol()))) &&
            ((this.UOMId==null && other.getUOMId()==null) || 
             (this.UOMId!=null &&
              this.UOMId.equals(other.getUOMId()))) &&
            ((this.currencyId==null && other.getCurrencyId()==null) || 
             (this.currencyId!=null &&
              this.currencyId.equals(other.getCurrencyId()))) &&
            ((this.marketId==null && other.getMarketId()==null) || 
             (this.marketId!=null &&
              this.marketId.equals(other.getMarketId()))) &&
            ((this.lbsPerBushel==null && other.getLbsPerBushel()==null) || 
             (this.lbsPerBushel!=null &&
              this.lbsPerBushel.equals(other.getLbsPerBushel()))) &&
            ((this.lastUpdate==null && other.getLastUpdate()==null) || 
             (this.lastUpdate!=null &&
              this.lastUpdate.equals(other.getLastUpdate()))) &&
            ((this.lastUpdateBy==null && other.getLastUpdateBy()==null) || 
             (this.lastUpdateBy!=null &&
              this.lastUpdateBy.equals(other.getLastUpdateBy()))) &&
            ((this.currency==null && other.getCurrency()==null) || 
             (this.currency!=null &&
              this.currency.equals(other.getCurrency()))) &&
            ((this.UOM==null && other.getUOM()==null) || 
             (this.UOM!=null &&
              this.UOM.equals(other.getUOM()))) &&
            this.basisDivisor == other.getBasisDivisor() &&
            this.DTNCommodityID == other.getDTNCommodityID();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getFuturesSymbol() != null) {
            _hashCode += getFuturesSymbol().hashCode();
        }
        if (getUOMId() != null) {
            _hashCode += getUOMId().hashCode();
        }
        if (getCurrencyId() != null) {
            _hashCode += getCurrencyId().hashCode();
        }
        if (getMarketId() != null) {
            _hashCode += getMarketId().hashCode();
        }
        if (getLbsPerBushel() != null) {
            _hashCode += getLbsPerBushel().hashCode();
        }
        if (getLastUpdate() != null) {
            _hashCode += getLastUpdate().hashCode();
        }
        if (getLastUpdateBy() != null) {
            _hashCode += getLastUpdateBy().hashCode();
        }
        if (getCurrency() != null) {
            _hashCode += getCurrency().hashCode();
        }
        if (getUOM() != null) {
            _hashCode += getUOM().hashCode();
        }
        _hashCode += new Double(getBasisDivisor()).hashCode();
        _hashCode += getDTNCommodityID();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BLI_Commodity.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Commodity"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("futuresSymbol");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "FuturesSymbol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UOMId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "UOMId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "CurrencyId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marketId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "MarketId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lbsPerBushel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LbsPerBushel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LastUpdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdateBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LastUpdateBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Currency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Currency"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UOM");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "UOM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_UnitOfMeasure"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("basisDivisor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BasisDivisor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DTNCommodityID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DTNCommodityID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
