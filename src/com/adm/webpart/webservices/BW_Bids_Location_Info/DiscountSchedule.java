/**
 * DiscountSchedule.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Bids_Location_Info;

public class DiscountSchedule  implements java.io.Serializable {
    private int DS_ID;

    private int DS_ElevID;

    private java.lang.String DS_FileName;

    private java.lang.String DS_LoadDate;

    private java.lang.String DS_Size;

    public DiscountSchedule() {
    }

    public DiscountSchedule(
           int DS_ID,
           int DS_ElevID,
           java.lang.String DS_FileName,
           java.lang.String DS_LoadDate,
           java.lang.String DS_Size) {
           this.DS_ID = DS_ID;
           this.DS_ElevID = DS_ElevID;
           this.DS_FileName = DS_FileName;
           this.DS_LoadDate = DS_LoadDate;
           this.DS_Size = DS_Size;
    }


    /**
     * Gets the DS_ID value for this DiscountSchedule.
     * 
     * @return DS_ID
     */
    public int getDS_ID() {
        return DS_ID;
    }


    /**
     * Sets the DS_ID value for this DiscountSchedule.
     * 
     * @param DS_ID
     */
    public void setDS_ID(int DS_ID) {
        this.DS_ID = DS_ID;
    }


    /**
     * Gets the DS_ElevID value for this DiscountSchedule.
     * 
     * @return DS_ElevID
     */
    public int getDS_ElevID() {
        return DS_ElevID;
    }


    /**
     * Sets the DS_ElevID value for this DiscountSchedule.
     * 
     * @param DS_ElevID
     */
    public void setDS_ElevID(int DS_ElevID) {
        this.DS_ElevID = DS_ElevID;
    }


    /**
     * Gets the DS_FileName value for this DiscountSchedule.
     * 
     * @return DS_FileName
     */
    public java.lang.String getDS_FileName() {
        return DS_FileName;
    }


    /**
     * Sets the DS_FileName value for this DiscountSchedule.
     * 
     * @param DS_FileName
     */
    public void setDS_FileName(java.lang.String DS_FileName) {
        this.DS_FileName = DS_FileName;
    }


    /**
     * Gets the DS_LoadDate value for this DiscountSchedule.
     * 
     * @return DS_LoadDate
     */
    public java.lang.String getDS_LoadDate() {
        return DS_LoadDate;
    }


    /**
     * Sets the DS_LoadDate value for this DiscountSchedule.
     * 
     * @param DS_LoadDate
     */
    public void setDS_LoadDate(java.lang.String DS_LoadDate) {
        this.DS_LoadDate = DS_LoadDate;
    }


    /**
     * Gets the DS_Size value for this DiscountSchedule.
     * 
     * @return DS_Size
     */
    public java.lang.String getDS_Size() {
        return DS_Size;
    }


    /**
     * Sets the DS_Size value for this DiscountSchedule.
     * 
     * @param DS_Size
     */
    public void setDS_Size(java.lang.String DS_Size) {
        this.DS_Size = DS_Size;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DiscountSchedule)) return false;
        DiscountSchedule other = (DiscountSchedule) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.DS_ID == other.getDS_ID() &&
            this.DS_ElevID == other.getDS_ElevID() &&
            ((this.DS_FileName==null && other.getDS_FileName()==null) || 
             (this.DS_FileName!=null &&
              this.DS_FileName.equals(other.getDS_FileName()))) &&
            ((this.DS_LoadDate==null && other.getDS_LoadDate()==null) || 
             (this.DS_LoadDate!=null &&
              this.DS_LoadDate.equals(other.getDS_LoadDate()))) &&
            ((this.DS_Size==null && other.getDS_Size()==null) || 
             (this.DS_Size!=null &&
              this.DS_Size.equals(other.getDS_Size())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getDS_ID();
        _hashCode += getDS_ElevID();
        if (getDS_FileName() != null) {
            _hashCode += getDS_FileName().hashCode();
        }
        if (getDS_LoadDate() != null) {
            _hashCode += getDS_LoadDate().hashCode();
        }
        if (getDS_Size() != null) {
            _hashCode += getDS_Size().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DiscountSchedule.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DiscountSchedule"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DS_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DS_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DS_ElevID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DS_ElevID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DS_FileName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DS_FileName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DS_LoadDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DS_LoadDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DS_Size");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DS_Size"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
