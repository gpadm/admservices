/**
 * LocationDocument.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Bids_Location_Info;

import java.util.Arrays;

public class LocationDocument  implements java.io.Serializable {
    private int LD_ID;

    private int LD_ElevID;

    private java.lang.String LD_Custom_Name;

    private java.lang.String LD_FileName;

    private java.lang.String LD_LoadDate;

    private java.lang.String LD_Size;

    private byte[] LD_FileData;

    private java.lang.String LD_View_Access;

    public LocationDocument() {
    }

    public LocationDocument(
           int LD_ID,
           int LD_ElevID,
           java.lang.String LD_Custom_Name,
           java.lang.String LD_FileName,
           java.lang.String LD_LoadDate,
           java.lang.String LD_Size,
           byte[] LD_FileData,
           java.lang.String LD_View_Access) {
           this.LD_ID = LD_ID;
           this.LD_ElevID = LD_ElevID;
           this.LD_Custom_Name = LD_Custom_Name;
           this.LD_FileName = LD_FileName;
           this.LD_LoadDate = LD_LoadDate;
           this.LD_Size = LD_Size;
           this.LD_FileData = LD_FileData;
           this.LD_View_Access = LD_View_Access;
    }


    /**
     * Gets the LD_ID value for this LocationDocument.
     * 
     * @return LD_ID
     */
    public int getLD_ID() {
        return LD_ID;
    }


    /**
     * Sets the LD_ID value for this LocationDocument.
     * 
     * @param LD_ID
     */
    public void setLD_ID(int LD_ID) {
        this.LD_ID = LD_ID;
    }


    /**
     * Gets the LD_ElevID value for this LocationDocument.
     * 
     * @return LD_ElevID
     */
    public int getLD_ElevID() {
        return LD_ElevID;
    }


    /**
     * Sets the LD_ElevID value for this LocationDocument.
     * 
     * @param LD_ElevID
     */
    public void setLD_ElevID(int LD_ElevID) {
        this.LD_ElevID = LD_ElevID;
    }


    /**
     * Gets the LD_Custom_Name value for this LocationDocument.
     * 
     * @return LD_Custom_Name
     */
    public java.lang.String getLD_Custom_Name() {
        return LD_Custom_Name;
    }


    /**
     * Sets the LD_Custom_Name value for this LocationDocument.
     * 
     * @param LD_Custom_Name
     */
    public void setLD_Custom_Name(java.lang.String LD_Custom_Name) {
        this.LD_Custom_Name = LD_Custom_Name;
    }


    /**
     * Gets the LD_FileName value for this LocationDocument.
     * 
     * @return LD_FileName
     */
    public java.lang.String getLD_FileName() {
        return LD_FileName;
    }


    /**
     * Sets the LD_FileName value for this LocationDocument.
     * 
     * @param LD_FileName
     */
    public void setLD_FileName(java.lang.String LD_FileName) {
        this.LD_FileName = LD_FileName;
    }


    /**
     * Gets the LD_LoadDate value for this LocationDocument.
     * 
     * @return LD_LoadDate
     */
    public java.lang.String getLD_LoadDate() {
        return LD_LoadDate;
    }


    /**
     * Sets the LD_LoadDate value for this LocationDocument.
     * 
     * @param LD_LoadDate
     */
    public void setLD_LoadDate(java.lang.String LD_LoadDate) {
        this.LD_LoadDate = LD_LoadDate;
    }


    /**
     * Gets the LD_Size value for this LocationDocument.
     * 
     * @return LD_Size
     */
    public java.lang.String getLD_Size() {
        return LD_Size;
    }


    /**
     * Sets the LD_Size value for this LocationDocument.
     * 
     * @param LD_Size
     */
    public void setLD_Size(java.lang.String LD_Size) {
        this.LD_Size = LD_Size;
    }


    /**
     * Gets the LD_FileData value for this LocationDocument.
     * 
     * @return LD_FileData
     */
    public byte[] getLD_FileData() {
        return LD_FileData;
    }


    /**
     * Sets the LD_FileData value for this LocationDocument.
     * 
     * @param LD_FileData
     */
    public void setLD_FileData(byte[] LD_FileData) {
        this.LD_FileData = LD_FileData;
    }


    /**
     * Gets the LD_View_Access value for this LocationDocument.
     * 
     * @return LD_View_Access
     */
    public java.lang.String getLD_View_Access() {
        return LD_View_Access;
    }


    /**
     * Sets the LD_View_Access value for this LocationDocument.
     * 
     * @param LD_View_Access
     */
    public void setLD_View_Access(java.lang.String LD_View_Access) {
        this.LD_View_Access = LD_View_Access;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LocationDocument)) return false;
        LocationDocument other = (LocationDocument) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.LD_ID == other.getLD_ID() &&
            this.LD_ElevID == other.getLD_ElevID() &&
            ((this.LD_Custom_Name==null && other.getLD_Custom_Name()==null) || 
             (this.LD_Custom_Name!=null &&
              this.LD_Custom_Name.equals(other.getLD_Custom_Name()))) &&
            ((this.LD_FileName==null && other.getLD_FileName()==null) || 
             (this.LD_FileName!=null &&
              this.LD_FileName.equals(other.getLD_FileName()))) &&
            ((this.LD_LoadDate==null && other.getLD_LoadDate()==null) || 
             (this.LD_LoadDate!=null &&
              this.LD_LoadDate.equals(other.getLD_LoadDate()))) &&
            ((this.LD_Size==null && other.getLD_Size()==null) || 
             (this.LD_Size!=null &&
              this.LD_Size.equals(other.getLD_Size()))) &&
            ((this.LD_FileData==null && other.getLD_FileData()==null) || 
             (this.LD_FileData!=null &&
              java.util.Arrays.equals(this.LD_FileData, other.getLD_FileData()))) &&
            ((this.LD_View_Access==null && other.getLD_View_Access()==null) || 
             (this.LD_View_Access!=null &&
              this.LD_View_Access.equals(other.getLD_View_Access())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getLD_ID();
        _hashCode += getLD_ElevID();
        if (getLD_Custom_Name() != null) {
            _hashCode += getLD_Custom_Name().hashCode();
        }
        if (getLD_FileName() != null) {
            _hashCode += getLD_FileName().hashCode();
        }
        if (getLD_LoadDate() != null) {
            _hashCode += getLD_LoadDate().hashCode();
        }
        if (getLD_Size() != null) {
            _hashCode += getLD_Size().hashCode();
        }
        if (getLD_FileData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLD_FileData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLD_FileData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLD_View_Access() != null) {
            _hashCode += getLD_View_Access().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LocationDocument.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LocationDocument"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LD_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LD_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LD_ElevID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LD_ElevID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LD_Custom_Name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LD_Custom_Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LD_FileName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LD_FileName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LD_LoadDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LD_LoadDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LD_Size");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LD_Size"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LD_FileData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LD_FileData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LD_View_Access");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LD_View_Access"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	@Override
	public String toString() {
		return "LocationDocument [LD_ID=" + LD_ID + ", LD_ElevID=" + LD_ElevID + ", LD_Custom_Name=" + LD_Custom_Name
				+ ", LD_FileName=" + LD_FileName + ", LD_LoadDate=" + LD_LoadDate + ", LD_Size=" + LD_Size
				+ ", LD_FileData=" + Arrays.toString(LD_FileData) + ", LD_View_Access=" + LD_View_Access
				+ ", __equalsCalc=" + __equalsCalc + ", __hashCodeCalc=" + __hashCodeCalc + "]";
	}

}
