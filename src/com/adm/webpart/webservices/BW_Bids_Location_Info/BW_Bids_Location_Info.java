/**
 * BW_Bids_Location_Info.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Bids_Location_Info;

public interface BW_Bids_Location_Info extends javax.xml.rpc.Service {

/**
 * This web service will retrieve different kinds information pertaining
 * to ADM locations.<br>
 */
    public java.lang.String getBW_Bids_Location_InfoSoapAddress();

    public com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoap getBW_Bids_Location_InfoSoap() throws javax.xml.rpc.ServiceException;

    public com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoap getBW_Bids_Location_InfoSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
