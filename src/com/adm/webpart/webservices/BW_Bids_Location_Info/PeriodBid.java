/**
 * PeriodBid.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Bids_Location_Info;

public class PeriodBid  implements java.io.Serializable {
    private int groupId;

    private int periodId;

    private java.util.Calendar beginDate;

    private java.util.Calendar endDate;

    private java.util.Calendar entryDate;

    private int elevatorId;

    private java.lang.String elevatorName;

    private com.adm.webpart.webservices.BW_Bids_Location_Info.VendorCustomerType vendCustType;

    private java.lang.String commodityId;

    private java.lang.String commodityCode;

    private java.lang.String commodityDescription;

    private java.lang.String futuresSymbolRoot;

    private java.lang.String futuresSymbolFull;

    private java.lang.String transportType;

    private com.adm.webpart.webservices.BW_Bids_Location_Info.DealerProducerType dealerProdType;

    private java.lang.String futures;

    private java.lang.String basis;

    private java.lang.String flat;

    private java.lang.String flatCurrency;

    private com.adm.webpart.webservices.BW_Bids_Location_Info.BidsEntryType entryType;

    private java.lang.String optionMonth;

    private java.lang.String optionMonthCode;

    private int optionMonthNumber;

    private int optionYear;

    private java.lang.String strOptionYear;

    private com.adm.webpart.webservices.BW_Bids_Location_Info.BidsRoundOption roundType;

    private boolean displayOnWeb;

    private boolean displayInDTN;

    private java.lang.String currencyCode;

    private java.lang.String marketCode;

    private boolean settled;

    private java.lang.String strVendCustType;

    private com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Commodity commodity;

    private com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Market market;

    private com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_UnitOfMeasure unitOfMeasure;

    private com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Currency currency;

    private java.lang.String basisOrFlat;

    private java.lang.String _final;

    private java.lang.String trans;

    private boolean wasEnteredThroughGlobalBidsSystem;

    private java.lang.String strGroupInitializationMode;

    private boolean isInGroupInitializationMode;

    private int DTNBidContractID;

    private java.lang.String DTNDeliveryLabel;

    public PeriodBid() {
    }

    public PeriodBid(
           int groupId,
           int periodId,
           java.util.Calendar beginDate,
           java.util.Calendar endDate,
           java.util.Calendar entryDate,
           int elevatorId,
           java.lang.String elevatorName,
           com.adm.webpart.webservices.BW_Bids_Location_Info.VendorCustomerType vendCustType,
           java.lang.String commodityId,
           java.lang.String commodityCode,
           java.lang.String commodityDescription,
           java.lang.String futuresSymbolRoot,
           java.lang.String futuresSymbolFull,
           java.lang.String transportType,
           com.adm.webpart.webservices.BW_Bids_Location_Info.DealerProducerType dealerProdType,
           java.lang.String futures,
           java.lang.String basis,
           java.lang.String flat,
           java.lang.String flatCurrency,
           com.adm.webpart.webservices.BW_Bids_Location_Info.BidsEntryType entryType,
           java.lang.String optionMonth,
           java.lang.String optionMonthCode,
           int optionMonthNumber,
           int optionYear,
           java.lang.String strOptionYear,
           com.adm.webpart.webservices.BW_Bids_Location_Info.BidsRoundOption roundType,
           boolean displayOnWeb,
           boolean displayInDTN,
           java.lang.String currencyCode,
           java.lang.String marketCode,
           boolean settled,
           java.lang.String strVendCustType,
           com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Commodity commodity,
           com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Market market,
           com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_UnitOfMeasure unitOfMeasure,
           com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Currency currency,
           java.lang.String basisOrFlat,
           java.lang.String _final,
           java.lang.String trans,
           boolean wasEnteredThroughGlobalBidsSystem,
           java.lang.String strGroupInitializationMode,
           boolean isInGroupInitializationMode,
           int DTNBidContractID,
           java.lang.String DTNDeliveryLabel) {
           this.groupId = groupId;
           this.periodId = periodId;
           this.beginDate = beginDate;
           this.endDate = endDate;
           this.entryDate = entryDate;
           this.elevatorId = elevatorId;
           this.elevatorName = elevatorName;
           this.vendCustType = vendCustType;
           this.commodityId = commodityId;
           this.commodityCode = commodityCode;
           this.commodityDescription = commodityDescription;
           this.futuresSymbolRoot = futuresSymbolRoot;
           this.futuresSymbolFull = futuresSymbolFull;
           this.transportType = transportType;
           this.dealerProdType = dealerProdType;
           this.futures = futures;
           this.basis = basis;
           this.flat = flat;
           this.flatCurrency = flatCurrency;
           this.entryType = entryType;
           this.optionMonth = optionMonth;
           this.optionMonthCode = optionMonthCode;
           this.optionMonthNumber = optionMonthNumber;
           this.optionYear = optionYear;
           this.strOptionYear = strOptionYear;
           this.roundType = roundType;
           this.displayOnWeb = displayOnWeb;
           this.displayInDTN = displayInDTN;
           this.currencyCode = currencyCode;
           this.marketCode = marketCode;
           this.settled = settled;
           this.strVendCustType = strVendCustType;
           this.commodity = commodity;
           this.market = market;
           this.unitOfMeasure = unitOfMeasure;
           this.currency = currency;
           this.basisOrFlat = basisOrFlat;
           this._final = _final;
           this.trans = trans;
           this.wasEnteredThroughGlobalBidsSystem = wasEnteredThroughGlobalBidsSystem;
           this.strGroupInitializationMode = strGroupInitializationMode;
           this.isInGroupInitializationMode = isInGroupInitializationMode;
           this.DTNBidContractID = DTNBidContractID;
           this.DTNDeliveryLabel = DTNDeliveryLabel;
    }


    /**
     * Gets the groupId value for this PeriodBid.
     * 
     * @return groupId
     */
    public int getGroupId() {
        return groupId;
    }


    /**
     * Sets the groupId value for this PeriodBid.
     * 
     * @param groupId
     */
    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }


    /**
     * Gets the periodId value for this PeriodBid.
     * 
     * @return periodId
     */
    public int getPeriodId() {
        return periodId;
    }


    /**
     * Sets the periodId value for this PeriodBid.
     * 
     * @param periodId
     */
    public void setPeriodId(int periodId) {
        this.periodId = periodId;
    }


    /**
     * Gets the beginDate value for this PeriodBid.
     * 
     * @return beginDate
     */
    public java.util.Calendar getBeginDate() {
        return beginDate;
    }


    /**
     * Sets the beginDate value for this PeriodBid.
     * 
     * @param beginDate
     */
    public void setBeginDate(java.util.Calendar beginDate) {
        this.beginDate = beginDate;
    }


    /**
     * Gets the endDate value for this PeriodBid.
     * 
     * @return endDate
     */
    public java.util.Calendar getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this PeriodBid.
     * 
     * @param endDate
     */
    public void setEndDate(java.util.Calendar endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the entryDate value for this PeriodBid.
     * 
     * @return entryDate
     */
    public java.util.Calendar getEntryDate() {
        return entryDate;
    }


    /**
     * Sets the entryDate value for this PeriodBid.
     * 
     * @param entryDate
     */
    public void setEntryDate(java.util.Calendar entryDate) {
        this.entryDate = entryDate;
    }


    /**
     * Gets the elevatorId value for this PeriodBid.
     * 
     * @return elevatorId
     */
    public int getElevatorId() {
        return elevatorId;
    }


    /**
     * Sets the elevatorId value for this PeriodBid.
     * 
     * @param elevatorId
     */
    public void setElevatorId(int elevatorId) {
        this.elevatorId = elevatorId;
    }


    /**
     * Gets the elevatorName value for this PeriodBid.
     * 
     * @return elevatorName
     */
    public java.lang.String getElevatorName() {
        return elevatorName;
    }


    /**
     * Sets the elevatorName value for this PeriodBid.
     * 
     * @param elevatorName
     */
    public void setElevatorName(java.lang.String elevatorName) {
        this.elevatorName = elevatorName;
    }


    /**
     * Gets the vendCustType value for this PeriodBid.
     * 
     * @return vendCustType
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.VendorCustomerType getVendCustType() {
        return vendCustType;
    }


    /**
     * Sets the vendCustType value for this PeriodBid.
     * 
     * @param vendCustType
     */
    public void setVendCustType(com.adm.webpart.webservices.BW_Bids_Location_Info.VendorCustomerType vendCustType) {
        this.vendCustType = vendCustType;
    }


    /**
     * Gets the commodityId value for this PeriodBid.
     * 
     * @return commodityId
     */
    public java.lang.String getCommodityId() {
        return commodityId;
    }


    /**
     * Sets the commodityId value for this PeriodBid.
     * 
     * @param commodityId
     */
    public void setCommodityId(java.lang.String commodityId) {
        this.commodityId = commodityId;
    }


    /**
     * Gets the commodityCode value for this PeriodBid.
     * 
     * @return commodityCode
     */
    public java.lang.String getCommodityCode() {
        return commodityCode;
    }


    /**
     * Sets the commodityCode value for this PeriodBid.
     * 
     * @param commodityCode
     */
    public void setCommodityCode(java.lang.String commodityCode) {
        this.commodityCode = commodityCode;
    }


    /**
     * Gets the commodityDescription value for this PeriodBid.
     * 
     * @return commodityDescription
     */
    public java.lang.String getCommodityDescription() {
        return commodityDescription;
    }


    /**
     * Sets the commodityDescription value for this PeriodBid.
     * 
     * @param commodityDescription
     */
    public void setCommodityDescription(java.lang.String commodityDescription) {
        this.commodityDescription = commodityDescription;
    }


    /**
     * Gets the futuresSymbolRoot value for this PeriodBid.
     * 
     * @return futuresSymbolRoot
     */
    public java.lang.String getFuturesSymbolRoot() {
        return futuresSymbolRoot;
    }


    /**
     * Sets the futuresSymbolRoot value for this PeriodBid.
     * 
     * @param futuresSymbolRoot
     */
    public void setFuturesSymbolRoot(java.lang.String futuresSymbolRoot) {
        this.futuresSymbolRoot = futuresSymbolRoot;
    }


    /**
     * Gets the futuresSymbolFull value for this PeriodBid.
     * 
     * @return futuresSymbolFull
     */
    public java.lang.String getFuturesSymbolFull() {
        return futuresSymbolFull;
    }


    /**
     * Sets the futuresSymbolFull value for this PeriodBid.
     * 
     * @param futuresSymbolFull
     */
    public void setFuturesSymbolFull(java.lang.String futuresSymbolFull) {
        this.futuresSymbolFull = futuresSymbolFull;
    }


    /**
     * Gets the transportType value for this PeriodBid.
     * 
     * @return transportType
     */
    public java.lang.String getTransportType() {
        return transportType;
    }


    /**
     * Sets the transportType value for this PeriodBid.
     * 
     * @param transportType
     */
    public void setTransportType(java.lang.String transportType) {
        this.transportType = transportType;
    }


    /**
     * Gets the dealerProdType value for this PeriodBid.
     * 
     * @return dealerProdType
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.DealerProducerType getDealerProdType() {
        return dealerProdType;
    }


    /**
     * Sets the dealerProdType value for this PeriodBid.
     * 
     * @param dealerProdType
     */
    public void setDealerProdType(com.adm.webpart.webservices.BW_Bids_Location_Info.DealerProducerType dealerProdType) {
        this.dealerProdType = dealerProdType;
    }


    /**
     * Gets the futures value for this PeriodBid.
     * 
     * @return futures
     */
    public java.lang.String getFutures() {
        return futures;
    }


    /**
     * Sets the futures value for this PeriodBid.
     * 
     * @param futures
     */
    public void setFutures(java.lang.String futures) {
        this.futures = futures;
    }


    /**
     * Gets the basis value for this PeriodBid.
     * 
     * @return basis
     */
    public java.lang.String getBasis() {
        return basis;
    }


    /**
     * Sets the basis value for this PeriodBid.
     * 
     * @param basis
     */
    public void setBasis(java.lang.String basis) {
        this.basis = basis;
    }


    /**
     * Gets the flat value for this PeriodBid.
     * 
     * @return flat
     */
    public java.lang.String getFlat() {
        return flat;
    }


    /**
     * Sets the flat value for this PeriodBid.
     * 
     * @param flat
     */
    public void setFlat(java.lang.String flat) {
        this.flat = flat;
    }


    /**
     * Gets the flatCurrency value for this PeriodBid.
     * 
     * @return flatCurrency
     */
    public java.lang.String getFlatCurrency() {
        return flatCurrency;
    }


    /**
     * Sets the flatCurrency value for this PeriodBid.
     * 
     * @param flatCurrency
     */
    public void setFlatCurrency(java.lang.String flatCurrency) {
        this.flatCurrency = flatCurrency;
    }


    /**
     * Gets the entryType value for this PeriodBid.
     * 
     * @return entryType
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.BidsEntryType getEntryType() {
        return entryType;
    }


    /**
     * Sets the entryType value for this PeriodBid.
     * 
     * @param entryType
     */
    public void setEntryType(com.adm.webpart.webservices.BW_Bids_Location_Info.BidsEntryType entryType) {
        this.entryType = entryType;
    }


    /**
     * Gets the optionMonth value for this PeriodBid.
     * 
     * @return optionMonth
     */
    public java.lang.String getOptionMonth() {
        return optionMonth;
    }


    /**
     * Sets the optionMonth value for this PeriodBid.
     * 
     * @param optionMonth
     */
    public void setOptionMonth(java.lang.String optionMonth) {
        this.optionMonth = optionMonth;
    }


    /**
     * Gets the optionMonthCode value for this PeriodBid.
     * 
     * @return optionMonthCode
     */
    public java.lang.String getOptionMonthCode() {
        return optionMonthCode;
    }


    /**
     * Sets the optionMonthCode value for this PeriodBid.
     * 
     * @param optionMonthCode
     */
    public void setOptionMonthCode(java.lang.String optionMonthCode) {
        this.optionMonthCode = optionMonthCode;
    }


    /**
     * Gets the optionMonthNumber value for this PeriodBid.
     * 
     * @return optionMonthNumber
     */
    public int getOptionMonthNumber() {
        return optionMonthNumber;
    }


    /**
     * Sets the optionMonthNumber value for this PeriodBid.
     * 
     * @param optionMonthNumber
     */
    public void setOptionMonthNumber(int optionMonthNumber) {
        this.optionMonthNumber = optionMonthNumber;
    }


    /**
     * Gets the optionYear value for this PeriodBid.
     * 
     * @return optionYear
     */
    public int getOptionYear() {
        return optionYear;
    }


    /**
     * Sets the optionYear value for this PeriodBid.
     * 
     * @param optionYear
     */
    public void setOptionYear(int optionYear) {
        this.optionYear = optionYear;
    }


    /**
     * Gets the strOptionYear value for this PeriodBid.
     * 
     * @return strOptionYear
     */
    public java.lang.String getStrOptionYear() {
        return strOptionYear;
    }


    /**
     * Sets the strOptionYear value for this PeriodBid.
     * 
     * @param strOptionYear
     */
    public void setStrOptionYear(java.lang.String strOptionYear) {
        this.strOptionYear = strOptionYear;
    }


    /**
     * Gets the roundType value for this PeriodBid.
     * 
     * @return roundType
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.BidsRoundOption getRoundType() {
        return roundType;
    }


    /**
     * Sets the roundType value for this PeriodBid.
     * 
     * @param roundType
     */
    public void setRoundType(com.adm.webpart.webservices.BW_Bids_Location_Info.BidsRoundOption roundType) {
        this.roundType = roundType;
    }


    /**
     * Gets the displayOnWeb value for this PeriodBid.
     * 
     * @return displayOnWeb
     */
    public boolean isDisplayOnWeb() {
        return displayOnWeb;
    }


    /**
     * Sets the displayOnWeb value for this PeriodBid.
     * 
     * @param displayOnWeb
     */
    public void setDisplayOnWeb(boolean displayOnWeb) {
        this.displayOnWeb = displayOnWeb;
    }


    /**
     * Gets the displayInDTN value for this PeriodBid.
     * 
     * @return displayInDTN
     */
    public boolean isDisplayInDTN() {
        return displayInDTN;
    }


    /**
     * Sets the displayInDTN value for this PeriodBid.
     * 
     * @param displayInDTN
     */
    public void setDisplayInDTN(boolean displayInDTN) {
        this.displayInDTN = displayInDTN;
    }


    /**
     * Gets the currencyCode value for this PeriodBid.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this PeriodBid.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the marketCode value for this PeriodBid.
     * 
     * @return marketCode
     */
    public java.lang.String getMarketCode() {
        return marketCode;
    }


    /**
     * Sets the marketCode value for this PeriodBid.
     * 
     * @param marketCode
     */
    public void setMarketCode(java.lang.String marketCode) {
        this.marketCode = marketCode;
    }


    /**
     * Gets the settled value for this PeriodBid.
     * 
     * @return settled
     */
    public boolean isSettled() {
        return settled;
    }


    /**
     * Sets the settled value for this PeriodBid.
     * 
     * @param settled
     */
    public void setSettled(boolean settled) {
        this.settled = settled;
    }


    /**
     * Gets the strVendCustType value for this PeriodBid.
     * 
     * @return strVendCustType
     */
    public java.lang.String getStrVendCustType() {
        return strVendCustType;
    }


    /**
     * Sets the strVendCustType value for this PeriodBid.
     * 
     * @param strVendCustType
     */
    public void setStrVendCustType(java.lang.String strVendCustType) {
        this.strVendCustType = strVendCustType;
    }


    /**
     * Gets the commodity value for this PeriodBid.
     * 
     * @return commodity
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Commodity getCommodity() {
        return commodity;
    }


    /**
     * Sets the commodity value for this PeriodBid.
     * 
     * @param commodity
     */
    public void setCommodity(com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Commodity commodity) {
        this.commodity = commodity;
    }


    /**
     * Gets the market value for this PeriodBid.
     * 
     * @return market
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Market getMarket() {
        return market;
    }


    /**
     * Sets the market value for this PeriodBid.
     * 
     * @param market
     */
    public void setMarket(com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Market market) {
        this.market = market;
    }


    /**
     * Gets the unitOfMeasure value for this PeriodBid.
     * 
     * @return unitOfMeasure
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_UnitOfMeasure getUnitOfMeasure() {
        return unitOfMeasure;
    }


    /**
     * Sets the unitOfMeasure value for this PeriodBid.
     * 
     * @param unitOfMeasure
     */
    public void setUnitOfMeasure(com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_UnitOfMeasure unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }


    /**
     * Gets the currency value for this PeriodBid.
     * 
     * @return currency
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Currency getCurrency() {
        return currency;
    }


    /**
     * Sets the currency value for this PeriodBid.
     * 
     * @param currency
     */
    public void setCurrency(com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Currency currency) {
        this.currency = currency;
    }


    /**
     * Gets the basisOrFlat value for this PeriodBid.
     * 
     * @return basisOrFlat
     */
    public java.lang.String getBasisOrFlat() {
        return basisOrFlat;
    }


    /**
     * Sets the basisOrFlat value for this PeriodBid.
     * 
     * @param basisOrFlat
     */
    public void setBasisOrFlat(java.lang.String basisOrFlat) {
        this.basisOrFlat = basisOrFlat;
    }


    /**
     * Gets the _final value for this PeriodBid.
     * 
     * @return _final
     */
    public java.lang.String get_final() {
        return _final;
    }


    /**
     * Sets the _final value for this PeriodBid.
     * 
     * @param _final
     */
    public void set_final(java.lang.String _final) {
        this._final = _final;
    }


    /**
     * Gets the trans value for this PeriodBid.
     * 
     * @return trans
     */
    public java.lang.String getTrans() {
        return trans;
    }


    /**
     * Sets the trans value for this PeriodBid.
     * 
     * @param trans
     */
    public void setTrans(java.lang.String trans) {
        this.trans = trans;
    }


    /**
     * Gets the wasEnteredThroughGlobalBidsSystem value for this PeriodBid.
     * 
     * @return wasEnteredThroughGlobalBidsSystem
     */
    public boolean isWasEnteredThroughGlobalBidsSystem() {
        return wasEnteredThroughGlobalBidsSystem;
    }


    /**
     * Sets the wasEnteredThroughGlobalBidsSystem value for this PeriodBid.
     * 
     * @param wasEnteredThroughGlobalBidsSystem
     */
    public void setWasEnteredThroughGlobalBidsSystem(boolean wasEnteredThroughGlobalBidsSystem) {
        this.wasEnteredThroughGlobalBidsSystem = wasEnteredThroughGlobalBidsSystem;
    }


    /**
     * Gets the strGroupInitializationMode value for this PeriodBid.
     * 
     * @return strGroupInitializationMode
     */
    public java.lang.String getStrGroupInitializationMode() {
        return strGroupInitializationMode;
    }


    /**
     * Sets the strGroupInitializationMode value for this PeriodBid.
     * 
     * @param strGroupInitializationMode
     */
    public void setStrGroupInitializationMode(java.lang.String strGroupInitializationMode) {
        this.strGroupInitializationMode = strGroupInitializationMode;
    }


    /**
     * Gets the isInGroupInitializationMode value for this PeriodBid.
     * 
     * @return isInGroupInitializationMode
     */
    public boolean isIsInGroupInitializationMode() {
        return isInGroupInitializationMode;
    }


    /**
     * Sets the isInGroupInitializationMode value for this PeriodBid.
     * 
     * @param isInGroupInitializationMode
     */
    public void setIsInGroupInitializationMode(boolean isInGroupInitializationMode) {
        this.isInGroupInitializationMode = isInGroupInitializationMode;
    }


    /**
     * Gets the DTNBidContractID value for this PeriodBid.
     * 
     * @return DTNBidContractID
     */
    public int getDTNBidContractID() {
        return DTNBidContractID;
    }


    /**
     * Sets the DTNBidContractID value for this PeriodBid.
     * 
     * @param DTNBidContractID
     */
    public void setDTNBidContractID(int DTNBidContractID) {
        this.DTNBidContractID = DTNBidContractID;
    }


    /**
     * Gets the DTNDeliveryLabel value for this PeriodBid.
     * 
     * @return DTNDeliveryLabel
     */
    public java.lang.String getDTNDeliveryLabel() {
        return DTNDeliveryLabel;
    }


    /**
     * Sets the DTNDeliveryLabel value for this PeriodBid.
     * 
     * @param DTNDeliveryLabel
     */
    public void setDTNDeliveryLabel(java.lang.String DTNDeliveryLabel) {
        this.DTNDeliveryLabel = DTNDeliveryLabel;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PeriodBid)) return false;
        PeriodBid other = (PeriodBid) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.groupId == other.getGroupId() &&
            this.periodId == other.getPeriodId() &&
            ((this.beginDate==null && other.getBeginDate()==null) || 
             (this.beginDate!=null &&
              this.beginDate.equals(other.getBeginDate()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.entryDate==null && other.getEntryDate()==null) || 
             (this.entryDate!=null &&
              this.entryDate.equals(other.getEntryDate()))) &&
            this.elevatorId == other.getElevatorId() &&
            ((this.elevatorName==null && other.getElevatorName()==null) || 
             (this.elevatorName!=null &&
              this.elevatorName.equals(other.getElevatorName()))) &&
            ((this.vendCustType==null && other.getVendCustType()==null) || 
             (this.vendCustType!=null &&
              this.vendCustType.equals(other.getVendCustType()))) &&
            ((this.commodityId==null && other.getCommodityId()==null) || 
             (this.commodityId!=null &&
              this.commodityId.equals(other.getCommodityId()))) &&
            ((this.commodityCode==null && other.getCommodityCode()==null) || 
             (this.commodityCode!=null &&
              this.commodityCode.equals(other.getCommodityCode()))) &&
            ((this.commodityDescription==null && other.getCommodityDescription()==null) || 
             (this.commodityDescription!=null &&
              this.commodityDescription.equals(other.getCommodityDescription()))) &&
            ((this.futuresSymbolRoot==null && other.getFuturesSymbolRoot()==null) || 
             (this.futuresSymbolRoot!=null &&
              this.futuresSymbolRoot.equals(other.getFuturesSymbolRoot()))) &&
            ((this.futuresSymbolFull==null && other.getFuturesSymbolFull()==null) || 
             (this.futuresSymbolFull!=null &&
              this.futuresSymbolFull.equals(other.getFuturesSymbolFull()))) &&
            ((this.transportType==null && other.getTransportType()==null) || 
             (this.transportType!=null &&
              this.transportType.equals(other.getTransportType()))) &&
            ((this.dealerProdType==null && other.getDealerProdType()==null) || 
             (this.dealerProdType!=null &&
              this.dealerProdType.equals(other.getDealerProdType()))) &&
            ((this.futures==null && other.getFutures()==null) || 
             (this.futures!=null &&
              this.futures.equals(other.getFutures()))) &&
            ((this.basis==null && other.getBasis()==null) || 
             (this.basis!=null &&
              this.basis.equals(other.getBasis()))) &&
            ((this.flat==null && other.getFlat()==null) || 
             (this.flat!=null &&
              this.flat.equals(other.getFlat()))) &&
            ((this.flatCurrency==null && other.getFlatCurrency()==null) || 
             (this.flatCurrency!=null &&
              this.flatCurrency.equals(other.getFlatCurrency()))) &&
            ((this.entryType==null && other.getEntryType()==null) || 
             (this.entryType!=null &&
              this.entryType.equals(other.getEntryType()))) &&
            ((this.optionMonth==null && other.getOptionMonth()==null) || 
             (this.optionMonth!=null &&
              this.optionMonth.equals(other.getOptionMonth()))) &&
            ((this.optionMonthCode==null && other.getOptionMonthCode()==null) || 
             (this.optionMonthCode!=null &&
              this.optionMonthCode.equals(other.getOptionMonthCode()))) &&
            this.optionMonthNumber == other.getOptionMonthNumber() &&
            this.optionYear == other.getOptionYear() &&
            ((this.strOptionYear==null && other.getStrOptionYear()==null) || 
             (this.strOptionYear!=null &&
              this.strOptionYear.equals(other.getStrOptionYear()))) &&
            ((this.roundType==null && other.getRoundType()==null) || 
             (this.roundType!=null &&
              this.roundType.equals(other.getRoundType()))) &&
            this.displayOnWeb == other.isDisplayOnWeb() &&
            this.displayInDTN == other.isDisplayInDTN() &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.marketCode==null && other.getMarketCode()==null) || 
             (this.marketCode!=null &&
              this.marketCode.equals(other.getMarketCode()))) &&
            this.settled == other.isSettled() &&
            ((this.strVendCustType==null && other.getStrVendCustType()==null) || 
             (this.strVendCustType!=null &&
              this.strVendCustType.equals(other.getStrVendCustType()))) &&
            ((this.commodity==null && other.getCommodity()==null) || 
             (this.commodity!=null &&
              this.commodity.equals(other.getCommodity()))) &&
            ((this.market==null && other.getMarket()==null) || 
             (this.market!=null &&
              this.market.equals(other.getMarket()))) &&
            ((this.unitOfMeasure==null && other.getUnitOfMeasure()==null) || 
             (this.unitOfMeasure!=null &&
              this.unitOfMeasure.equals(other.getUnitOfMeasure()))) &&
            ((this.currency==null && other.getCurrency()==null) || 
             (this.currency!=null &&
              this.currency.equals(other.getCurrency()))) &&
            ((this.basisOrFlat==null && other.getBasisOrFlat()==null) || 
             (this.basisOrFlat!=null &&
              this.basisOrFlat.equals(other.getBasisOrFlat()))) &&
            ((this._final==null && other.get_final()==null) || 
             (this._final!=null &&
              this._final.equals(other.get_final()))) &&
            ((this.trans==null && other.getTrans()==null) || 
             (this.trans!=null &&
              this.trans.equals(other.getTrans()))) &&
            this.wasEnteredThroughGlobalBidsSystem == other.isWasEnteredThroughGlobalBidsSystem() &&
            ((this.strGroupInitializationMode==null && other.getStrGroupInitializationMode()==null) || 
             (this.strGroupInitializationMode!=null &&
              this.strGroupInitializationMode.equals(other.getStrGroupInitializationMode()))) &&
            this.isInGroupInitializationMode == other.isIsInGroupInitializationMode() &&
            this.DTNBidContractID == other.getDTNBidContractID() &&
            ((this.DTNDeliveryLabel==null && other.getDTNDeliveryLabel()==null) || 
             (this.DTNDeliveryLabel!=null &&
              this.DTNDeliveryLabel.equals(other.getDTNDeliveryLabel())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getGroupId();
        _hashCode += getPeriodId();
        if (getBeginDate() != null) {
            _hashCode += getBeginDate().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getEntryDate() != null) {
            _hashCode += getEntryDate().hashCode();
        }
        _hashCode += getElevatorId();
        if (getElevatorName() != null) {
            _hashCode += getElevatorName().hashCode();
        }
        if (getVendCustType() != null) {
            _hashCode += getVendCustType().hashCode();
        }
        if (getCommodityId() != null) {
            _hashCode += getCommodityId().hashCode();
        }
        if (getCommodityCode() != null) {
            _hashCode += getCommodityCode().hashCode();
        }
        if (getCommodityDescription() != null) {
            _hashCode += getCommodityDescription().hashCode();
        }
        if (getFuturesSymbolRoot() != null) {
            _hashCode += getFuturesSymbolRoot().hashCode();
        }
        if (getFuturesSymbolFull() != null) {
            _hashCode += getFuturesSymbolFull().hashCode();
        }
        if (getTransportType() != null) {
            _hashCode += getTransportType().hashCode();
        }
        if (getDealerProdType() != null) {
            _hashCode += getDealerProdType().hashCode();
        }
        if (getFutures() != null) {
            _hashCode += getFutures().hashCode();
        }
        if (getBasis() != null) {
            _hashCode += getBasis().hashCode();
        }
        if (getFlat() != null) {
            _hashCode += getFlat().hashCode();
        }
        if (getFlatCurrency() != null) {
            _hashCode += getFlatCurrency().hashCode();
        }
        if (getEntryType() != null) {
            _hashCode += getEntryType().hashCode();
        }
        if (getOptionMonth() != null) {
            _hashCode += getOptionMonth().hashCode();
        }
        if (getOptionMonthCode() != null) {
            _hashCode += getOptionMonthCode().hashCode();
        }
        _hashCode += getOptionMonthNumber();
        _hashCode += getOptionYear();
        if (getStrOptionYear() != null) {
            _hashCode += getStrOptionYear().hashCode();
        }
        if (getRoundType() != null) {
            _hashCode += getRoundType().hashCode();
        }
        _hashCode += (isDisplayOnWeb() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isDisplayInDTN() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getMarketCode() != null) {
            _hashCode += getMarketCode().hashCode();
        }
        _hashCode += (isSettled() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getStrVendCustType() != null) {
            _hashCode += getStrVendCustType().hashCode();
        }
        if (getCommodity() != null) {
            _hashCode += getCommodity().hashCode();
        }
        if (getMarket() != null) {
            _hashCode += getMarket().hashCode();
        }
        if (getUnitOfMeasure() != null) {
            _hashCode += getUnitOfMeasure().hashCode();
        }
        if (getCurrency() != null) {
            _hashCode += getCurrency().hashCode();
        }
        if (getBasisOrFlat() != null) {
            _hashCode += getBasisOrFlat().hashCode();
        }
        if (get_final() != null) {
            _hashCode += get_final().hashCode();
        }
        if (getTrans() != null) {
            _hashCode += getTrans().hashCode();
        }
        _hashCode += (isWasEnteredThroughGlobalBidsSystem() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getStrGroupInitializationMode() != null) {
            _hashCode += getStrGroupInitializationMode().hashCode();
        }
        _hashCode += (isIsInGroupInitializationMode() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += getDTNBidContractID();
        if (getDTNDeliveryLabel() != null) {
            _hashCode += getDTNDeliveryLabel().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PeriodBid.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBid"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GroupId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("periodId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BeginDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "EndDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entryDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "EntryDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("elevatorId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ElevatorId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("elevatorName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ElevatorName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendCustType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "VendCustType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "VendorCustomerType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commodityId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "CommodityId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commodityCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "CommodityCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commodityDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "CommodityDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("futuresSymbolRoot");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "FuturesSymbolRoot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("futuresSymbolFull");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "FuturesSymbolFull"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transportType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "TransportType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerProdType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DealerProdType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DealerProducerType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("futures");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Futures"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("basis");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Basis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Flat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flatCurrency");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "FlatCurrency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entryType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "EntryType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BidsEntryType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("optionMonth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "OptionMonth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("optionMonthCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "OptionMonthCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("optionMonthNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "OptionMonthNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("optionYear");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "OptionYear"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strOptionYear");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "StrOptionYear"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("roundType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "RoundType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BidsRoundOption"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayOnWeb");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DisplayOnWeb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayInDTN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DisplayInDTN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marketCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "MarketCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("settled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Settled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strVendCustType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "StrVendCustType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commodity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Commodity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Commodity"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("market");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Market"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Market"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitOfMeasure");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "UnitOfMeasure"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_UnitOfMeasure"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Currency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Currency"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("basisOrFlat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BasisOrFlat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("_final");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Final"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trans");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Trans"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("wasEnteredThroughGlobalBidsSystem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "WasEnteredThroughGlobalBidsSystem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strGroupInitializationMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "StrGroupInitializationMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isInGroupInitializationMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "IsInGroupInitializationMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DTNBidContractID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DTNBidContractID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DTNDeliveryLabel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DTNDeliveryLabel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	@Override
	public String toString() {
		return "PeriodBid [groupId=" + groupId + ", periodId=" + periodId + ", beginDate=" + beginDate + ", endDate="
				+ endDate + ", entryDate=" + entryDate + ", elevatorId=" + elevatorId + ", elevatorName=" + elevatorName
				+ ", vendCustType=" + vendCustType + ", commodityId=" + commodityId + ", commodityCode=" + commodityCode
				+ ", commodityDescription=" + commodityDescription + ", futuresSymbolRoot=" + futuresSymbolRoot
				+ ", futuresSymbolFull=" + futuresSymbolFull + ", transportType=" + transportType + ", dealerProdType="
				+ dealerProdType + ", futures=" + futures + ", basis=" + basis + ", flat=" + flat + ", flatCurrency="
				+ flatCurrency + ", entryType=" + entryType + ", optionMonth=" + optionMonth + ", optionMonthCode="
				+ optionMonthCode + ", optionMonthNumber=" + optionMonthNumber + ", optionYear=" + optionYear
				+ ", strOptionYear=" + strOptionYear + ", roundType=" + roundType + ", displayOnWeb=" + displayOnWeb
				+ ", displayInDTN=" + displayInDTN + ", currencyCode=" + currencyCode + ", marketCode=" + marketCode
				+ ", settled=" + settled + ", strVendCustType=" + strVendCustType + ", commodity=" + commodity
				+ ", market=" + market + ", unitOfMeasure=" + unitOfMeasure + ", currency=" + currency
				+ ", basisOrFlat=" + basisOrFlat + ", _final=" + _final + ", trans=" + trans
				+ ", wasEnteredThroughGlobalBidsSystem=" + wasEnteredThroughGlobalBidsSystem
				+ ", strGroupInitializationMode=" + strGroupInitializationMode + ", isInGroupInitializationMode="
				+ isInGroupInitializationMode + ", DTNBidContractID=" + DTNBidContractID + ", DTNDeliveryLabel="
				+ DTNDeliveryLabel + ", __equalsCalc=" + __equalsCalc + ", __hashCodeCalc=" + __hashCodeCalc + "]";
	}

}
