/**
 * BidsRoundOption.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Bids_Location_Info;

public class BidsRoundOption implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected BidsRoundOption(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Actual = "Actual";
    public static final java.lang.String _Up = "Up";
    public static final java.lang.String _Down = "Down";
    public static final java.lang.String _Whole = "Whole";
    public static final java.lang.String _None = "None";
    public static final BidsRoundOption Actual = new BidsRoundOption(_Actual);
    public static final BidsRoundOption Up = new BidsRoundOption(_Up);
    public static final BidsRoundOption Down = new BidsRoundOption(_Down);
    public static final BidsRoundOption Whole = new BidsRoundOption(_Whole);
    public static final BidsRoundOption None = new BidsRoundOption(_None);
    public java.lang.String getValue() { return _value_;}
    public static BidsRoundOption fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        BidsRoundOption enumeration = (BidsRoundOption)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static BidsRoundOption fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BidsRoundOption.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BidsRoundOption"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
