package com.adm.webpart.webservices.BW_Bids_Location_Info;

public class BW_Bids_Location_InfoSoapProxy implements com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoap {
  private String _endpoint = null;
  private com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoap bW_Bids_Location_InfoSoap = null;
  
  public BW_Bids_Location_InfoSoapProxy() {
    _initBW_Bids_Location_InfoSoapProxy();
  }
  
  public BW_Bids_Location_InfoSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initBW_Bids_Location_InfoSoapProxy();
  }
  
  private void _initBW_Bids_Location_InfoSoapProxy() {
    try {
      bW_Bids_Location_InfoSoap = (new com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoLocator()).getBW_Bids_Location_InfoSoap();
      if (bW_Bids_Location_InfoSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bW_Bids_Location_InfoSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bW_Bids_Location_InfoSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bW_Bids_Location_InfoSoap != null)
      ((javax.xml.rpc.Stub)bW_Bids_Location_InfoSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoap getBW_Bids_Location_InfoSoap() {
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap;
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBidCommodity[] getCommodityBidsForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getCommodityBidsForLocation(locationId, languageCode);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getBidsAndOffersForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getBidsAndOffersForLocation(locationId, languageCode);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getDisplayBidsAndOffersForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getDisplayBidsAndOffersForLocation(locationId, languageCode);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getBidsForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getBidsForLocation(locationId, languageCode);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getDisplayBidsForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getDisplayBidsForLocation(locationId, languageCode);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getOffersForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getOffersForLocation(locationId, languageCode);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getDisplayOffersForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getDisplayOffersForLocation(locationId, languageCode);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getBidsForLocations(java.lang.String locationIds, java.lang.String languageCode) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getBidsForLocations(locationIds, languageCode);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.LocationBidCount[] getBidCountForLocations(java.lang.String locationIds) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getBidCountForLocations(locationIds);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getHistoricalBidsAndOffersForLocation(java.lang.String locationId, java.lang.String languageCode, java.lang.String date) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getHistoricalBidsAndOffersForLocation(locationId, languageCode, date);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getHistoricalBidsForLocation(java.lang.String locationId, java.lang.String languageCode, java.lang.String date) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getHistoricalBidsForLocation(locationId, languageCode, date);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getHistoricalOffersForLocation(java.lang.String locationId, java.lang.String languageCode, java.lang.String date) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getHistoricalOffersForLocation(locationId, languageCode, date);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator getDetailedElevatorInformation(java.lang.String elevatorID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getDetailedElevatorInformation(elevatorID);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getDTNBidsAndOffersForLocation(java.lang.String locationId) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getDTNBidsAndOffersForLocation(locationId);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getBidsAndOffersByLocation(int locationID, java.lang.String dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getBidsAndOffersByLocation(locationID, dealerProducerCode, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getBidsByLocation(int locationID, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getBidsByLocation(locationID, dealerProducerCode, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getOffersByLocation(int locationID, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getOffersByLocation(locationID, dealerProducerCode, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getHistoricBidsAndOffersByLocation(int locationID, java.lang.String date, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getHistoricBidsAndOffersByLocation(locationID, date, dealerProducerCode, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getHistoricBidsByLocation(int locationID, java.lang.String date, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getHistoricBidsByLocation(locationID, date, dealerProducerCode, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getHistoricOffersByLocation(int locationID, java.lang.String date, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getHistoricOffersByLocation(locationID, date, dealerProducerCode, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getFarmerViewBidsByLocation(int locationID, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getFarmerViewBidsByLocation(locationID, dealerProducerCode, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getFarmerViewOffersByLocation(int locationID, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getFarmerViewOffersByLocation(locationID, dealerProducerCode, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.BidCommodity[] getLocationBids(java.lang.String locationID, org.apache.axis.types.UnsignedShort dealerProducerCode) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getLocationBids(locationID, dealerProducerCode);
  }
  
  public java.lang.String[] getStateProvinceWithLocations(java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getStateProvinceWithLocations(subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator[] getADMLocationsByState(java.lang.String stateProvince, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getADMLocationsByState(stateProvince, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator getLocationByID(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getLocationByID(locationID, subscriptionID);
  }
  
  public java.lang.String getBidReplyToEmail(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getBidReplyToEmail(locationID, subscriptionID);
  }
  
  public java.lang.String getLocationHours(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getLocationHours(locationID, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.BidLocation getBidLocationInfo(java.lang.String locationID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getBidLocationInfo(locationID);
  }
  
  public java.lang.String getLocationManagerEmail(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getLocationManagerEmail(locationID, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.Admin[] getElevatorAdmins(java.lang.String locationId) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getElevatorAdmins(locationId);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.Contact[] getLocationContacts(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getLocationContacts(locationID, subscriptionID);
  }
  
  public byte[] getContactPhoto(java.lang.String staffIDs) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getContactPhoto(staffIDs);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.DiscountSchedule[] getDSFileInfo(java.lang.String locationID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getDSFileInfo(locationID);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.LocationDocument[] getLocationDocumentFile(java.lang.String locationID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getLocationDocumentFile(locationID);
  }
  
  public com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Announcement[] getAllLocationAnnouncements(java.lang.String locationID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getAllLocationAnnouncements(locationID);
  }
  
  public java.lang.String getLocationAnnouncements(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getLocationAnnouncements(locationID, subscriptionID);
  }
  
  public java.lang.String getLocationComments(int locationID, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Bids_Location_InfoSoap == null)
      _initBW_Bids_Location_InfoSoapProxy();
    return bW_Bids_Location_InfoSoap.getLocationComments(locationID, dealerProducerCode, subscriptionID);
  }
  
  
}