/**
 * Elevator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Bids_Location_Info;

public class Elevator  implements java.io.Serializable {
    private int ID;

    private java.lang.String code;

    private java.lang.String name;

    private java.lang.String longName;

    private java.lang.String code2;

    private java.lang.String shp;

    private java.lang.String freight;

    private java.lang.String name2;

    private java.lang.String groupID;

    private java.lang.String longName2;

    private java.lang.String addr1;

    private java.lang.String addr2;

    private java.lang.String addr3;

    private java.lang.String addr4;

    private java.lang.String city;

    private java.lang.String state;

    private java.lang.String country;

    private java.lang.String zip;

    private java.lang.String hours;

    private java.lang.String PComments;

    private java.lang.String phone1;

    private java.lang.String phone2;

    private java.lang.String fax;

    private java.lang.String imageName;

    private java.lang.String x;

    private java.lang.String y;

    private java.lang.String manager;

    private java.lang.String email;

    private java.lang.String mgrLogon;

    private java.lang.String IBid;

    private java.util.Calendar lastUpdate;

    private java.lang.String strLastUpdate;

    private java.lang.String timeZone;

    private java.lang.String strIsActive;

    private boolean isActive;

    private java.lang.String companyID;

    private java.lang.String special;

    private java.lang.String map;

    private java.lang.String DComments;

    private java.lang.String destCode;

    private java.lang.String freightSecondary;

    private java.lang.String returnAddr;

    private java.lang.String replyEmail;

    private java.lang.String reMailIsValid;

    private float latitude;

    private float longitude;

    private int hits;

    private java.lang.String unloadHours;

    private java.lang.String strDTNSend;

    private boolean DTNSend;

    private java.lang.String DTNElevID;

    private java.lang.String DTNPortalID;

    private com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Company company;

    private com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Group group;

    public Elevator() {
    }

    public Elevator(
           int ID,
           java.lang.String code,
           java.lang.String name,
           java.lang.String longName,
           java.lang.String code2,
           java.lang.String shp,
           java.lang.String freight,
           java.lang.String name2,
           java.lang.String groupID,
           java.lang.String longName2,
           java.lang.String addr1,
           java.lang.String addr2,
           java.lang.String addr3,
           java.lang.String addr4,
           java.lang.String city,
           java.lang.String state,
           java.lang.String country,
           java.lang.String zip,
           java.lang.String hours,
           java.lang.String PComments,
           java.lang.String phone1,
           java.lang.String phone2,
           java.lang.String fax,
           java.lang.String imageName,
           java.lang.String x,
           java.lang.String y,
           java.lang.String manager,
           java.lang.String email,
           java.lang.String mgrLogon,
           java.lang.String IBid,
           java.util.Calendar lastUpdate,
           java.lang.String strLastUpdate,
           java.lang.String timeZone,
           java.lang.String strIsActive,
           boolean isActive,
           java.lang.String companyID,
           java.lang.String special,
           java.lang.String map,
           java.lang.String DComments,
           java.lang.String destCode,
           java.lang.String freightSecondary,
           java.lang.String returnAddr,
           java.lang.String replyEmail,
           java.lang.String reMailIsValid,
           float latitude,
           float longitude,
           int hits,
           java.lang.String unloadHours,
           java.lang.String strDTNSend,
           boolean DTNSend,
           java.lang.String DTNElevID,
           java.lang.String DTNPortalID,
           com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Company company,
           com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Group group) {
           this.ID = ID;
           this.code = code;
           this.name = name;
           this.longName = longName;
           this.code2 = code2;
           this.shp = shp;
           this.freight = freight;
           this.name2 = name2;
           this.groupID = groupID;
           this.longName2 = longName2;
           this.addr1 = addr1;
           this.addr2 = addr2;
           this.addr3 = addr3;
           this.addr4 = addr4;
           this.city = city;
           this.state = state;
           this.country = country;
           this.zip = zip;
           this.hours = hours;
           this.PComments = PComments;
           this.phone1 = phone1;
           this.phone2 = phone2;
           this.fax = fax;
           this.imageName = imageName;
           this.x = x;
           this.y = y;
           this.manager = manager;
           this.email = email;
           this.mgrLogon = mgrLogon;
           this.IBid = IBid;
           this.lastUpdate = lastUpdate;
           this.strLastUpdate = strLastUpdate;
           this.timeZone = timeZone;
           this.strIsActive = strIsActive;
           this.isActive = isActive;
           this.companyID = companyID;
           this.special = special;
           this.map = map;
           this.DComments = DComments;
           this.destCode = destCode;
           this.freightSecondary = freightSecondary;
           this.returnAddr = returnAddr;
           this.replyEmail = replyEmail;
           this.reMailIsValid = reMailIsValid;
           this.latitude = latitude;
           this.longitude = longitude;
           this.hits = hits;
           this.unloadHours = unloadHours;
           this.strDTNSend = strDTNSend;
           this.DTNSend = DTNSend;
           this.DTNElevID = DTNElevID;
           this.DTNPortalID = DTNPortalID;
           this.company = company;
           this.group = group;
    }


    /**
     * Gets the ID value for this Elevator.
     * 
     * @return ID
     */
    public int getID() {
        return ID;
    }


    /**
     * Sets the ID value for this Elevator.
     * 
     * @param ID
     */
    public void setID(int ID) {
        this.ID = ID;
    }


    /**
     * Gets the code value for this Elevator.
     * 
     * @return code
     */
    public java.lang.String getCode() {
        return code;
    }


    /**
     * Sets the code value for this Elevator.
     * 
     * @param code
     */
    public void setCode(java.lang.String code) {
        this.code = code;
    }


    /**
     * Gets the name value for this Elevator.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this Elevator.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the longName value for this Elevator.
     * 
     * @return longName
     */
    public java.lang.String getLongName() {
        return longName;
    }


    /**
     * Sets the longName value for this Elevator.
     * 
     * @param longName
     */
    public void setLongName(java.lang.String longName) {
        this.longName = longName;
    }


    /**
     * Gets the code2 value for this Elevator.
     * 
     * @return code2
     */
    public java.lang.String getCode2() {
        return code2;
    }


    /**
     * Sets the code2 value for this Elevator.
     * 
     * @param code2
     */
    public void setCode2(java.lang.String code2) {
        this.code2 = code2;
    }


    /**
     * Gets the shp value for this Elevator.
     * 
     * @return shp
     */
    public java.lang.String getShp() {
        return shp;
    }


    /**
     * Sets the shp value for this Elevator.
     * 
     * @param shp
     */
    public void setShp(java.lang.String shp) {
        this.shp = shp;
    }


    /**
     * Gets the freight value for this Elevator.
     * 
     * @return freight
     */
    public java.lang.String getFreight() {
        return freight;
    }


    /**
     * Sets the freight value for this Elevator.
     * 
     * @param freight
     */
    public void setFreight(java.lang.String freight) {
        this.freight = freight;
    }


    /**
     * Gets the name2 value for this Elevator.
     * 
     * @return name2
     */
    public java.lang.String getName2() {
        return name2;
    }


    /**
     * Sets the name2 value for this Elevator.
     * 
     * @param name2
     */
    public void setName2(java.lang.String name2) {
        this.name2 = name2;
    }


    /**
     * Gets the groupID value for this Elevator.
     * 
     * @return groupID
     */
    public java.lang.String getGroupID() {
        return groupID;
    }


    /**
     * Sets the groupID value for this Elevator.
     * 
     * @param groupID
     */
    public void setGroupID(java.lang.String groupID) {
        this.groupID = groupID;
    }


    /**
     * Gets the longName2 value for this Elevator.
     * 
     * @return longName2
     */
    public java.lang.String getLongName2() {
        return longName2;
    }


    /**
     * Sets the longName2 value for this Elevator.
     * 
     * @param longName2
     */
    public void setLongName2(java.lang.String longName2) {
        this.longName2 = longName2;
    }


    /**
     * Gets the addr1 value for this Elevator.
     * 
     * @return addr1
     */
    public java.lang.String getAddr1() {
        return addr1;
    }


    /**
     * Sets the addr1 value for this Elevator.
     * 
     * @param addr1
     */
    public void setAddr1(java.lang.String addr1) {
        this.addr1 = addr1;
    }


    /**
     * Gets the addr2 value for this Elevator.
     * 
     * @return addr2
     */
    public java.lang.String getAddr2() {
        return addr2;
    }


    /**
     * Sets the addr2 value for this Elevator.
     * 
     * @param addr2
     */
    public void setAddr2(java.lang.String addr2) {
        this.addr2 = addr2;
    }


    /**
     * Gets the addr3 value for this Elevator.
     * 
     * @return addr3
     */
    public java.lang.String getAddr3() {
        return addr3;
    }


    /**
     * Sets the addr3 value for this Elevator.
     * 
     * @param addr3
     */
    public void setAddr3(java.lang.String addr3) {
        this.addr3 = addr3;
    }


    /**
     * Gets the addr4 value for this Elevator.
     * 
     * @return addr4
     */
    public java.lang.String getAddr4() {
        return addr4;
    }


    /**
     * Sets the addr4 value for this Elevator.
     * 
     * @param addr4
     */
    public void setAddr4(java.lang.String addr4) {
        this.addr4 = addr4;
    }


    /**
     * Gets the city value for this Elevator.
     * 
     * @return city
     */
    public java.lang.String getCity() {
        return city;
    }


    /**
     * Sets the city value for this Elevator.
     * 
     * @param city
     */
    public void setCity(java.lang.String city) {
        this.city = city;
    }


    /**
     * Gets the state value for this Elevator.
     * 
     * @return state
     */
    public java.lang.String getState() {
        return state;
    }


    /**
     * Sets the state value for this Elevator.
     * 
     * @param state
     */
    public void setState(java.lang.String state) {
        this.state = state;
    }


    /**
     * Gets the country value for this Elevator.
     * 
     * @return country
     */
    public java.lang.String getCountry() {
        return country;
    }


    /**
     * Sets the country value for this Elevator.
     * 
     * @param country
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }


    /**
     * Gets the zip value for this Elevator.
     * 
     * @return zip
     */
    public java.lang.String getZip() {
        return zip;
    }


    /**
     * Sets the zip value for this Elevator.
     * 
     * @param zip
     */
    public void setZip(java.lang.String zip) {
        this.zip = zip;
    }


    /**
     * Gets the hours value for this Elevator.
     * 
     * @return hours
     */
    public java.lang.String getHours() {
        return hours;
    }


    /**
     * Sets the hours value for this Elevator.
     * 
     * @param hours
     */
    public void setHours(java.lang.String hours) {
        this.hours = hours;
    }


    /**
     * Gets the PComments value for this Elevator.
     * 
     * @return PComments
     */
    public java.lang.String getPComments() {
        return PComments;
    }


    /**
     * Sets the PComments value for this Elevator.
     * 
     * @param PComments
     */
    public void setPComments(java.lang.String PComments) {
        this.PComments = PComments;
    }


    /**
     * Gets the phone1 value for this Elevator.
     * 
     * @return phone1
     */
    public java.lang.String getPhone1() {
        return phone1;
    }


    /**
     * Sets the phone1 value for this Elevator.
     * 
     * @param phone1
     */
    public void setPhone1(java.lang.String phone1) {
        this.phone1 = phone1;
    }


    /**
     * Gets the phone2 value for this Elevator.
     * 
     * @return phone2
     */
    public java.lang.String getPhone2() {
        return phone2;
    }


    /**
     * Sets the phone2 value for this Elevator.
     * 
     * @param phone2
     */
    public void setPhone2(java.lang.String phone2) {
        this.phone2 = phone2;
    }


    /**
     * Gets the fax value for this Elevator.
     * 
     * @return fax
     */
    public java.lang.String getFax() {
        return fax;
    }


    /**
     * Sets the fax value for this Elevator.
     * 
     * @param fax
     */
    public void setFax(java.lang.String fax) {
        this.fax = fax;
    }


    /**
     * Gets the imageName value for this Elevator.
     * 
     * @return imageName
     */
    public java.lang.String getImageName() {
        return imageName;
    }


    /**
     * Sets the imageName value for this Elevator.
     * 
     * @param imageName
     */
    public void setImageName(java.lang.String imageName) {
        this.imageName = imageName;
    }


    /**
     * Gets the x value for this Elevator.
     * 
     * @return x
     */
    public java.lang.String getX() {
        return x;
    }


    /**
     * Sets the x value for this Elevator.
     * 
     * @param x
     */
    public void setX(java.lang.String x) {
        this.x = x;
    }


    /**
     * Gets the y value for this Elevator.
     * 
     * @return y
     */
    public java.lang.String getY() {
        return y;
    }


    /**
     * Sets the y value for this Elevator.
     * 
     * @param y
     */
    public void setY(java.lang.String y) {
        this.y = y;
    }


    /**
     * Gets the manager value for this Elevator.
     * 
     * @return manager
     */
    public java.lang.String getManager() {
        return manager;
    }


    /**
     * Sets the manager value for this Elevator.
     * 
     * @param manager
     */
    public void setManager(java.lang.String manager) {
        this.manager = manager;
    }


    /**
     * Gets the email value for this Elevator.
     * 
     * @return email
     */
    public java.lang.String getEmail() {
        return email;
    }


    /**
     * Sets the email value for this Elevator.
     * 
     * @param email
     */
    public void setEmail(java.lang.String email) {
        this.email = email;
    }


    /**
     * Gets the mgrLogon value for this Elevator.
     * 
     * @return mgrLogon
     */
    public java.lang.String getMgrLogon() {
        return mgrLogon;
    }


    /**
     * Sets the mgrLogon value for this Elevator.
     * 
     * @param mgrLogon
     */
    public void setMgrLogon(java.lang.String mgrLogon) {
        this.mgrLogon = mgrLogon;
    }


    /**
     * Gets the IBid value for this Elevator.
     * 
     * @return IBid
     */
    public java.lang.String getIBid() {
        return IBid;
    }


    /**
     * Sets the IBid value for this Elevator.
     * 
     * @param IBid
     */
    public void setIBid(java.lang.String IBid) {
        this.IBid = IBid;
    }


    /**
     * Gets the lastUpdate value for this Elevator.
     * 
     * @return lastUpdate
     */
    public java.util.Calendar getLastUpdate() {
        return lastUpdate;
    }


    /**
     * Sets the lastUpdate value for this Elevator.
     * 
     * @param lastUpdate
     */
    public void setLastUpdate(java.util.Calendar lastUpdate) {
        this.lastUpdate = lastUpdate;
    }


    /**
     * Gets the strLastUpdate value for this Elevator.
     * 
     * @return strLastUpdate
     */
    public java.lang.String getStrLastUpdate() {
        return strLastUpdate;
    }


    /**
     * Sets the strLastUpdate value for this Elevator.
     * 
     * @param strLastUpdate
     */
    public void setStrLastUpdate(java.lang.String strLastUpdate) {
        this.strLastUpdate = strLastUpdate;
    }


    /**
     * Gets the timeZone value for this Elevator.
     * 
     * @return timeZone
     */
    public java.lang.String getTimeZone() {
        return timeZone;
    }


    /**
     * Sets the timeZone value for this Elevator.
     * 
     * @param timeZone
     */
    public void setTimeZone(java.lang.String timeZone) {
        this.timeZone = timeZone;
    }


    /**
     * Gets the strIsActive value for this Elevator.
     * 
     * @return strIsActive
     */
    public java.lang.String getStrIsActive() {
        return strIsActive;
    }


    /**
     * Sets the strIsActive value for this Elevator.
     * 
     * @param strIsActive
     */
    public void setStrIsActive(java.lang.String strIsActive) {
        this.strIsActive = strIsActive;
    }


    /**
     * Gets the isActive value for this Elevator.
     * 
     * @return isActive
     */
    public boolean isIsActive() {
        return isActive;
    }


    /**
     * Sets the isActive value for this Elevator.
     * 
     * @param isActive
     */
    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }


    /**
     * Gets the companyID value for this Elevator.
     * 
     * @return companyID
     */
    public java.lang.String getCompanyID() {
        return companyID;
    }


    /**
     * Sets the companyID value for this Elevator.
     * 
     * @param companyID
     */
    public void setCompanyID(java.lang.String companyID) {
        this.companyID = companyID;
    }


    /**
     * Gets the special value for this Elevator.
     * 
     * @return special
     */
    public java.lang.String getSpecial() {
        return special;
    }


    /**
     * Sets the special value for this Elevator.
     * 
     * @param special
     */
    public void setSpecial(java.lang.String special) {
        this.special = special;
    }


    /**
     * Gets the map value for this Elevator.
     * 
     * @return map
     */
    public java.lang.String getMap() {
        return map;
    }


    /**
     * Sets the map value for this Elevator.
     * 
     * @param map
     */
    public void setMap(java.lang.String map) {
        this.map = map;
    }


    /**
     * Gets the DComments value for this Elevator.
     * 
     * @return DComments
     */
    public java.lang.String getDComments() {
        return DComments;
    }


    /**
     * Sets the DComments value for this Elevator.
     * 
     * @param DComments
     */
    public void setDComments(java.lang.String DComments) {
        this.DComments = DComments;
    }


    /**
     * Gets the destCode value for this Elevator.
     * 
     * @return destCode
     */
    public java.lang.String getDestCode() {
        return destCode;
    }


    /**
     * Sets the destCode value for this Elevator.
     * 
     * @param destCode
     */
    public void setDestCode(java.lang.String destCode) {
        this.destCode = destCode;
    }


    /**
     * Gets the freightSecondary value for this Elevator.
     * 
     * @return freightSecondary
     */
    public java.lang.String getFreightSecondary() {
        return freightSecondary;
    }


    /**
     * Sets the freightSecondary value for this Elevator.
     * 
     * @param freightSecondary
     */
    public void setFreightSecondary(java.lang.String freightSecondary) {
        this.freightSecondary = freightSecondary;
    }


    /**
     * Gets the returnAddr value for this Elevator.
     * 
     * @return returnAddr
     */
    public java.lang.String getReturnAddr() {
        return returnAddr;
    }


    /**
     * Sets the returnAddr value for this Elevator.
     * 
     * @param returnAddr
     */
    public void setReturnAddr(java.lang.String returnAddr) {
        this.returnAddr = returnAddr;
    }


    /**
     * Gets the replyEmail value for this Elevator.
     * 
     * @return replyEmail
     */
    public java.lang.String getReplyEmail() {
        return replyEmail;
    }


    /**
     * Sets the replyEmail value for this Elevator.
     * 
     * @param replyEmail
     */
    public void setReplyEmail(java.lang.String replyEmail) {
        this.replyEmail = replyEmail;
    }


    /**
     * Gets the reMailIsValid value for this Elevator.
     * 
     * @return reMailIsValid
     */
    public java.lang.String getReMailIsValid() {
        return reMailIsValid;
    }


    /**
     * Sets the reMailIsValid value for this Elevator.
     * 
     * @param reMailIsValid
     */
    public void setReMailIsValid(java.lang.String reMailIsValid) {
        this.reMailIsValid = reMailIsValid;
    }


    /**
     * Gets the latitude value for this Elevator.
     * 
     * @return latitude
     */
    public float getLatitude() {
        return latitude;
    }


    /**
     * Sets the latitude value for this Elevator.
     * 
     * @param latitude
     */
    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude value for this Elevator.
     * 
     * @return longitude
     */
    public float getLongitude() {
        return longitude;
    }


    /**
     * Sets the longitude value for this Elevator.
     * 
     * @param longitude
     */
    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the hits value for this Elevator.
     * 
     * @return hits
     */
    public int getHits() {
        return hits;
    }


    /**
     * Sets the hits value for this Elevator.
     * 
     * @param hits
     */
    public void setHits(int hits) {
        this.hits = hits;
    }


    /**
     * Gets the unloadHours value for this Elevator.
     * 
     * @return unloadHours
     */
    public java.lang.String getUnloadHours() {
        return unloadHours;
    }


    /**
     * Sets the unloadHours value for this Elevator.
     * 
     * @param unloadHours
     */
    public void setUnloadHours(java.lang.String unloadHours) {
        this.unloadHours = unloadHours;
    }


    /**
     * Gets the strDTNSend value for this Elevator.
     * 
     * @return strDTNSend
     */
    public java.lang.String getStrDTNSend() {
        return strDTNSend;
    }


    /**
     * Sets the strDTNSend value for this Elevator.
     * 
     * @param strDTNSend
     */
    public void setStrDTNSend(java.lang.String strDTNSend) {
        this.strDTNSend = strDTNSend;
    }


    /**
     * Gets the DTNSend value for this Elevator.
     * 
     * @return DTNSend
     */
    public boolean isDTNSend() {
        return DTNSend;
    }


    /**
     * Sets the DTNSend value for this Elevator.
     * 
     * @param DTNSend
     */
    public void setDTNSend(boolean DTNSend) {
        this.DTNSend = DTNSend;
    }


    /**
     * Gets the DTNElevID value for this Elevator.
     * 
     * @return DTNElevID
     */
    public java.lang.String getDTNElevID() {
        return DTNElevID;
    }


    /**
     * Sets the DTNElevID value for this Elevator.
     * 
     * @param DTNElevID
     */
    public void setDTNElevID(java.lang.String DTNElevID) {
        this.DTNElevID = DTNElevID;
    }


    /**
     * Gets the DTNPortalID value for this Elevator.
     * 
     * @return DTNPortalID
     */
    public java.lang.String getDTNPortalID() {
        return DTNPortalID;
    }


    /**
     * Sets the DTNPortalID value for this Elevator.
     * 
     * @param DTNPortalID
     */
    public void setDTNPortalID(java.lang.String DTNPortalID) {
        this.DTNPortalID = DTNPortalID;
    }


    /**
     * Gets the company value for this Elevator.
     * 
     * @return company
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Company getCompany() {
        return company;
    }


    /**
     * Sets the company value for this Elevator.
     * 
     * @param company
     */
    public void setCompany(com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Company company) {
        this.company = company;
    }


    /**
     * Gets the group value for this Elevator.
     * 
     * @return group
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Group getGroup() {
        return group;
    }


    /**
     * Sets the group value for this Elevator.
     * 
     * @param group
     */
    public void setGroup(com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Group group) {
        this.group = group;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Elevator)) return false;
        Elevator other = (Elevator) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.ID == other.getID() &&
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.longName==null && other.getLongName()==null) || 
             (this.longName!=null &&
              this.longName.equals(other.getLongName()))) &&
            ((this.code2==null && other.getCode2()==null) || 
             (this.code2!=null &&
              this.code2.equals(other.getCode2()))) &&
            ((this.shp==null && other.getShp()==null) || 
             (this.shp!=null &&
              this.shp.equals(other.getShp()))) &&
            ((this.freight==null && other.getFreight()==null) || 
             (this.freight!=null &&
              this.freight.equals(other.getFreight()))) &&
            ((this.name2==null && other.getName2()==null) || 
             (this.name2!=null &&
              this.name2.equals(other.getName2()))) &&
            ((this.groupID==null && other.getGroupID()==null) || 
             (this.groupID!=null &&
              this.groupID.equals(other.getGroupID()))) &&
            ((this.longName2==null && other.getLongName2()==null) || 
             (this.longName2!=null &&
              this.longName2.equals(other.getLongName2()))) &&
            ((this.addr1==null && other.getAddr1()==null) || 
             (this.addr1!=null &&
              this.addr1.equals(other.getAddr1()))) &&
            ((this.addr2==null && other.getAddr2()==null) || 
             (this.addr2!=null &&
              this.addr2.equals(other.getAddr2()))) &&
            ((this.addr3==null && other.getAddr3()==null) || 
             (this.addr3!=null &&
              this.addr3.equals(other.getAddr3()))) &&
            ((this.addr4==null && other.getAddr4()==null) || 
             (this.addr4!=null &&
              this.addr4.equals(other.getAddr4()))) &&
            ((this.city==null && other.getCity()==null) || 
             (this.city!=null &&
              this.city.equals(other.getCity()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.country==null && other.getCountry()==null) || 
             (this.country!=null &&
              this.country.equals(other.getCountry()))) &&
            ((this.zip==null && other.getZip()==null) || 
             (this.zip!=null &&
              this.zip.equals(other.getZip()))) &&
            ((this.hours==null && other.getHours()==null) || 
             (this.hours!=null &&
              this.hours.equals(other.getHours()))) &&
            ((this.PComments==null && other.getPComments()==null) || 
             (this.PComments!=null &&
              this.PComments.equals(other.getPComments()))) &&
            ((this.phone1==null && other.getPhone1()==null) || 
             (this.phone1!=null &&
              this.phone1.equals(other.getPhone1()))) &&
            ((this.phone2==null && other.getPhone2()==null) || 
             (this.phone2!=null &&
              this.phone2.equals(other.getPhone2()))) &&
            ((this.fax==null && other.getFax()==null) || 
             (this.fax!=null &&
              this.fax.equals(other.getFax()))) &&
            ((this.imageName==null && other.getImageName()==null) || 
             (this.imageName!=null &&
              this.imageName.equals(other.getImageName()))) &&
            ((this.x==null && other.getX()==null) || 
             (this.x!=null &&
              this.x.equals(other.getX()))) &&
            ((this.y==null && other.getY()==null) || 
             (this.y!=null &&
              this.y.equals(other.getY()))) &&
            ((this.manager==null && other.getManager()==null) || 
             (this.manager!=null &&
              this.manager.equals(other.getManager()))) &&
            ((this.email==null && other.getEmail()==null) || 
             (this.email!=null &&
              this.email.equals(other.getEmail()))) &&
            ((this.mgrLogon==null && other.getMgrLogon()==null) || 
             (this.mgrLogon!=null &&
              this.mgrLogon.equals(other.getMgrLogon()))) &&
            ((this.IBid==null && other.getIBid()==null) || 
             (this.IBid!=null &&
              this.IBid.equals(other.getIBid()))) &&
            ((this.lastUpdate==null && other.getLastUpdate()==null) || 
             (this.lastUpdate!=null &&
              this.lastUpdate.equals(other.getLastUpdate()))) &&
            ((this.strLastUpdate==null && other.getStrLastUpdate()==null) || 
             (this.strLastUpdate!=null &&
              this.strLastUpdate.equals(other.getStrLastUpdate()))) &&
            ((this.timeZone==null && other.getTimeZone()==null) || 
             (this.timeZone!=null &&
              this.timeZone.equals(other.getTimeZone()))) &&
            ((this.strIsActive==null && other.getStrIsActive()==null) || 
             (this.strIsActive!=null &&
              this.strIsActive.equals(other.getStrIsActive()))) &&
            this.isActive == other.isIsActive() &&
            ((this.companyID==null && other.getCompanyID()==null) || 
             (this.companyID!=null &&
              this.companyID.equals(other.getCompanyID()))) &&
            ((this.special==null && other.getSpecial()==null) || 
             (this.special!=null &&
              this.special.equals(other.getSpecial()))) &&
            ((this.map==null && other.getMap()==null) || 
             (this.map!=null &&
              this.map.equals(other.getMap()))) &&
            ((this.DComments==null && other.getDComments()==null) || 
             (this.DComments!=null &&
              this.DComments.equals(other.getDComments()))) &&
            ((this.destCode==null && other.getDestCode()==null) || 
             (this.destCode!=null &&
              this.destCode.equals(other.getDestCode()))) &&
            ((this.freightSecondary==null && other.getFreightSecondary()==null) || 
             (this.freightSecondary!=null &&
              this.freightSecondary.equals(other.getFreightSecondary()))) &&
            ((this.returnAddr==null && other.getReturnAddr()==null) || 
             (this.returnAddr!=null &&
              this.returnAddr.equals(other.getReturnAddr()))) &&
            ((this.replyEmail==null && other.getReplyEmail()==null) || 
             (this.replyEmail!=null &&
              this.replyEmail.equals(other.getReplyEmail()))) &&
            ((this.reMailIsValid==null && other.getReMailIsValid()==null) || 
             (this.reMailIsValid!=null &&
              this.reMailIsValid.equals(other.getReMailIsValid()))) &&
            this.latitude == other.getLatitude() &&
            this.longitude == other.getLongitude() &&
            this.hits == other.getHits() &&
            ((this.unloadHours==null && other.getUnloadHours()==null) || 
             (this.unloadHours!=null &&
              this.unloadHours.equals(other.getUnloadHours()))) &&
            ((this.strDTNSend==null && other.getStrDTNSend()==null) || 
             (this.strDTNSend!=null &&
              this.strDTNSend.equals(other.getStrDTNSend()))) &&
            this.DTNSend == other.isDTNSend() &&
            ((this.DTNElevID==null && other.getDTNElevID()==null) || 
             (this.DTNElevID!=null &&
              this.DTNElevID.equals(other.getDTNElevID()))) &&
            ((this.DTNPortalID==null && other.getDTNPortalID()==null) || 
             (this.DTNPortalID!=null &&
              this.DTNPortalID.equals(other.getDTNPortalID()))) &&
            ((this.company==null && other.getCompany()==null) || 
             (this.company!=null &&
              this.company.equals(other.getCompany()))) &&
            ((this.group==null && other.getGroup()==null) || 
             (this.group!=null &&
              this.group.equals(other.getGroup())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getID();
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getLongName() != null) {
            _hashCode += getLongName().hashCode();
        }
        if (getCode2() != null) {
            _hashCode += getCode2().hashCode();
        }
        if (getShp() != null) {
            _hashCode += getShp().hashCode();
        }
        if (getFreight() != null) {
            _hashCode += getFreight().hashCode();
        }
        if (getName2() != null) {
            _hashCode += getName2().hashCode();
        }
        if (getGroupID() != null) {
            _hashCode += getGroupID().hashCode();
        }
        if (getLongName2() != null) {
            _hashCode += getLongName2().hashCode();
        }
        if (getAddr1() != null) {
            _hashCode += getAddr1().hashCode();
        }
        if (getAddr2() != null) {
            _hashCode += getAddr2().hashCode();
        }
        if (getAddr3() != null) {
            _hashCode += getAddr3().hashCode();
        }
        if (getAddr4() != null) {
            _hashCode += getAddr4().hashCode();
        }
        if (getCity() != null) {
            _hashCode += getCity().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getCountry() != null) {
            _hashCode += getCountry().hashCode();
        }
        if (getZip() != null) {
            _hashCode += getZip().hashCode();
        }
        if (getHours() != null) {
            _hashCode += getHours().hashCode();
        }
        if (getPComments() != null) {
            _hashCode += getPComments().hashCode();
        }
        if (getPhone1() != null) {
            _hashCode += getPhone1().hashCode();
        }
        if (getPhone2() != null) {
            _hashCode += getPhone2().hashCode();
        }
        if (getFax() != null) {
            _hashCode += getFax().hashCode();
        }
        if (getImageName() != null) {
            _hashCode += getImageName().hashCode();
        }
        if (getX() != null) {
            _hashCode += getX().hashCode();
        }
        if (getY() != null) {
            _hashCode += getY().hashCode();
        }
        if (getManager() != null) {
            _hashCode += getManager().hashCode();
        }
        if (getEmail() != null) {
            _hashCode += getEmail().hashCode();
        }
        if (getMgrLogon() != null) {
            _hashCode += getMgrLogon().hashCode();
        }
        if (getIBid() != null) {
            _hashCode += getIBid().hashCode();
        }
        if (getLastUpdate() != null) {
            _hashCode += getLastUpdate().hashCode();
        }
        if (getStrLastUpdate() != null) {
            _hashCode += getStrLastUpdate().hashCode();
        }
        if (getTimeZone() != null) {
            _hashCode += getTimeZone().hashCode();
        }
        if (getStrIsActive() != null) {
            _hashCode += getStrIsActive().hashCode();
        }
        _hashCode += (isIsActive() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getCompanyID() != null) {
            _hashCode += getCompanyID().hashCode();
        }
        if (getSpecial() != null) {
            _hashCode += getSpecial().hashCode();
        }
        if (getMap() != null) {
            _hashCode += getMap().hashCode();
        }
        if (getDComments() != null) {
            _hashCode += getDComments().hashCode();
        }
        if (getDestCode() != null) {
            _hashCode += getDestCode().hashCode();
        }
        if (getFreightSecondary() != null) {
            _hashCode += getFreightSecondary().hashCode();
        }
        if (getReturnAddr() != null) {
            _hashCode += getReturnAddr().hashCode();
        }
        if (getReplyEmail() != null) {
            _hashCode += getReplyEmail().hashCode();
        }
        if (getReMailIsValid() != null) {
            _hashCode += getReMailIsValid().hashCode();
        }
        _hashCode += new Float(getLatitude()).hashCode();
        _hashCode += new Float(getLongitude()).hashCode();
        _hashCode += getHits();
        if (getUnloadHours() != null) {
            _hashCode += getUnloadHours().hashCode();
        }
        if (getStrDTNSend() != null) {
            _hashCode += getStrDTNSend().hashCode();
        }
        _hashCode += (isDTNSend() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getDTNElevID() != null) {
            _hashCode += getDTNElevID().hashCode();
        }
        if (getDTNPortalID() != null) {
            _hashCode += getDTNPortalID().hashCode();
        }
        if (getCompany() != null) {
            _hashCode += getCompany().hashCode();
        }
        if (getGroup() != null) {
            _hashCode += getGroup().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Elevator.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Elevator"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "longName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Shp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("freight");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Freight"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GroupID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LongName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addr1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Addr1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addr2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Addr2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addr3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Addr3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addr4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Addr4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "City"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "State"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Country"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zip");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Zip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hours");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Hours"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PComments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PComments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phone1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Phone1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phone2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Phone2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Fax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imageName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ImageName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("x");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "X"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("y");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Y"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("manager");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Manager"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("email");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Email"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mgrLogon");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "MgrLogon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IBid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "IBid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LastUpdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strLastUpdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "StrLastUpdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timeZone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "TimeZone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strIsActive");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "StrIsActive"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isActive");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "IsActive"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "CompanyID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("special");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Special"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("map");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Map"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DComments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DComments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DestCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("freightSecondary");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "FreightSecondary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returnAddr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ReturnAddr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("replyEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ReplyEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reMailIsValid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ReMailIsValid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Latitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Longitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hits");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Hits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unloadHours");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "UnloadHours"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strDTNSend");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "StrDTNSend"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DTNSend");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DTNSend"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DTNElevID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DTNElevID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DTNPortalID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DTNPortalID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("company");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Company"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Company"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("group");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Group"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Group"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	@Override
	public String toString() {
		return "Elevator [ID=" + ID + ", code=" + code + ", name=" + name + ", longName=" + longName + ", code2="
				+ code2 + ", shp=" + shp + ", freight=" + freight + ", name2=" + name2 + ", groupID=" + groupID
				+ ", longName2=" + longName2 + ", addr1=" + addr1 + ", addr2=" + addr2 + ", addr3=" + addr3 + ", addr4="
				+ addr4 + ", city=" + city + ", state=" + state + ", country=" + country + ", zip=" + zip + ", hours="
				+ hours + ", PComments=" + PComments + ", phone1=" + phone1 + ", phone2=" + phone2 + ", fax=" + fax
				+ ", imageName=" + imageName + ", x=" + x + ", y=" + y + ", manager=" + manager + ", email=" + email
				+ ", mgrLogon=" + mgrLogon + ", IBid=" + IBid + ", lastUpdate=" + lastUpdate + ", strLastUpdate="
				+ strLastUpdate + ", timeZone=" + timeZone + ", strIsActive=" + strIsActive + ", isActive=" + isActive
				+ ", companyID=" + companyID + ", special=" + special + ", map=" + map + ", DComments=" + DComments
				+ ", destCode=" + destCode + ", freightSecondary=" + freightSecondary + ", returnAddr=" + returnAddr
				+ ", replyEmail=" + replyEmail + ", reMailIsValid=" + reMailIsValid + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", hits=" + hits + ", unloadHours=" + unloadHours + ", strDTNSend="
				+ strDTNSend + ", DTNSend=" + DTNSend + ", DTNElevID=" + DTNElevID + ", DTNPortalID=" + DTNPortalID
				+ ", company=" + company + ", group=" + group + ", __equalsCalc=" + __equalsCalc + ", __hashCodeCalc="
				+ __hashCodeCalc + "]";
	}

}
