/**
 * BW_Bids_Location_InfoSoapStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Bids_Location_Info;

public class BW_Bids_Location_InfoSoapStub extends org.apache.axis.client.Stub implements com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoap {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[38];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
        _initOperationDesc4();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetCommodityBidsForLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "languageCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfPeriodBidCommodity"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBidCommodity[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetCommodityBidsForLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBidCommodity"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBidsAndOffersForLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "languageCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfPeriodBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetBidsAndOffersForLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetDisplayBidsAndOffersForLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "languageCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfPeriodBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetDisplayBidsAndOffersForLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBidsForLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "languageCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfPeriodBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetBidsForLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetDisplayBidsForLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "languageCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfPeriodBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetDisplayBidsForLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetOffersForLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "languageCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfPeriodBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetOffersForLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetDisplayOffersForLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "languageCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfPeriodBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetDisplayOffersForLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBidsForLocations");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationIds"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "languageCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfPeriodBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetBidsForLocationsResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBidCountForLocations");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationIds"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfLocationBidCount"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.LocationBidCount[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetBidCountForLocationsResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LocationBidCount"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetHistoricalBidsAndOffersForLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "languageCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "date"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfPeriodBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetHistoricalBidsAndOffersForLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetHistoricalBidsForLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "languageCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "date"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfPeriodBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetHistoricalBidsForLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetHistoricalOffersForLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "languageCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "date"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfPeriodBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetHistoricalOffersForLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetDetailedElevatorInformation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "elevatorID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Elevator"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetDetailedElevatorInformationResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetDTNBidsAndOffersForLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfPeriodBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetDTNBidsAndOffersForLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBidsAndOffersByLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "dealerProducerCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetBidsAndOffersByLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Bid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBidsByLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "dealerProducerCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://microsoft.com/wsdl/types/", "char"), org.apache.axis.types.UnsignedShort.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetBidsByLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Bid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetOffersByLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "dealerProducerCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://microsoft.com/wsdl/types/", "char"), org.apache.axis.types.UnsignedShort.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetOffersByLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Bid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetHistoricBidsAndOffersByLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "date"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "dealerProducerCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://microsoft.com/wsdl/types/", "char"), org.apache.axis.types.UnsignedShort.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetHistoricBidsAndOffersByLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Bid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetHistoricBidsByLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "date"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "dealerProducerCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://microsoft.com/wsdl/types/", "char"), org.apache.axis.types.UnsignedShort.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetHistoricBidsByLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Bid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetHistoricOffersByLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "date"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "dealerProducerCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://microsoft.com/wsdl/types/", "char"), org.apache.axis.types.UnsignedShort.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetHistoricOffersByLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Bid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetFarmerViewBidsByLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "dealerProducerCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://microsoft.com/wsdl/types/", "char"), org.apache.axis.types.UnsignedShort.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetFarmerViewBidsByLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Bid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetFarmerViewOffersByLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "dealerProducerCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://microsoft.com/wsdl/types/", "char"), org.apache.axis.types.UnsignedShort.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfBid"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetFarmerViewOffersByLocationResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Bid"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLocationBids");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "dealerProducerCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://microsoft.com/wsdl/types/", "char"), org.apache.axis.types.UnsignedShort.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfBidCommodity"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.BidCommodity[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetLocationBidsResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BidCommodity"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetStateProvinceWithLocations");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfString"));
        oper.setReturnClass(java.lang.String[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetStateProvinceWithLocationsResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "string"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetADMLocationsByState");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "stateProvince"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfElevator"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetADMLocationsByStateResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Elevator"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[24] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLocationByID");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Elevator"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetLocationByIDResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[25] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBidReplyToEmail");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetBidReplyToEmailResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[26] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLocationHours");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetLocationHoursResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[27] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBidLocationInfo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BidLocation"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.BidLocation.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetBidLocationInfoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[28] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLocationManagerEmail");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetLocationManagerEmailResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[29] = oper;

    }

    private static void _initOperationDesc4(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetElevatorAdmins");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfAdmin"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.Admin[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetElevatorAdminsResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Admin"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[30] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLocationContacts");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfContact"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.Contact[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetLocationContactsResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Contact"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[31] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetContactPhoto");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "staffIDs"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        oper.setReturnClass(byte[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetContactPhotoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[32] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetDSFileInfo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfDiscountSchedule"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.DiscountSchedule[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetDSFileInfoResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DiscountSchedule"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[33] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLocationDocumentFile");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfLocationDocument"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.LocationDocument[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetLocationDocumentFileResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LocationDocument"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[34] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAllLocationAnnouncements");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfBLI_Announcement"));
        oper.setReturnClass(com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Announcement[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetAllLocationAnnouncementsResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Announcement"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[35] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLocationAnnouncements");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetLocationAnnouncementsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[36] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLocationComments");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "locationID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "dealerProducerCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://microsoft.com/wsdl/types/", "char"), org.apache.axis.types.UnsignedShort.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "subscriptionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetLocationCommentsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[37] = oper;

    }

    public BW_Bids_Location_InfoSoapStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public BW_Bids_Location_InfoSoapStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public BW_Bids_Location_InfoSoapStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://microsoft.com/wsdl/types/", "char");
            cachedSerQNames.add(qName);
            cls = org.apache.axis.types.UnsignedShort.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Admin");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.Admin.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfAdmin");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.Admin[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Admin");
            qName2 = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Admin");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfBid");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Bid");
            qName2 = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Bid");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfBidCommodity");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.BidCommodity[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BidCommodity");
            qName2 = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BidCommodity");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfBLI_Announcement");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Announcement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Announcement");
            qName2 = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Announcement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfContact");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.Contact[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Contact");
            qName2 = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Contact");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfDiscountSchedule");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.DiscountSchedule[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DiscountSchedule");
            qName2 = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DiscountSchedule");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfElevator");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Elevator");
            qName2 = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Elevator");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfLocationBidCount");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.LocationBidCount[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LocationBidCount");
            qName2 = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LocationBidCount");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfLocationDocument");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.LocationDocument[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LocationDocument");
            qName2 = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LocationDocument");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfPeriodBid");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBid");
            qName2 = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBid");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfPeriodBidCommodity");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBidCommodity[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBidCommodity");
            qName2 = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBidCommodity");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ArrayOfString");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Bid");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.Bid.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BidCommodity");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.BidCommodity.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BidLocation");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.BidLocation.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BidsEntryType");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.BidsEntryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BidsRoundOption");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.BidsRoundOption.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Announcement");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Announcement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Commodity");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Commodity.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Company");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Company.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Currency");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Currency.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Group");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Group.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Market");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Market.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_UnitOfMeasure");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_UnitOfMeasure.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Contact");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.Contact.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DealerProducerType");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.DealerProducerType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "DiscountSchedule");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.DiscountSchedule.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Elevator");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LocationBidCount");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.LocationBidCount.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LocationDocument");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.LocationDocument.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBid");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "PeriodBidCommodity");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBidCommodity.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "VendorCustomerType");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.BW_Bids_Location_Info.VendorCustomerType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBidCommodity[] getCommodityBidsForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetCommodityBidsForLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetCommodityBidsForLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationId, languageCode});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBidCommodity[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBidCommodity[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBidCommodity[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getBidsAndOffersForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetBidsAndOffersForLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetBidsAndOffersForLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationId, languageCode});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getDisplayBidsAndOffersForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetDisplayBidsAndOffersForLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetDisplayBidsAndOffersForLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationId, languageCode});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getBidsForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetBidsForLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetBidsForLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationId, languageCode});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getDisplayBidsForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetDisplayBidsForLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetDisplayBidsForLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationId, languageCode});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getOffersForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetOffersForLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetOffersForLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationId, languageCode});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getDisplayOffersForLocation(java.lang.String locationId, java.lang.String languageCode) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetDisplayOffersForLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetDisplayOffersForLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationId, languageCode});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getBidsForLocations(java.lang.String locationIds, java.lang.String languageCode) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetBidsForLocations");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetBidsForLocations"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationIds, languageCode});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.LocationBidCount[] getBidCountForLocations(java.lang.String locationIds) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetBidCountForLocations");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetBidCountForLocations"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationIds});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.LocationBidCount[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.LocationBidCount[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.LocationBidCount[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getHistoricalBidsAndOffersForLocation(java.lang.String locationId, java.lang.String languageCode, java.lang.String date) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetHistoricalBidsAndOffersForLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetHistoricalBidsAndOffersForLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationId, languageCode, date});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getHistoricalBidsForLocation(java.lang.String locationId, java.lang.String languageCode, java.lang.String date) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetHistoricalBidsForLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetHistoricalBidsForLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationId, languageCode, date});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getHistoricalOffersForLocation(java.lang.String locationId, java.lang.String languageCode, java.lang.String date) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetHistoricalOffersForLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetHistoricalOffersForLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationId, languageCode, date});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator getDetailedElevatorInformation(java.lang.String elevatorID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetDetailedElevatorInformation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetDetailedElevatorInformation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {elevatorID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[] getDTNBidsAndOffersForLocation(java.lang.String locationId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetDTNBidsAndOffersForLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetDTNBidsAndOffersForLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.PeriodBid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getBidsAndOffersByLocation(int locationID, java.lang.String dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetBidsAndOffersByLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetBidsAndOffersByLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(locationID), dealerProducerCode, subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getBidsByLocation(int locationID, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetBidsByLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetBidsByLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(locationID), dealerProducerCode, subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getOffersByLocation(int locationID, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetOffersByLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetOffersByLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(locationID), dealerProducerCode, subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getHistoricBidsAndOffersByLocation(int locationID, java.lang.String date, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetHistoricBidsAndOffersByLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetHistoricBidsAndOffersByLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(locationID), date, dealerProducerCode, subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getHistoricBidsByLocation(int locationID, java.lang.String date, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetHistoricBidsByLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetHistoricBidsByLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(locationID), date, dealerProducerCode, subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getHistoricOffersByLocation(int locationID, java.lang.String date, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetHistoricOffersByLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetHistoricOffersByLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(locationID), date, dealerProducerCode, subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getFarmerViewBidsByLocation(int locationID, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetFarmerViewBidsByLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetFarmerViewBidsByLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(locationID), dealerProducerCode, subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getFarmerViewOffersByLocation(int locationID, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetFarmerViewOffersByLocation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetFarmerViewOffersByLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(locationID), dealerProducerCode, subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.BidCommodity[] getLocationBids(java.lang.String locationID, org.apache.axis.types.UnsignedShort dealerProducerCode) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetLocationBids");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetLocationBids"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationID, dealerProducerCode});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.BidCommodity[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.BidCommodity[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.BidCommodity[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String[] getStateProvinceWithLocations(java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetStateProvinceWithLocations");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetStateProvinceWithLocations"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator[] getADMLocationsByState(java.lang.String stateProvince, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetADMLocationsByState");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetADMLocationsByState"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {stateProvince, subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator getLocationByID(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[25]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetLocationByID");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetLocationByID"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(locationID), subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String getBidReplyToEmail(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[26]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetBidReplyToEmail");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetBidReplyToEmail"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(locationID), subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String getLocationHours(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[27]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetLocationHours");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetLocationHours"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(locationID), subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.BidLocation getBidLocationInfo(java.lang.String locationID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[28]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetBidLocationInfo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetBidLocationInfo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.BidLocation) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.BidLocation) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.BidLocation.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String getLocationManagerEmail(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[29]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetLocationManagerEmail");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetLocationManagerEmail"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(locationID), subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.Admin[] getElevatorAdmins(java.lang.String locationId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[30]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetElevatorAdmins");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetElevatorAdmins"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Admin[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Admin[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.Admin[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.Contact[] getLocationContacts(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[31]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetLocationContacts");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetLocationContacts"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(locationID), subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Contact[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.Contact[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.Contact[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public byte[] getContactPhoto(java.lang.String staffIDs) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[32]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetContactPhoto");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetContactPhoto"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {staffIDs});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (byte[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (byte[]) org.apache.axis.utils.JavaUtils.convert(_resp, byte[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.DiscountSchedule[] getDSFileInfo(java.lang.String locationID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[33]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetDSFileInfo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetDSFileInfo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.DiscountSchedule[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.DiscountSchedule[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.DiscountSchedule[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.LocationDocument[] getLocationDocumentFile(java.lang.String locationID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[34]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetLocationDocumentFile");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetLocationDocumentFile"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.LocationDocument[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.LocationDocument[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.LocationDocument[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Announcement[] getAllLocationAnnouncements(java.lang.String locationID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[35]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetAllLocationAnnouncements");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetAllLocationAnnouncements"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {locationID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Announcement[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Announcement[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.BW_Bids_Location_Info.BLI_Announcement[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String getLocationAnnouncements(int locationID, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[36]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetLocationAnnouncements");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetLocationAnnouncements"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(locationID), subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String getLocationComments(int locationID, org.apache.axis.types.UnsignedShort dealerProducerCode, java.lang.String subscriptionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[37]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/BW_Bids_Location_Info/GetLocationComments");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "GetLocationComments"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(locationID), dealerProducerCode, subscriptionID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
