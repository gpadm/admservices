/**
 * Bid.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Bids_Location_Info;

public class Bid  implements java.io.Serializable {
    private java.lang.String vendorCustomerCode;

    private java.lang.String dealerProducerCode;

    private java.lang.String futureCode;

    private java.lang.String longDescription;

    private java.lang.String beginDate;

    private java.lang.String endDate;

    private java.lang.String futureClose;

    private java.lang.String basis;

    private java.lang.String opt;

    private java.lang.String flat;

    private java.lang.String lastUpdated;

    private boolean settled;

    private java.lang.String admCode;

    private java.lang.String periodID;

    private java.lang.String basisOrFlat;

    private java.lang.String _final;

    private java.lang.String trans;

    private java.lang.String roundOpt;

    private java.lang.String optYear;

    private boolean isExcluded;

    private int futureCloseDecimalPlaces;

    private int basisDecimalPlaces;

    private int flatDecimalPlaces;

    private boolean displayOnWeb;

    private java.lang.String currency;

    private java.lang.String uom;

    private java.lang.String currencyPerUOM;

    private java.lang.String defaultCurrencyPerUOM;

    public Bid() {
    }

    public Bid(
           java.lang.String vendorCustomerCode,
           java.lang.String dealerProducerCode,
           java.lang.String futureCode,
           java.lang.String longDescription,
           java.lang.String beginDate,
           java.lang.String endDate,
           java.lang.String futureClose,
           java.lang.String basis,
           java.lang.String opt,
           java.lang.String flat,
           java.lang.String lastUpdated,
           boolean settled,
           java.lang.String admCode,
           java.lang.String periodID,
           java.lang.String basisOrFlat,
           java.lang.String _final,
           java.lang.String trans,
           java.lang.String roundOpt,
           java.lang.String optYear,
           boolean isExcluded,
           int futureCloseDecimalPlaces,
           int basisDecimalPlaces,
           int flatDecimalPlaces,
           boolean displayOnWeb,
           java.lang.String currency,
           java.lang.String uom,
           java.lang.String currencyPerUOM,
           java.lang.String defaultCurrencyPerUOM) {
           this.vendorCustomerCode = vendorCustomerCode;
           this.dealerProducerCode = dealerProducerCode;
           this.futureCode = futureCode;
           this.longDescription = longDescription;
           this.beginDate = beginDate;
           this.endDate = endDate;
           this.futureClose = futureClose;
           this.basis = basis;
           this.opt = opt;
           this.flat = flat;
           this.lastUpdated = lastUpdated;
           this.settled = settled;
           this.admCode = admCode;
           this.periodID = periodID;
           this.basisOrFlat = basisOrFlat;
           this._final = _final;
           this.trans = trans;
           this.roundOpt = roundOpt;
           this.optYear = optYear;
           this.isExcluded = isExcluded;
           this.futureCloseDecimalPlaces = futureCloseDecimalPlaces;
           this.basisDecimalPlaces = basisDecimalPlaces;
           this.flatDecimalPlaces = flatDecimalPlaces;
           this.displayOnWeb = displayOnWeb;
           this.currency = currency;
           this.uom = uom;
           this.currencyPerUOM = currencyPerUOM;
           this.defaultCurrencyPerUOM = defaultCurrencyPerUOM;
    }


    /**
     * Gets the vendorCustomerCode value for this Bid.
     * 
     * @return vendorCustomerCode
     */
    public java.lang.String getVendorCustomerCode() {
        return vendorCustomerCode;
    }


    /**
     * Sets the vendorCustomerCode value for this Bid.
     * 
     * @param vendorCustomerCode
     */
    public void setVendorCustomerCode(java.lang.String vendorCustomerCode) {
        this.vendorCustomerCode = vendorCustomerCode;
    }


    /**
     * Gets the dealerProducerCode value for this Bid.
     * 
     * @return dealerProducerCode
     */
    public java.lang.String getDealerProducerCode() {
        return dealerProducerCode;
    }


    /**
     * Sets the dealerProducerCode value for this Bid.
     * 
     * @param dealerProducerCode
     */
    public void setDealerProducerCode(java.lang.String dealerProducerCode) {
        this.dealerProducerCode = dealerProducerCode;
    }


    /**
     * Gets the futureCode value for this Bid.
     * 
     * @return futureCode
     */
    public java.lang.String getFutureCode() {
        return futureCode;
    }


    /**
     * Sets the futureCode value for this Bid.
     * 
     * @param futureCode
     */
    public void setFutureCode(java.lang.String futureCode) {
        this.futureCode = futureCode;
    }


    /**
     * Gets the longDescription value for this Bid.
     * 
     * @return longDescription
     */
    public java.lang.String getLongDescription() {
        return longDescription;
    }


    /**
     * Sets the longDescription value for this Bid.
     * 
     * @param longDescription
     */
    public void setLongDescription(java.lang.String longDescription) {
        this.longDescription = longDescription;
    }


    /**
     * Gets the beginDate value for this Bid.
     * 
     * @return beginDate
     */
    public java.lang.String getBeginDate() {
        return beginDate;
    }


    /**
     * Sets the beginDate value for this Bid.
     * 
     * @param beginDate
     */
    public void setBeginDate(java.lang.String beginDate) {
        this.beginDate = beginDate;
    }


    /**
     * Gets the endDate value for this Bid.
     * 
     * @return endDate
     */
    public java.lang.String getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this Bid.
     * 
     * @param endDate
     */
    public void setEndDate(java.lang.String endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the futureClose value for this Bid.
     * 
     * @return futureClose
     */
    public java.lang.String getFutureClose() {
        return futureClose;
    }


    /**
     * Sets the futureClose value for this Bid.
     * 
     * @param futureClose
     */
    public void setFutureClose(java.lang.String futureClose) {
        this.futureClose = futureClose;
    }


    /**
     * Gets the basis value for this Bid.
     * 
     * @return basis
     */
    public java.lang.String getBasis() {
        return basis;
    }


    /**
     * Sets the basis value for this Bid.
     * 
     * @param basis
     */
    public void setBasis(java.lang.String basis) {
        this.basis = basis;
    }


    /**
     * Gets the opt value for this Bid.
     * 
     * @return opt
     */
    public java.lang.String getOpt() {
        return opt;
    }


    /**
     * Sets the opt value for this Bid.
     * 
     * @param opt
     */
    public void setOpt(java.lang.String opt) {
        this.opt = opt;
    }


    /**
     * Gets the flat value for this Bid.
     * 
     * @return flat
     */
    public java.lang.String getFlat() {
        return flat;
    }


    /**
     * Sets the flat value for this Bid.
     * 
     * @param flat
     */
    public void setFlat(java.lang.String flat) {
        this.flat = flat;
    }


    /**
     * Gets the lastUpdated value for this Bid.
     * 
     * @return lastUpdated
     */
    public java.lang.String getLastUpdated() {
        return lastUpdated;
    }


    /**
     * Sets the lastUpdated value for this Bid.
     * 
     * @param lastUpdated
     */
    public void setLastUpdated(java.lang.String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }


    /**
     * Gets the settled value for this Bid.
     * 
     * @return settled
     */
    public boolean isSettled() {
        return settled;
    }


    /**
     * Sets the settled value for this Bid.
     * 
     * @param settled
     */
    public void setSettled(boolean settled) {
        this.settled = settled;
    }


    /**
     * Gets the admCode value for this Bid.
     * 
     * @return admCode
     */
    public java.lang.String getAdmCode() {
        return admCode;
    }


    /**
     * Sets the admCode value for this Bid.
     * 
     * @param admCode
     */
    public void setAdmCode(java.lang.String admCode) {
        this.admCode = admCode;
    }


    /**
     * Gets the periodID value for this Bid.
     * 
     * @return periodID
     */
    public java.lang.String getPeriodID() {
        return periodID;
    }


    /**
     * Sets the periodID value for this Bid.
     * 
     * @param periodID
     */
    public void setPeriodID(java.lang.String periodID) {
        this.periodID = periodID;
    }


    /**
     * Gets the basisOrFlat value for this Bid.
     * 
     * @return basisOrFlat
     */
    public java.lang.String getBasisOrFlat() {
        return basisOrFlat;
    }


    /**
     * Sets the basisOrFlat value for this Bid.
     * 
     * @param basisOrFlat
     */
    public void setBasisOrFlat(java.lang.String basisOrFlat) {
        this.basisOrFlat = basisOrFlat;
    }


    /**
     * Gets the _final value for this Bid.
     * 
     * @return _final
     */
    public java.lang.String get_final() {
        return _final;
    }


    /**
     * Sets the _final value for this Bid.
     * 
     * @param _final
     */
    public void set_final(java.lang.String _final) {
        this._final = _final;
    }


    /**
     * Gets the trans value for this Bid.
     * 
     * @return trans
     */
    public java.lang.String getTrans() {
        return trans;
    }


    /**
     * Sets the trans value for this Bid.
     * 
     * @param trans
     */
    public void setTrans(java.lang.String trans) {
        this.trans = trans;
    }


    /**
     * Gets the roundOpt value for this Bid.
     * 
     * @return roundOpt
     */
    public java.lang.String getRoundOpt() {
        return roundOpt;
    }


    /**
     * Sets the roundOpt value for this Bid.
     * 
     * @param roundOpt
     */
    public void setRoundOpt(java.lang.String roundOpt) {
        this.roundOpt = roundOpt;
    }


    /**
     * Gets the optYear value for this Bid.
     * 
     * @return optYear
     */
    public java.lang.String getOptYear() {
        return optYear;
    }


    /**
     * Sets the optYear value for this Bid.
     * 
     * @param optYear
     */
    public void setOptYear(java.lang.String optYear) {
        this.optYear = optYear;
    }


    /**
     * Gets the isExcluded value for this Bid.
     * 
     * @return isExcluded
     */
    public boolean isIsExcluded() {
        return isExcluded;
    }


    /**
     * Sets the isExcluded value for this Bid.
     * 
     * @param isExcluded
     */
    public void setIsExcluded(boolean isExcluded) {
        this.isExcluded = isExcluded;
    }


    /**
     * Gets the futureCloseDecimalPlaces value for this Bid.
     * 
     * @return futureCloseDecimalPlaces
     */
    public int getFutureCloseDecimalPlaces() {
        return futureCloseDecimalPlaces;
    }


    /**
     * Sets the futureCloseDecimalPlaces value for this Bid.
     * 
     * @param futureCloseDecimalPlaces
     */
    public void setFutureCloseDecimalPlaces(int futureCloseDecimalPlaces) {
        this.futureCloseDecimalPlaces = futureCloseDecimalPlaces;
    }


    /**
     * Gets the basisDecimalPlaces value for this Bid.
     * 
     * @return basisDecimalPlaces
     */
    public int getBasisDecimalPlaces() {
        return basisDecimalPlaces;
    }


    /**
     * Sets the basisDecimalPlaces value for this Bid.
     * 
     * @param basisDecimalPlaces
     */
    public void setBasisDecimalPlaces(int basisDecimalPlaces) {
        this.basisDecimalPlaces = basisDecimalPlaces;
    }


    /**
     * Gets the flatDecimalPlaces value for this Bid.
     * 
     * @return flatDecimalPlaces
     */
    public int getFlatDecimalPlaces() {
        return flatDecimalPlaces;
    }


    /**
     * Sets the flatDecimalPlaces value for this Bid.
     * 
     * @param flatDecimalPlaces
     */
    public void setFlatDecimalPlaces(int flatDecimalPlaces) {
        this.flatDecimalPlaces = flatDecimalPlaces;
    }


    /**
     * Gets the displayOnWeb value for this Bid.
     * 
     * @return displayOnWeb
     */
    public boolean isDisplayOnWeb() {
        return displayOnWeb;
    }


    /**
     * Sets the displayOnWeb value for this Bid.
     * 
     * @param displayOnWeb
     */
    public void setDisplayOnWeb(boolean displayOnWeb) {
        this.displayOnWeb = displayOnWeb;
    }


    /**
     * Gets the currency value for this Bid.
     * 
     * @return currency
     */
    public java.lang.String getCurrency() {
        return currency;
    }


    /**
     * Sets the currency value for this Bid.
     * 
     * @param currency
     */
    public void setCurrency(java.lang.String currency) {
        this.currency = currency;
    }


    /**
     * Gets the uom value for this Bid.
     * 
     * @return uom
     */
    public java.lang.String getUom() {
        return uom;
    }


    /**
     * Sets the uom value for this Bid.
     * 
     * @param uom
     */
    public void setUom(java.lang.String uom) {
        this.uom = uom;
    }


    /**
     * Gets the currencyPerUOM value for this Bid.
     * 
     * @return currencyPerUOM
     */
    public java.lang.String getCurrencyPerUOM() {
        return currencyPerUOM;
    }


    /**
     * Sets the currencyPerUOM value for this Bid.
     * 
     * @param currencyPerUOM
     */
    public void setCurrencyPerUOM(java.lang.String currencyPerUOM) {
        this.currencyPerUOM = currencyPerUOM;
    }


    /**
     * Gets the defaultCurrencyPerUOM value for this Bid.
     * 
     * @return defaultCurrencyPerUOM
     */
    public java.lang.String getDefaultCurrencyPerUOM() {
        return defaultCurrencyPerUOM;
    }


    /**
     * Sets the defaultCurrencyPerUOM value for this Bid.
     * 
     * @param defaultCurrencyPerUOM
     */
    public void setDefaultCurrencyPerUOM(java.lang.String defaultCurrencyPerUOM) {
        this.defaultCurrencyPerUOM = defaultCurrencyPerUOM;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Bid)) return false;
        Bid other = (Bid) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.vendorCustomerCode==null && other.getVendorCustomerCode()==null) || 
             (this.vendorCustomerCode!=null &&
              this.vendorCustomerCode.equals(other.getVendorCustomerCode()))) &&
            ((this.dealerProducerCode==null && other.getDealerProducerCode()==null) || 
             (this.dealerProducerCode!=null &&
              this.dealerProducerCode.equals(other.getDealerProducerCode()))) &&
            ((this.futureCode==null && other.getFutureCode()==null) || 
             (this.futureCode!=null &&
              this.futureCode.equals(other.getFutureCode()))) &&
            ((this.longDescription==null && other.getLongDescription()==null) || 
             (this.longDescription!=null &&
              this.longDescription.equals(other.getLongDescription()))) &&
            ((this.beginDate==null && other.getBeginDate()==null) || 
             (this.beginDate!=null &&
              this.beginDate.equals(other.getBeginDate()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.futureClose==null && other.getFutureClose()==null) || 
             (this.futureClose!=null &&
              this.futureClose.equals(other.getFutureClose()))) &&
            ((this.basis==null && other.getBasis()==null) || 
             (this.basis!=null &&
              this.basis.equals(other.getBasis()))) &&
            ((this.opt==null && other.getOpt()==null) || 
             (this.opt!=null &&
              this.opt.equals(other.getOpt()))) &&
            ((this.flat==null && other.getFlat()==null) || 
             (this.flat!=null &&
              this.flat.equals(other.getFlat()))) &&
            ((this.lastUpdated==null && other.getLastUpdated()==null) || 
             (this.lastUpdated!=null &&
              this.lastUpdated.equals(other.getLastUpdated()))) &&
            this.settled == other.isSettled() &&
            ((this.admCode==null && other.getAdmCode()==null) || 
             (this.admCode!=null &&
              this.admCode.equals(other.getAdmCode()))) &&
            ((this.periodID==null && other.getPeriodID()==null) || 
             (this.periodID!=null &&
              this.periodID.equals(other.getPeriodID()))) &&
            ((this.basisOrFlat==null && other.getBasisOrFlat()==null) || 
             (this.basisOrFlat!=null &&
              this.basisOrFlat.equals(other.getBasisOrFlat()))) &&
            ((this._final==null && other.get_final()==null) || 
             (this._final!=null &&
              this._final.equals(other.get_final()))) &&
            ((this.trans==null && other.getTrans()==null) || 
             (this.trans!=null &&
              this.trans.equals(other.getTrans()))) &&
            ((this.roundOpt==null && other.getRoundOpt()==null) || 
             (this.roundOpt!=null &&
              this.roundOpt.equals(other.getRoundOpt()))) &&
            ((this.optYear==null && other.getOptYear()==null) || 
             (this.optYear!=null &&
              this.optYear.equals(other.getOptYear()))) &&
            this.isExcluded == other.isIsExcluded() &&
            this.futureCloseDecimalPlaces == other.getFutureCloseDecimalPlaces() &&
            this.basisDecimalPlaces == other.getBasisDecimalPlaces() &&
            this.flatDecimalPlaces == other.getFlatDecimalPlaces() &&
            this.displayOnWeb == other.isDisplayOnWeb() &&
            ((this.currency==null && other.getCurrency()==null) || 
             (this.currency!=null &&
              this.currency.equals(other.getCurrency()))) &&
            ((this.uom==null && other.getUom()==null) || 
             (this.uom!=null &&
              this.uom.equals(other.getUom()))) &&
            ((this.currencyPerUOM==null && other.getCurrencyPerUOM()==null) || 
             (this.currencyPerUOM!=null &&
              this.currencyPerUOM.equals(other.getCurrencyPerUOM()))) &&
            ((this.defaultCurrencyPerUOM==null && other.getDefaultCurrencyPerUOM()==null) || 
             (this.defaultCurrencyPerUOM!=null &&
              this.defaultCurrencyPerUOM.equals(other.getDefaultCurrencyPerUOM())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVendorCustomerCode() != null) {
            _hashCode += getVendorCustomerCode().hashCode();
        }
        if (getDealerProducerCode() != null) {
            _hashCode += getDealerProducerCode().hashCode();
        }
        if (getFutureCode() != null) {
            _hashCode += getFutureCode().hashCode();
        }
        if (getLongDescription() != null) {
            _hashCode += getLongDescription().hashCode();
        }
        if (getBeginDate() != null) {
            _hashCode += getBeginDate().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getFutureClose() != null) {
            _hashCode += getFutureClose().hashCode();
        }
        if (getBasis() != null) {
            _hashCode += getBasis().hashCode();
        }
        if (getOpt() != null) {
            _hashCode += getOpt().hashCode();
        }
        if (getFlat() != null) {
            _hashCode += getFlat().hashCode();
        }
        if (getLastUpdated() != null) {
            _hashCode += getLastUpdated().hashCode();
        }
        _hashCode += (isSettled() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getAdmCode() != null) {
            _hashCode += getAdmCode().hashCode();
        }
        if (getPeriodID() != null) {
            _hashCode += getPeriodID().hashCode();
        }
        if (getBasisOrFlat() != null) {
            _hashCode += getBasisOrFlat().hashCode();
        }
        if (get_final() != null) {
            _hashCode += get_final().hashCode();
        }
        if (getTrans() != null) {
            _hashCode += getTrans().hashCode();
        }
        if (getRoundOpt() != null) {
            _hashCode += getRoundOpt().hashCode();
        }
        if (getOptYear() != null) {
            _hashCode += getOptYear().hashCode();
        }
        _hashCode += (isIsExcluded() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += getFutureCloseDecimalPlaces();
        _hashCode += getBasisDecimalPlaces();
        _hashCode += getFlatDecimalPlaces();
        _hashCode += (isDisplayOnWeb() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getCurrency() != null) {
            _hashCode += getCurrency().hashCode();
        }
        if (getUom() != null) {
            _hashCode += getUom().hashCode();
        }
        if (getCurrencyPerUOM() != null) {
            _hashCode += getCurrencyPerUOM().hashCode();
        }
        if (getDefaultCurrencyPerUOM() != null) {
            _hashCode += getDefaultCurrencyPerUOM().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Bid.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Bid"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendorCustomerCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "vendorCustomerCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerProducerCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "dealerProducerCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("futureCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "futureCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "longDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "beginDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "endDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("futureClose");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "futureClose"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("basis");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "basis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "opt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "flat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdated");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "lastUpdated"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("settled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "settled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("admCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "admCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("periodID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "periodID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("basisOrFlat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "basisOrFlat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("_final");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "final"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trans");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "trans"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("roundOpt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "roundOpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("optYear");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "optYear"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isExcluded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "isExcluded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("futureCloseDecimalPlaces");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "futureCloseDecimalPlaces"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("basisDecimalPlaces");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "basisDecimalPlaces"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flatDecimalPlaces");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "flatDecimalPlaces"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayOnWeb");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "displayOnWeb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "currency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uom");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "uom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyPerUOM");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "currencyPerUOM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("defaultCurrencyPerUOM");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "defaultCurrencyPerUOM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	@Override
	public String toString() {
		return "Bid [vendorCustomerCode=" + vendorCustomerCode + ", dealerProducerCode=" + dealerProducerCode
				+ ", futureCode=" + futureCode + ", longDescription=" + longDescription + ", beginDate=" + beginDate
				+ ", endDate=" + endDate + ", futureClose=" + futureClose + ", basis=" + basis + ", opt=" + opt
				+ ", flat=" + flat + ", lastUpdated=" + lastUpdated + ", settled=" + settled + ", admCode=" + admCode
				+ ", periodID=" + periodID + ", basisOrFlat=" + basisOrFlat + ", _final=" + _final + ", trans=" + trans
				+ ", roundOpt=" + roundOpt + ", optYear=" + optYear + ", isExcluded=" + isExcluded
				+ ", futureCloseDecimalPlaces=" + futureCloseDecimalPlaces + ", basisDecimalPlaces="
				+ basisDecimalPlaces + ", flatDecimalPlaces=" + flatDecimalPlaces + ", displayOnWeb=" + displayOnWeb
				+ ", currency=" + currency + ", uom=" + uom + ", currencyPerUOM=" + currencyPerUOM
				+ ", defaultCurrencyPerUOM=" + defaultCurrencyPerUOM + ", __equalsCalc=" + __equalsCalc
				+ ", __hashCodeCalc=" + __hashCodeCalc + "]";
	}
    
    

}
