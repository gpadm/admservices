/**
 * BidCommodity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Bids_Location_Info;

public class BidCommodity  implements java.io.Serializable {
    private java.lang.String commodityDescription;

    private java.lang.String commodityCode;

    private java.lang.String commodityFutureCode;

    private java.lang.String commodityCompositeCode;

    private com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] bids;

    public BidCommodity() {
    }

    public BidCommodity(
           java.lang.String commodityDescription,
           java.lang.String commodityCode,
           java.lang.String commodityFutureCode,
           java.lang.String commodityCompositeCode,
           com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] bids) {
           this.commodityDescription = commodityDescription;
           this.commodityCode = commodityCode;
           this.commodityFutureCode = commodityFutureCode;
           this.commodityCompositeCode = commodityCompositeCode;
           this.bids = bids;
    }


    /**
     * Gets the commodityDescription value for this BidCommodity.
     * 
     * @return commodityDescription
     */
    public java.lang.String getCommodityDescription() {
        return commodityDescription;
    }


    /**
     * Sets the commodityDescription value for this BidCommodity.
     * 
     * @param commodityDescription
     */
    public void setCommodityDescription(java.lang.String commodityDescription) {
        this.commodityDescription = commodityDescription;
    }


    /**
     * Gets the commodityCode value for this BidCommodity.
     * 
     * @return commodityCode
     */
    public java.lang.String getCommodityCode() {
        return commodityCode;
    }


    /**
     * Sets the commodityCode value for this BidCommodity.
     * 
     * @param commodityCode
     */
    public void setCommodityCode(java.lang.String commodityCode) {
        this.commodityCode = commodityCode;
    }


    /**
     * Gets the commodityFutureCode value for this BidCommodity.
     * 
     * @return commodityFutureCode
     */
    public java.lang.String getCommodityFutureCode() {
        return commodityFutureCode;
    }


    /**
     * Sets the commodityFutureCode value for this BidCommodity.
     * 
     * @param commodityFutureCode
     */
    public void setCommodityFutureCode(java.lang.String commodityFutureCode) {
        this.commodityFutureCode = commodityFutureCode;
    }


    /**
     * Gets the commodityCompositeCode value for this BidCommodity.
     * 
     * @return commodityCompositeCode
     */
    public java.lang.String getCommodityCompositeCode() {
        return commodityCompositeCode;
    }


    /**
     * Sets the commodityCompositeCode value for this BidCommodity.
     * 
     * @param commodityCompositeCode
     */
    public void setCommodityCompositeCode(java.lang.String commodityCompositeCode) {
        this.commodityCompositeCode = commodityCompositeCode;
    }


    /**
     * Gets the bids value for this BidCommodity.
     * 
     * @return bids
     */
    public com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] getBids() {
        return bids;
    }


    /**
     * Sets the bids value for this BidCommodity.
     * 
     * @param bids
     */
    public void setBids(com.adm.webpart.webservices.BW_Bids_Location_Info.Bid[] bids) {
        this.bids = bids;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BidCommodity)) return false;
        BidCommodity other = (BidCommodity) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.commodityDescription==null && other.getCommodityDescription()==null) || 
             (this.commodityDescription!=null &&
              this.commodityDescription.equals(other.getCommodityDescription()))) &&
            ((this.commodityCode==null && other.getCommodityCode()==null) || 
             (this.commodityCode!=null &&
              this.commodityCode.equals(other.getCommodityCode()))) &&
            ((this.commodityFutureCode==null && other.getCommodityFutureCode()==null) || 
             (this.commodityFutureCode!=null &&
              this.commodityFutureCode.equals(other.getCommodityFutureCode()))) &&
            ((this.commodityCompositeCode==null && other.getCommodityCompositeCode()==null) || 
             (this.commodityCompositeCode!=null &&
              this.commodityCompositeCode.equals(other.getCommodityCompositeCode()))) &&
            ((this.bids==null && other.getBids()==null) || 
             (this.bids!=null &&
              java.util.Arrays.equals(this.bids, other.getBids())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCommodityDescription() != null) {
            _hashCode += getCommodityDescription().hashCode();
        }
        if (getCommodityCode() != null) {
            _hashCode += getCommodityCode().hashCode();
        }
        if (getCommodityFutureCode() != null) {
            _hashCode += getCommodityFutureCode().hashCode();
        }
        if (getCommodityCompositeCode() != null) {
            _hashCode += getCommodityCompositeCode().hashCode();
        }
        if (getBids() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBids());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBids(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BidCommodity.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BidCommodity"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commodityDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "CommodityDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commodityCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "CommodityCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commodityFutureCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "CommodityFutureCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commodityCompositeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "CommodityCompositeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bids");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Bids"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Bid"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Bid"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
