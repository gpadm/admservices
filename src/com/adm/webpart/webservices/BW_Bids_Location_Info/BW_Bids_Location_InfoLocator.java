/**
 * BW_Bids_Location_InfoLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Bids_Location_Info;

public class BW_Bids_Location_InfoLocator extends org.apache.axis.client.Service implements com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_Info {

/**
 * This web service will retrieve different kinds information pertaining
 * to ADM locations.<br>
 */

    public BW_Bids_Location_InfoLocator() {
    }


    public BW_Bids_Location_InfoLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BW_Bids_Location_InfoLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BW_Bids_Location_InfoSoap
    private java.lang.String BW_Bids_Location_InfoSoap_address = "http://webservices.adm.com/BW_Bids_Location_Info/BW_Bids_Location_Info.asmx";

    public java.lang.String getBW_Bids_Location_InfoSoapAddress() {
        return BW_Bids_Location_InfoSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BW_Bids_Location_InfoSoapWSDDServiceName = "BW_Bids_Location_InfoSoap";

    public java.lang.String getBW_Bids_Location_InfoSoapWSDDServiceName() {
        return BW_Bids_Location_InfoSoapWSDDServiceName;
    }

    public void setBW_Bids_Location_InfoSoapWSDDServiceName(java.lang.String name) {
        BW_Bids_Location_InfoSoapWSDDServiceName = name;
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoap getBW_Bids_Location_InfoSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BW_Bids_Location_InfoSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBW_Bids_Location_InfoSoap(endpoint);
    }

    public com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoap getBW_Bids_Location_InfoSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoapStub _stub = new com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoapStub(portAddress, this);
            _stub.setPortName(getBW_Bids_Location_InfoSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBW_Bids_Location_InfoSoapEndpointAddress(java.lang.String address) {
        BW_Bids_Location_InfoSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoapStub _stub = new com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoapStub(new java.net.URL(BW_Bids_Location_InfoSoap_address), this);
                _stub.setPortName(getBW_Bids_Location_InfoSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BW_Bids_Location_InfoSoap".equals(inputPortName)) {
            return getBW_Bids_Location_InfoSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BW_Bids_Location_Info");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BW_Bids_Location_InfoSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BW_Bids_Location_InfoSoap".equals(portName)) {
            setBW_Bids_Location_InfoSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
