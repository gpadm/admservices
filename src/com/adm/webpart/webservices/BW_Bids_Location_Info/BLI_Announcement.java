/**
 * BLI_Announcement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Bids_Location_Info;

public class BLI_Announcement  implements java.io.Serializable {
    private int id;

    private java.lang.String elevatorId;

    private java.lang.String body;

    private java.lang.String strIsActive;

    private boolean isActive;

    private java.util.Calendar startDate;

    private java.util.Calendar stopDate;

    private java.util.Calendar lastUpdate;

    private java.lang.String lastUpdateBy;

    public BLI_Announcement() {
    }

    public BLI_Announcement(
           int id,
           java.lang.String elevatorId,
           java.lang.String body,
           java.lang.String strIsActive,
           boolean isActive,
           java.util.Calendar startDate,
           java.util.Calendar stopDate,
           java.util.Calendar lastUpdate,
           java.lang.String lastUpdateBy) {
           this.id = id;
           this.elevatorId = elevatorId;
           this.body = body;
           this.strIsActive = strIsActive;
           this.isActive = isActive;
           this.startDate = startDate;
           this.stopDate = stopDate;
           this.lastUpdate = lastUpdate;
           this.lastUpdateBy = lastUpdateBy;
    }


    /**
     * Gets the id value for this BLI_Announcement.
     * 
     * @return id
     */
    public int getId() {
        return id;
    }


    /**
     * Sets the id value for this BLI_Announcement.
     * 
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }


    /**
     * Gets the elevatorId value for this BLI_Announcement.
     * 
     * @return elevatorId
     */
    public java.lang.String getElevatorId() {
        return elevatorId;
    }


    /**
     * Sets the elevatorId value for this BLI_Announcement.
     * 
     * @param elevatorId
     */
    public void setElevatorId(java.lang.String elevatorId) {
        this.elevatorId = elevatorId;
    }


    /**
     * Gets the body value for this BLI_Announcement.
     * 
     * @return body
     */
    public java.lang.String getBody() {
        return body;
    }


    /**
     * Sets the body value for this BLI_Announcement.
     * 
     * @param body
     */
    public void setBody(java.lang.String body) {
        this.body = body;
    }


    /**
     * Gets the strIsActive value for this BLI_Announcement.
     * 
     * @return strIsActive
     */
    public java.lang.String getStrIsActive() {
        return strIsActive;
    }


    /**
     * Sets the strIsActive value for this BLI_Announcement.
     * 
     * @param strIsActive
     */
    public void setStrIsActive(java.lang.String strIsActive) {
        this.strIsActive = strIsActive;
    }


    /**
     * Gets the isActive value for this BLI_Announcement.
     * 
     * @return isActive
     */
    public boolean isIsActive() {
        return isActive;
    }


    /**
     * Sets the isActive value for this BLI_Announcement.
     * 
     * @param isActive
     */
    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }


    /**
     * Gets the startDate value for this BLI_Announcement.
     * 
     * @return startDate
     */
    public java.util.Calendar getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this BLI_Announcement.
     * 
     * @param startDate
     */
    public void setStartDate(java.util.Calendar startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the stopDate value for this BLI_Announcement.
     * 
     * @return stopDate
     */
    public java.util.Calendar getStopDate() {
        return stopDate;
    }


    /**
     * Sets the stopDate value for this BLI_Announcement.
     * 
     * @param stopDate
     */
    public void setStopDate(java.util.Calendar stopDate) {
        this.stopDate = stopDate;
    }


    /**
     * Gets the lastUpdate value for this BLI_Announcement.
     * 
     * @return lastUpdate
     */
    public java.util.Calendar getLastUpdate() {
        return lastUpdate;
    }


    /**
     * Sets the lastUpdate value for this BLI_Announcement.
     * 
     * @param lastUpdate
     */
    public void setLastUpdate(java.util.Calendar lastUpdate) {
        this.lastUpdate = lastUpdate;
    }


    /**
     * Gets the lastUpdateBy value for this BLI_Announcement.
     * 
     * @return lastUpdateBy
     */
    public java.lang.String getLastUpdateBy() {
        return lastUpdateBy;
    }


    /**
     * Sets the lastUpdateBy value for this BLI_Announcement.
     * 
     * @param lastUpdateBy
     */
    public void setLastUpdateBy(java.lang.String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BLI_Announcement)) return false;
        BLI_Announcement other = (BLI_Announcement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.id == other.getId() &&
            ((this.elevatorId==null && other.getElevatorId()==null) || 
             (this.elevatorId!=null &&
              this.elevatorId.equals(other.getElevatorId()))) &&
            ((this.body==null && other.getBody()==null) || 
             (this.body!=null &&
              this.body.equals(other.getBody()))) &&
            ((this.strIsActive==null && other.getStrIsActive()==null) || 
             (this.strIsActive!=null &&
              this.strIsActive.equals(other.getStrIsActive()))) &&
            this.isActive == other.isIsActive() &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.stopDate==null && other.getStopDate()==null) || 
             (this.stopDate!=null &&
              this.stopDate.equals(other.getStopDate()))) &&
            ((this.lastUpdate==null && other.getLastUpdate()==null) || 
             (this.lastUpdate!=null &&
              this.lastUpdate.equals(other.getLastUpdate()))) &&
            ((this.lastUpdateBy==null && other.getLastUpdateBy()==null) || 
             (this.lastUpdateBy!=null &&
              this.lastUpdateBy.equals(other.getLastUpdateBy())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getId();
        if (getElevatorId() != null) {
            _hashCode += getElevatorId().hashCode();
        }
        if (getBody() != null) {
            _hashCode += getBody().hashCode();
        }
        if (getStrIsActive() != null) {
            _hashCode += getStrIsActive().hashCode();
        }
        _hashCode += (isIsActive() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getStopDate() != null) {
            _hashCode += getStopDate().hashCode();
        }
        if (getLastUpdate() != null) {
            _hashCode += getLastUpdate().hashCode();
        }
        if (getLastUpdateBy() != null) {
            _hashCode += getLastUpdateBy().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BLI_Announcement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "BLI_Announcement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("elevatorId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "ElevatorId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("body");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "Body"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strIsActive");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "StrIsActive"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isActive");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "IsActive"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "StartDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stopDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "StopDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LastUpdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdateBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Bids_Location_Info", "LastUpdateBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
