package com.adm.webpart.webservices.client;

import java.util.ArrayList;

import com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoap;
import com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoapProxy;
import com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator;

public class ELocationID {
	
	public String[] getState() throws Exception
	{
		
		BW_Bids_Location_InfoSoap client = new BW_Bids_Location_InfoSoapProxy();
		return (client.getStateProvinceWithLocations(""));
		
	} // end method getState()
	
	public ArrayList<String> getCityList(String stateName) throws Exception
	{

	        ArrayList<String> cityList = new ArrayList<String>();
	     	BW_Bids_Location_InfoSoap client = new BW_Bids_Location_InfoSoapProxy();
			Elevator[] loc = client.getADMLocationsByState(stateName, null);
			for (int i =0;i<loc.length;i++)
				cityList.add(loc[i].getLongName());
			
			return cityList;
	} // end method getCityList
	
	public int getLocationId(String stateName, String cityName) throws Exception
	{
		int locationId = 0;
     	BW_Bids_Location_InfoSoap client = new BW_Bids_Location_InfoSoapProxy();
		Elevator[] loc = client.getADMLocationsByState(stateName, null);
		for (int i=0;i<loc.length;i++)
		{
			if(loc[i].getLongName().equals(cityName))
			{
				locationId = loc[i].getID();
				break;
			}// end if
			
		}// end for loop
		
		return locationId;
	} // end method getLocationId
	
	
}

