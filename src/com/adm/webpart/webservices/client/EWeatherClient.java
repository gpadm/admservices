package com.adm.webpart.webservices.client;

import java.rmi.RemoteException;

import com.adm.webpart.webservices.BW_Weather.BW_WeatherSoap;
import com.adm.webpart.webservices.BW_Weather.BW_WeatherSoapProxy;
import com.adm.webpart.webservices.BW_Weather.CurrentCondition;
import com.adm.webpart.webservices.client.bean.CurrentWeather;
import com.adm.webpart.webservices.BW_Weather.ForecastDayExtended;
import com.adm.webpart.beans.FiveDayForecast;

public class EWeatherClient {
	public CurrentWeather getCurrentWeather(String zipCode,
			String temperatureUnit) {
		BW_WeatherSoap client = new BW_WeatherSoapProxy();
		CurrentCondition cr = null;
		try {
			cr = client.getCurrentConditions(zipCode,
					temperatureUnit, "");
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		CurrentWeather cw = new CurrentWeather();

		cw.setDisplayName(cr.getDisplayName());
		cw.setCurrTemp(cr.getCurrTemp());
		cw.setFeelTemp(cr.getFeelTemp());
		cw.sethighTemp(cr.getHighTemp());
		cw.setIconURL(cr.getIconURL());
		cw.setLowTemp(cr.getLowTemp());

		return cw;
	}
	
	public CurrentCondition getCurrentFullWeather(String zipCode,
			String temperatureUnit) {
		BW_WeatherSoap client = new BW_WeatherSoapProxy();
		CurrentCondition cr = null;
		try {
			cr = client.getCurrentConditions(zipCode,
					temperatureUnit, "");
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		return cr;
	}
	

	public String getRadarUrl(String zipCode, int zoomLevel) {
		BW_WeatherSoap client = new BW_WeatherSoapProxy();
		String url = "";
		try {
			url = client.getRadarMapURL(zipCode, zoomLevel, "");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return url;
	}

	public Boolean isLocationValid(String location) throws Exception {
		BW_WeatherSoap client = new BW_WeatherSoapProxy();
		Boolean isValid = client.isLocationValid(location, "");
		if(!isValid) throw new Exception("Location not Valid");
		return isValid;
	}

	public FiveDayForecast[] getExtendedForecast(String location,
			String temperatureUnit) throws Exception {
		BW_WeatherSoap client = new BW_WeatherSoapProxy();

		ForecastDayExtended[] fe = client.getExtendedForecast(location,
				temperatureUnit, "");
		FiveDayForecast[] forecast = new FiveDayForecast[fe.length];
		for(int i = 0; i < fe.length; i++) {
			forecast[i] = new FiveDayForecast(fe[i]);
			System.out.printf("\nFor Day %s ->\n%s", (i+1), forecast[i]);
		}		
		return forecast;
	}
	
	
	public FiveDayForecast[] getFiveDayForcast(String location,
			String temperatureUnit) throws Exception {
		isLocationValid(location);
		BW_WeatherSoap client = new BW_WeatherSoapProxy();
		ForecastDayExtended[] fe = client.getFiveDayForecast(location, temperatureUnit, "");
		FiveDayForecast[] forecast = new FiveDayForecast[fe.length];
		for(int i = 0; i < fe.length; i++) {
			forecast[i] = new FiveDayForecast(fe[i]);
			System.out.printf("\nFor Day %s ->\n%s", (i+1), forecast[i]);
		}		
		return forecast;
	}
	
	public String getOnlyTime(String date) {
		String temp = date.substring(date.indexOf(" "));
		temp = temp.trim();
		System.out.println("1:VAlue of temp" + temp);
		String ampm = temp.substring(temp.indexOf(" "));
		System.out.println("2:VAlue of ampm" + temp);
		temp = temp.substring(0, temp.indexOf(" "));
		System.out.println("3:VAlue of ampm" + temp);
		temp = temp.substring(0, temp.lastIndexOf(":"));
		System.out.println("4:VAlue of ampm" + temp);
		
		return temp + " " + ampm.trim();
	}
	
	public static void main(String args[])  {
		EWeatherClient ec = new EWeatherClient();
		//System.out.println(ec.getCurrentWeather("DURG, CH", "F"));
		//System.out.println(ec.getRadarUrl("LONDON, ON", 2));
		ForecastDayExtended[] fe = null;
		CurrentCondition cc = null;
		String location = "67869";
		
		try {
			System.out.printf("locaton valid?: %s\n",ec.isLocationValid(location));
			//fe = ec.getExtendedForecast(location, "F");
			//fe = ec.extendedForeCast(" MONROE, NC", "F");
			cc = ec.getCurrentFullWeather(location, "F");
			System.out.println("After function call !!!!");
			System.out.printf("CurrTemp: %s\nHigh Temp: %s\nIconUrl: %s\n", cc.getCurrTemp(), cc.getHighTemp(), cc.getIconURL());			
		} catch (Exception e) {
			System.out.println("Exception !!!!");
			e.printStackTrace();
		}
		/*if(fe != null) {
			for (int i = 0; i < fe.length; i++) {
				System.out.printf("\nFor Day %s ->\n%s", (i+1), fe[i]);
			}
		}*/
		
		//System.out.println("VAlue of 6/17/2010 8:05:00 PM--->" + ec.getOnlyTime("6/17/2010 8:05:00 PM"));
	}
}