package com.adm.webpart.webservices.client;

import com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoap;
import com.adm.webpart.webservices.BW_Bids_Location_Info.BW_Bids_Location_InfoSoapProxy;
import com.adm.webpart.webservices.BW_Bids_Location_Info.LocationDocument;
import org.apache.axis.types.UnsignedShort;
import com.adm.webpart.webservices.BW_Bids_Location_Info.Bid;
import com.adm.webpart.webservices.BW_Bids_Location_Info.Contact;
import com.adm.webpart.webservices.BW_Bids_Location_Info.Elevator;
import com.adm.webpart.webservices.BW_Bids_Location_Info.BidLocation;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class EBidsLocationClient {

	public Bid[] getBidsByLocation(String locId, String vendorPriceType) throws Exception {

		Bid[] bid = null;
		UnsignedShort usInput = new UnsignedShort();
		int locationId = Integer.parseInt(locId);

		BW_Bids_Location_InfoSoap client = new BW_Bids_Location_InfoSoapProxy();

		if (vendorPriceType.equals("A"))
			usInput = new UnsignedShort('A');
		else if (vendorPriceType.equals("P"))
			usInput = new UnsignedShort('P');
		else if (vendorPriceType.equals("D"))
			usInput = new UnsignedShort('D');
		else
			usInput = new UnsignedShort('O');

		try {
			bid = client.getBidsByLocation(locationId, usInput, "");

		} catch (Exception e) {

			e.printStackTrace();
		}

		return bid;
	}// end getBidsByLocation

	public Bid[] getOffersByLocation(String locId, String vendorPriceType) throws Exception {

		Bid[] offers = null;
		UnsignedShort usInput = new UnsignedShort();
		int locationId = Integer.parseInt(locId);

		BW_Bids_Location_InfoSoap client = new BW_Bids_Location_InfoSoapProxy();

		if (vendorPriceType.equals("A"))
			usInput = new UnsignedShort('A');
		else if (vendorPriceType.equals("P"))
			usInput = new UnsignedShort('P');
		else if (vendorPriceType.equals("D"))
			usInput = new UnsignedShort('D');
		else
			usInput = new UnsignedShort('O');

		try {
			offers = client.getOffersByLocation(locationId, usInput, "");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return offers;
	}// end getOffersByLocation

	public String getAnnouncements(String locId) throws Exception {

		String announcement = "";
		BW_Bids_Location_InfoSoap client = new BW_Bids_Location_InfoSoapProxy();
		int locationId = Integer.parseInt(locId);

		try {
			announcement = client.getLocationAnnouncements(locationId, "");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return announcement;
	}// end getAnnouncements

	public String getComments(String locId, String vendorPriceType) throws Exception {

		UnsignedShort usInput = new UnsignedShort();
		String comments = "";

		if (vendorPriceType.equals("A"))
			usInput = new UnsignedShort('A');
		else if (vendorPriceType.equals("P"))
			usInput = new UnsignedShort('P');
		else if (vendorPriceType.equals("D"))
			usInput = new UnsignedShort('D');
		else
			usInput = new UnsignedShort('O');

		BW_Bids_Location_InfoSoap client = new BW_Bids_Location_InfoSoapProxy();
		int locationId = Integer.parseInt(locId);
		try {
			comments = client.getLocationComments(locationId, usInput, "");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return comments;

	}// end getComments

	public Contact[] getContacts(String locId) throws Exception {

		Contact[] contacts = null;

		BW_Bids_Location_InfoSoap client = new BW_Bids_Location_InfoSoapProxy();
		int locationId = Integer.parseInt(locId);

		try {
			contacts = client.getLocationContacts(locationId, "");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return contacts;
	}// end getContacts

	public String getHours(String locId) throws Exception {

		String hours = "";
		BW_Bids_Location_InfoSoap client = new BW_Bids_Location_InfoSoapProxy();
		int locationId = Integer.parseInt(locId);

		try {
			hours = client.getLocationHours(locationId, "");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return hours;
	}// end getHours

	public String getLocationManagerEmail(String locId) throws Exception {

		String locationManagerEmail = "";
		BW_Bids_Location_InfoSoap client = new BW_Bids_Location_InfoSoapProxy();
		int locationId = Integer.parseInt(locId);

		try {
			locationManagerEmail = client.getLocationManagerEmail(locationId, "");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return locationManagerEmail;
	}// end getLocationManagerEmail

	public BidLocation getBidLocationInfo(String locId) throws Exception {

		BidLocation locationInfo = new BidLocation();
		BW_Bids_Location_InfoSoap client = new BW_Bids_Location_InfoSoapProxy();

		try {
			locationInfo = client.getBidLocationInfo(locId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return locationInfo;
	}// end getBidLocationInfo

    public ArrayList<Integer> getLocationDocumentId(String locId) throws Exception
    {
    	  
    	  ArrayList<Integer> docId = new ArrayList<Integer>();
    	  BW_Bids_Location_InfoSoap client = new BW_Bids_Location_InfoSoapProxy();
    	  try
    	  {
    	  LocationDocument[] docs = client.getLocationDocumentFile(locId);
    	  for (int i =0;i<docs.length;i++)
    		  docId.add(docs[i].getLD_ID());
    	  } catch(Exception e) {
    		   e.printStackTrace();
    	  }
    
    	 return docId;  
    } // end getLocationDocumentId
    
	public LocationDocument getDocumentsByDocId(String locId, int docId) {
		LocationDocument document = null;
		BW_Bids_Location_InfoSoap client = new BW_Bids_Location_InfoSoapProxy();
		try {
			LocationDocument[] allDocs = client.getLocationDocumentFile(locId);
			for (int i = 0; i < allDocs.length; i++) {
				if (allDocs[i].getLD_ID() == docId) {
					document = allDocs[i];
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return document;
	} // end getDocumentsByDocId

	public byte[] getStaffImage(String staffId) {
		BW_Bids_Location_InfoSoap client = new BW_Bids_Location_InfoSoapProxy();
		byte[] image = null;
		try {
			image = client.getContactPhoto(staffId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return image;
	} // end getStaffImage

	public String getFormattedDate(String inputDate) throws Exception {
		String string = inputDate;
		SimpleDateFormat df = new SimpleDateFormat("M/d/yyyy h:mm:ss a");
		Date date = df.parse(string);
		SimpleDateFormat print = new SimpleDateFormat("M/d/YY h:mm a z");
		return (print.format(date));

	} // end getFormattedDate

	public String getFormattedDouble(String inputVal) throws Exception {
		Double inputNum = Double.parseDouble(inputVal);
		DecimalFormat format = new DecimalFormat("##0.00##");
		return (format.format(inputNum));

	} // end getFormattedDouble

	public String strJoin(String[] aArr, String sSep) throws Exception {
		StringBuilder sbStr = new StringBuilder();
		for (int i = 0, il = aArr.length; i < il; i++) {
			if (i > 0)
				sbStr.append(sSep);
			sbStr.append(aArr[i]);
		}
		return sbStr.toString();
	} // end strJoin
	
	public String getDetailedElevatorInformation(String elevatorID) throws Exception
	{
		   /*This method is used to return the name used in Location Contacts' contact form(on clicking email) */
		   BW_Bids_Location_InfoSoap client = new BW_Bids_Location_InfoSoapProxy();
		   String formName ="";
		   try {
			   Elevator ele = client.getDetailedElevatorInformation(elevatorID);
			   formName     = ele.getName();
		   } catch(Exception e)
		   {
			   e.printStackTrace();
		   }
		   return formName;
	} // end getDetailedElevatorInformation

}
