package com.adm.webpart.webservices.client;

import com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_IntelligenceSoap;
import com.adm.webpart.webservices.BW_Market_Intelligence.BW_Market_IntelligenceSoapProxy;
import com.adm.webpart.webservices.BW_Market_Intelligence.Commentator;
import com.adm.webpart.webservices.BW_Market_Intelligence.MarketIntell;

import java.util.Calendar;
import java.util.TimeZone;
import java.text.SimpleDateFormat;

public class EMarketIntelliClient {

	public  MarketIntell[] getDetailMarket(int commentryId) throws Exception {
		
    BW_Market_IntelligenceSoap client = new BW_Market_IntelligenceSoapProxy();
    MarketIntell[] csi2 			  = new MarketIntell[1];
    try {    
    Commentator cs4 = client.getCommentatorByID(commentryId,null); 
    csi2 = cs4.getCommentarys();
	} catch (Exception e) {
		e.printStackTrace();
		} 
    return csi2;
	 	 
	} //end method getDetailMarktet()
	

	
   public String getCommentator(int commentryId) throws Exception
   {
	    BW_Market_IntelligenceSoap client = new BW_Market_IntelligenceSoapProxy();
	    String titleList				  = null;
	    try {
	    	Commentator cs4 = client.getCommentatorByID(commentryId,null);
	    		if  (commentryId == 1) 
	    			titleList = cs4.getTitle().substring(0,cs4.getTitle().length()-1); // to remove * at the end of the string [ADM - Benson Quinn*]
	    		else
	    			titleList =cs4.getTitle();
        	} catch (Exception e) {
        		e.printStackTrace();
     } 	    
	   
		  return titleList; // This returns [ADM - Benson Quinn] for commentryId : 1
          //  [ADMIS Daily Grain Commentary] for commentryId: 2 
          // ADM Lloydminster for commentryId : 5 
   } // end method getCommentator()
   
   public String getFormattedDate(Calendar fileDate) throws Exception
   {
     String formattedDate="";
	  try  {
		  	SimpleDateFormat df = new SimpleDateFormat("M/d/yyyy h:mm:ss a");
		  	df.setTimeZone(TimeZone.getTimeZone("GMT"));
		  	formattedDate =df.format(fileDate.getTime());  
  	       }catch (Exception e) {
          		e.printStackTrace();
       }
	  return formattedDate;
    } // end getFormattedDate
	
} // end Class EMarketIntelliClient
