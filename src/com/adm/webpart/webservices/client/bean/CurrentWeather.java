package com.adm.webpart.webservices.client.bean;

public class CurrentWeather {
    private String displayName;
    private String iconURL;
    private String feelTemp;
    private String currTemp;
    private String highTemp;
    private String lowTemp;
    
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    
    public void setIconURL(String iconURL) {
        this.iconURL = iconURL;
    }
    
    public void setFeelTemp(String feelTemp) {
        this.feelTemp = feelTemp;
    }
    
    public void setCurrTemp(String currTemp) {
        this.currTemp = currTemp;
    }
    
    public void sethighTemp(String highTemp) {
        this.highTemp = highTemp;
    }
    
    public void setLowTemp(String lowTemp) {
        this.lowTemp = lowTemp;
    }

    public String getDisplayName() {
        return displayName;
    }
    
    public String getIconURL() {
        return iconURL;
    }
    
    public String getfeelTemp() {
        return feelTemp;
    }
    
    public String getCurrTemp() {
        return currTemp;
    }
    
    public String getHighTemp() {
        return highTemp;
    }
    
    public String getLowTemp() {
        return lowTemp;
    }

	@Override
	public String toString() {
		return "CurrentWeather [displayName=" + displayName + ", iconURL="
				+ iconURL + ", feelTemp=" + feelTemp + ", currTemp=" + currTemp
				+ ", highTemp=" + highTemp + ", lowTemp=" + lowTemp + "]";
	}


    
}
