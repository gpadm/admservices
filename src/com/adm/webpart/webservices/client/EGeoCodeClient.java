package com.adm.webpart.webservices.client;

import java.rmi.RemoteException;
import java.text.DecimalFormat;

import com.adm.webpart.webservices.BW_Location_Geo_Code.BW_Location_Geo_CodeSoap;
import com.adm.webpart.webservices.BW_Location_Geo_Code.BW_Location_Geo_CodeSoapProxy;
import com.adm.webpart.webservices.BW_Location_Geo_Code.Location;

public class EGeoCodeClient {

	BW_Location_Geo_CodeSoap client = new BW_Location_Geo_CodeSoapProxy();

	public void getLocationByCityState() throws Exception
	{
        Location rs1 =client.getLocationByCityState("Macon", "IL", null);
        Integer location_ID1 = rs1.getLocation_ID();
        String country_Name1 = rs1.getCountry_Name();
        
    	System.out.println("location_ID1-->"+ location_ID1);
    	System.out.println("country_Name1-->"+ country_Name1);
	}
	
	public Double getLatiguteByPostalCode(String postalCode) {
		Double latitude = null;
		Location rs = getLocationByPostalCode(postalCode);
		
        if(rs != null) {
	    	latitude = rs.getLatitude();
        }        
        return latitude;    	
	}

	public Double getLongitudeByPostalCode(String postalCode) {
		Double longitude = null;
		Location rs = getLocationByPostalCode(postalCode);
		
		if(rs != null) {
			longitude = rs.getLongitude();
		}        
		return longitude;    	
	}
	
	public Location getLocationByPostalCode(String postalCode) {
		Location rs = null;
		try {
			rs = client.getLocationByPostalCode(postalCode, null);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return rs;
	}

	public Location getLocationByCityState(String address) {
		Location rs = null;
		String[] tokens = address.split(",");

		try {
			rs = client.getLocationByCityState(tokens[0], tokens[1], null);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		return rs;
	}
	
	public static void main(String args[]) {
		EGeoCodeClient cl = new EGeoCodeClient();
		Location rs = cl.getLocationByCityState("fjdjf, dh");
		System.out.printf("getLocation_ID= %d\n, getPostal_Code = %d\n", rs.getLocation_ID(), rs.getPostal_Code());
		System.out.println("getLocation_ID= " + rs.getLatitude());
		
		
	}
	
	
}
