package com.adm.webpart.webservices.client;


import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.adm.webpart.beans.futures.Quotes;
import com.adm.webpart.parser.DelayedQuotesParser;
import com.adm.webpart.webservices.eSignalQuotes.ESignalQuotesSoap;
import com.adm.webpart.webservices.eSignalQuotes.ESignalQuotesSoapProxy;
import com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesResponseGetDelayedQuotesResult;

public class EFuturesClient {

	public Quotes getFutureQuote(String codeName) throws Exception
	{
		Quotes q = new Quotes();
		ESignalQuotesSoap client = new ESignalQuotesSoapProxy();
		GetDelayedQuotesResponseGetDelayedQuotesResult rs = 
				client.getDelayedQuotes(codeName, "", "", "", true, "", "");
	    try {
		org.apache.axis.message.MessageElement [] any = rs.get_any();
		q = DelayedQuotesParser.parse(any[0].getAsString());
	    }catch (Exception e) {
    		e.printStackTrace();
	    } // end catch 	
		
	    
	    
		return q;
	}  // end method getFutureQuote
	
	/* public String getRetrieved(String codeName) throws Exception
	{

		String returnString = "";
		ESignalQuotesSoap client = new ESignalQuotesSoapProxy();
		GetDelayedQuotesResponseGetDelayedQuotesResult rs = 
				client.getDelayedQuotes(codeName, "", "", "", true, "", "");
	    try {
		org.apache.axis.message.MessageElement [] any = rs.get_any();
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc =	 dBuilder.parse(new ByteArrayInputStream(any[0].getAsString().getBytes()));
        NodeList nList = doc.getElementsByTagName("quotes");
        Node nNode1 = nList.item(0);
		Element eElement1 = (Element) nNode1;
        returnString =  eElement1.getAttribute("retrieved");
        

	    }catch (Exception e) {
    		e.printStackTrace();
	    } // end catch 	
		
	    return returnString;
	} */  // end method getRetrieved
	
	
	public String getFormattedQuote(BigInteger bigInt) throws Exception
	{
		String returnString="";
		if(bigInt!=null)
		{
		int num = bigInt.intValue();
		String str = Integer.toString(num);
		
	    try {
		if(num>0 && str.length()>1)  // for eg : 12--> 1'2, 123 --> 12'3, 6557--> 655'7
		    returnString = str.substring(0,str.length()-1)+"'"+str.substring(str.length()-1);
		else if (num>=0 && str.length()==1) // for eg : 4--> 0'4
		    returnString ="0'"+str;
		else if (num<0 && str.length()>2) // for eg : -14 --> -1'4, -123 --> -12'3
			returnString = "-"+str.substring(1,str.length()-1)+"'"+str.substring(str.length()-1);
		else if (num<0 && str.length()==2)
		    returnString ="-0'"+str.substring(str.length()-1);  // for eg: -4 --> -0'4
	    	}catch (Exception e) {
    		e.printStackTrace();
	    } // end catch 	
		} // end if null check
		
		return returnString;
	} // end method getFormattedQuote
	
	public String getFormattedDate(String inputDate) throws Exception
	{
		
		String string = inputDate;
		SimpleDateFormat df = new SimpleDateFormat("E, MMM dd, yyyy H:mm:ss");
		
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		Date date = df.parse(string);	
		
		SimpleDateFormat print = new SimpleDateFormat("MM/dd/yy h:mm a z");   
		return(print.format(date));
    	
//    	Calendar currentdate = Calendar.getInstance();
//    	SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yy h:mm a");
//    	return (fmt.format(currentdate.getTime()));
    	
     
	
	} // end getFormattedDate
	
	public String[] getCodeList(List futureList) throws Exception
	
	{
		String[] returnList = new String[futureList.size()];
		for(int i=0;i<futureList.size();i++)
		{
			String temp= (String) futureList.get(i);
			returnList[i] = temp.substring(temp.lastIndexOf(("("))+1,temp.lastIndexOf(")"));
			
		} // end for loop
		
	  return returnList;
	} // end method getCodeList
	
}
