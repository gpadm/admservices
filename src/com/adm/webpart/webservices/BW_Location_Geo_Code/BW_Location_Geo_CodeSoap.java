/**
 * BW_Location_Geo_CodeSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Location_Geo_Code;

public interface BW_Location_Geo_CodeSoap extends java.rmi.Remote {
    public com.adm.webpart.webservices.BW_Location_Geo_Code.Location getLocationByCityState(java.lang.String city, java.lang.String state, java.lang.String subscriptionID) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.BW_Location_Geo_Code.Location getLocationByPostalCode(java.lang.String postalCode, java.lang.String subscriptionID) throws java.rmi.RemoteException;
    public java.lang.String getNearestPostalCodeByLatitudeLongitude(java.lang.String latitude, java.lang.String longitude) throws java.rmi.RemoteException;
    public java.lang.String[] get20NearestPostalCodesByLatitudeLongitude(java.lang.String latitude, java.lang.String longitude) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.BW_Location_Geo_Code.Coordinates getCoordinatesByPostalCode(java.lang.String postalCode) throws java.rmi.RemoteException;
}
