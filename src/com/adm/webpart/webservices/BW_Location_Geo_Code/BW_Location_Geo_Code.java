/**
 * BW_Location_Geo_Code.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Location_Geo_Code;

public interface BW_Location_Geo_Code extends javax.xml.rpc.Service {
    public java.lang.String getBW_Location_Geo_CodeSoapAddress();

    public com.adm.webpart.webservices.BW_Location_Geo_Code.BW_Location_Geo_CodeSoap getBW_Location_Geo_CodeSoap() throws javax.xml.rpc.ServiceException;

    public com.adm.webpart.webservices.BW_Location_Geo_Code.BW_Location_Geo_CodeSoap getBW_Location_Geo_CodeSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
