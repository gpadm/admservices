package com.adm.webpart.webservices.BW_Location_Geo_Code;

public class BW_Location_Geo_CodeSoapProxy implements com.adm.webpart.webservices.BW_Location_Geo_Code.BW_Location_Geo_CodeSoap {
  private String _endpoint = null;
  private com.adm.webpart.webservices.BW_Location_Geo_Code.BW_Location_Geo_CodeSoap bW_Location_Geo_CodeSoap = null;
  
  public BW_Location_Geo_CodeSoapProxy() {
    _initBW_Location_Geo_CodeSoapProxy();
  }
  
  public BW_Location_Geo_CodeSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initBW_Location_Geo_CodeSoapProxy();
  }
  
  private void _initBW_Location_Geo_CodeSoapProxy() {
    try {
      bW_Location_Geo_CodeSoap = (new com.adm.webpart.webservices.BW_Location_Geo_Code.BW_Location_Geo_CodeLocator()).getBW_Location_Geo_CodeSoap();
      if (bW_Location_Geo_CodeSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bW_Location_Geo_CodeSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bW_Location_Geo_CodeSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bW_Location_Geo_CodeSoap != null)
      ((javax.xml.rpc.Stub)bW_Location_Geo_CodeSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.adm.webpart.webservices.BW_Location_Geo_Code.BW_Location_Geo_CodeSoap getBW_Location_Geo_CodeSoap() {
    if (bW_Location_Geo_CodeSoap == null)
      _initBW_Location_Geo_CodeSoapProxy();
    return bW_Location_Geo_CodeSoap;
  }
  
  public com.adm.webpart.webservices.BW_Location_Geo_Code.Location getLocationByCityState(java.lang.String city, java.lang.String state, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Location_Geo_CodeSoap == null)
      _initBW_Location_Geo_CodeSoapProxy();
    return bW_Location_Geo_CodeSoap.getLocationByCityState(city, state, subscriptionID);
  }
  
  public com.adm.webpart.webservices.BW_Location_Geo_Code.Location getLocationByPostalCode(java.lang.String postalCode, java.lang.String subscriptionID) throws java.rmi.RemoteException{
    if (bW_Location_Geo_CodeSoap == null)
      _initBW_Location_Geo_CodeSoapProxy();
    return bW_Location_Geo_CodeSoap.getLocationByPostalCode(postalCode, subscriptionID);
  }
  
  public java.lang.String getNearestPostalCodeByLatitudeLongitude(java.lang.String latitude, java.lang.String longitude) throws java.rmi.RemoteException{
    if (bW_Location_Geo_CodeSoap == null)
      _initBW_Location_Geo_CodeSoapProxy();
    return bW_Location_Geo_CodeSoap.getNearestPostalCodeByLatitudeLongitude(latitude, longitude);
  }
  
  public java.lang.String[] get20NearestPostalCodesByLatitudeLongitude(java.lang.String latitude, java.lang.String longitude) throws java.rmi.RemoteException{
    if (bW_Location_Geo_CodeSoap == null)
      _initBW_Location_Geo_CodeSoapProxy();
    return bW_Location_Geo_CodeSoap.get20NearestPostalCodesByLatitudeLongitude(latitude, longitude);
  }
  
  public com.adm.webpart.webservices.BW_Location_Geo_Code.Coordinates getCoordinatesByPostalCode(java.lang.String postalCode) throws java.rmi.RemoteException{
    if (bW_Location_Geo_CodeSoap == null)
      _initBW_Location_Geo_CodeSoapProxy();
    return bW_Location_Geo_CodeSoap.getCoordinatesByPostalCode(postalCode);
  }
  
  
}