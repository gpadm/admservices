/**
 * Location.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Location_Geo_Code;

public class Location  implements java.io.Serializable {
    private int location_ID;

    private java.lang.String country_Name;

    private java.lang.String postal_Code;

    private java.lang.String postal_Type;

    private java.lang.String city_Name;

    private java.lang.String city_Type;

    private java.lang.String county_Name;

    private int county_FIPS;

    private java.lang.String province_Name;

    private java.lang.String province_Abbr;

    private int state_FIPS;

    private int MSA_Code;

    private java.lang.String area_Code;

    private java.lang.String time_Zone;

    private double UTC;

    private java.lang.String DST;

    private double latitude;

    private double longitude;

    public Location() {
    }

    public Location(
           int location_ID,
           java.lang.String country_Name,
           java.lang.String postal_Code,
           java.lang.String postal_Type,
           java.lang.String city_Name,
           java.lang.String city_Type,
           java.lang.String county_Name,
           int county_FIPS,
           java.lang.String province_Name,
           java.lang.String province_Abbr,
           int state_FIPS,
           int MSA_Code,
           java.lang.String area_Code,
           java.lang.String time_Zone,
           double UTC,
           java.lang.String DST,
           double latitude,
           double longitude) {
           this.location_ID = location_ID;
           this.country_Name = country_Name;
           this.postal_Code = postal_Code;
           this.postal_Type = postal_Type;
           this.city_Name = city_Name;
           this.city_Type = city_Type;
           this.county_Name = county_Name;
           this.county_FIPS = county_FIPS;
           this.province_Name = province_Name;
           this.province_Abbr = province_Abbr;
           this.state_FIPS = state_FIPS;
           this.MSA_Code = MSA_Code;
           this.area_Code = area_Code;
           this.time_Zone = time_Zone;
           this.UTC = UTC;
           this.DST = DST;
           this.latitude = latitude;
           this.longitude = longitude;
    }


    /**
     * Gets the location_ID value for this Location.
     * 
     * @return location_ID
     */
    public int getLocation_ID() {
        return location_ID;
    }


    /**
     * Sets the location_ID value for this Location.
     * 
     * @param location_ID
     */
    public void setLocation_ID(int location_ID) {
        this.location_ID = location_ID;
    }


    /**
     * Gets the country_Name value for this Location.
     * 
     * @return country_Name
     */
    public java.lang.String getCountry_Name() {
        return country_Name;
    }


    /**
     * Sets the country_Name value for this Location.
     * 
     * @param country_Name
     */
    public void setCountry_Name(java.lang.String country_Name) {
        this.country_Name = country_Name;
    }


    /**
     * Gets the postal_Code value for this Location.
     * 
     * @return postal_Code
     */
    public java.lang.String getPostal_Code() {
        return postal_Code;
    }


    /**
     * Sets the postal_Code value for this Location.
     * 
     * @param postal_Code
     */
    public void setPostal_Code(java.lang.String postal_Code) {
        this.postal_Code = postal_Code;
    }


    /**
     * Gets the postal_Type value for this Location.
     * 
     * @return postal_Type
     */
    public java.lang.String getPostal_Type() {
        return postal_Type;
    }


    /**
     * Sets the postal_Type value for this Location.
     * 
     * @param postal_Type
     */
    public void setPostal_Type(java.lang.String postal_Type) {
        this.postal_Type = postal_Type;
    }


    /**
     * Gets the city_Name value for this Location.
     * 
     * @return city_Name
     */
    public java.lang.String getCity_Name() {
        return city_Name;
    }


    /**
     * Sets the city_Name value for this Location.
     * 
     * @param city_Name
     */
    public void setCity_Name(java.lang.String city_Name) {
        this.city_Name = city_Name;
    }


    /**
     * Gets the city_Type value for this Location.
     * 
     * @return city_Type
     */
    public java.lang.String getCity_Type() {
        return city_Type;
    }


    /**
     * Sets the city_Type value for this Location.
     * 
     * @param city_Type
     */
    public void setCity_Type(java.lang.String city_Type) {
        this.city_Type = city_Type;
    }


    /**
     * Gets the county_Name value for this Location.
     * 
     * @return county_Name
     */
    public java.lang.String getCounty_Name() {
        return county_Name;
    }


    /**
     * Sets the county_Name value for this Location.
     * 
     * @param county_Name
     */
    public void setCounty_Name(java.lang.String county_Name) {
        this.county_Name = county_Name;
    }


    /**
     * Gets the county_FIPS value for this Location.
     * 
     * @return county_FIPS
     */
    public int getCounty_FIPS() {
        return county_FIPS;
    }


    /**
     * Sets the county_FIPS value for this Location.
     * 
     * @param county_FIPS
     */
    public void setCounty_FIPS(int county_FIPS) {
        this.county_FIPS = county_FIPS;
    }


    /**
     * Gets the province_Name value for this Location.
     * 
     * @return province_Name
     */
    public java.lang.String getProvince_Name() {
        return province_Name;
    }


    /**
     * Sets the province_Name value for this Location.
     * 
     * @param province_Name
     */
    public void setProvince_Name(java.lang.String province_Name) {
        this.province_Name = province_Name;
    }


    /**
     * Gets the province_Abbr value for this Location.
     * 
     * @return province_Abbr
     */
    public java.lang.String getProvince_Abbr() {
        return province_Abbr;
    }


    /**
     * Sets the province_Abbr value for this Location.
     * 
     * @param province_Abbr
     */
    public void setProvince_Abbr(java.lang.String province_Abbr) {
        this.province_Abbr = province_Abbr;
    }


    /**
     * Gets the state_FIPS value for this Location.
     * 
     * @return state_FIPS
     */
    public int getState_FIPS() {
        return state_FIPS;
    }


    /**
     * Sets the state_FIPS value for this Location.
     * 
     * @param state_FIPS
     */
    public void setState_FIPS(int state_FIPS) {
        this.state_FIPS = state_FIPS;
    }


    /**
     * Gets the MSA_Code value for this Location.
     * 
     * @return MSA_Code
     */
    public int getMSA_Code() {
        return MSA_Code;
    }


    /**
     * Sets the MSA_Code value for this Location.
     * 
     * @param MSA_Code
     */
    public void setMSA_Code(int MSA_Code) {
        this.MSA_Code = MSA_Code;
    }


    /**
     * Gets the area_Code value for this Location.
     * 
     * @return area_Code
     */
    public java.lang.String getArea_Code() {
        return area_Code;
    }


    /**
     * Sets the area_Code value for this Location.
     * 
     * @param area_Code
     */
    public void setArea_Code(java.lang.String area_Code) {
        this.area_Code = area_Code;
    }


    /**
     * Gets the time_Zone value for this Location.
     * 
     * @return time_Zone
     */
    public java.lang.String getTime_Zone() {
        return time_Zone;
    }


    /**
     * Sets the time_Zone value for this Location.
     * 
     * @param time_Zone
     */
    public void setTime_Zone(java.lang.String time_Zone) {
        this.time_Zone = time_Zone;
    }


    /**
     * Gets the UTC value for this Location.
     * 
     * @return UTC
     */
    public double getUTC() {
        return UTC;
    }


    /**
     * Sets the UTC value for this Location.
     * 
     * @param UTC
     */
    public void setUTC(double UTC) {
        this.UTC = UTC;
    }


    /**
     * Gets the DST value for this Location.
     * 
     * @return DST
     */
    public java.lang.String getDST() {
        return DST;
    }


    /**
     * Sets the DST value for this Location.
     * 
     * @param DST
     */
    public void setDST(java.lang.String DST) {
        this.DST = DST;
    }


    /**
     * Gets the latitude value for this Location.
     * 
     * @return latitude
     */
    public double getLatitude() {
        return latitude;
    }


    /**
     * Sets the latitude value for this Location.
     * 
     * @param latitude
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude value for this Location.
     * 
     * @return longitude
     */
    public double getLongitude() {
        return longitude;
    }


    /**
     * Sets the longitude value for this Location.
     * 
     * @param longitude
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Location)) return false;
        Location other = (Location) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.location_ID == other.getLocation_ID() &&
            ((this.country_Name==null && other.getCountry_Name()==null) || 
             (this.country_Name!=null &&
              this.country_Name.equals(other.getCountry_Name()))) &&
            ((this.postal_Code==null && other.getPostal_Code()==null) || 
             (this.postal_Code!=null &&
              this.postal_Code.equals(other.getPostal_Code()))) &&
            ((this.postal_Type==null && other.getPostal_Type()==null) || 
             (this.postal_Type!=null &&
              this.postal_Type.equals(other.getPostal_Type()))) &&
            ((this.city_Name==null && other.getCity_Name()==null) || 
             (this.city_Name!=null &&
              this.city_Name.equals(other.getCity_Name()))) &&
            ((this.city_Type==null && other.getCity_Type()==null) || 
             (this.city_Type!=null &&
              this.city_Type.equals(other.getCity_Type()))) &&
            ((this.county_Name==null && other.getCounty_Name()==null) || 
             (this.county_Name!=null &&
              this.county_Name.equals(other.getCounty_Name()))) &&
            this.county_FIPS == other.getCounty_FIPS() &&
            ((this.province_Name==null && other.getProvince_Name()==null) || 
             (this.province_Name!=null &&
              this.province_Name.equals(other.getProvince_Name()))) &&
            ((this.province_Abbr==null && other.getProvince_Abbr()==null) || 
             (this.province_Abbr!=null &&
              this.province_Abbr.equals(other.getProvince_Abbr()))) &&
            this.state_FIPS == other.getState_FIPS() &&
            this.MSA_Code == other.getMSA_Code() &&
            ((this.area_Code==null && other.getArea_Code()==null) || 
             (this.area_Code!=null &&
              this.area_Code.equals(other.getArea_Code()))) &&
            ((this.time_Zone==null && other.getTime_Zone()==null) || 
             (this.time_Zone!=null &&
              this.time_Zone.equals(other.getTime_Zone()))) &&
            this.UTC == other.getUTC() &&
            ((this.DST==null && other.getDST()==null) || 
             (this.DST!=null &&
              this.DST.equals(other.getDST()))) &&
            this.latitude == other.getLatitude() &&
            this.longitude == other.getLongitude();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getLocation_ID();
        if (getCountry_Name() != null) {
            _hashCode += getCountry_Name().hashCode();
        }
        if (getPostal_Code() != null) {
            _hashCode += getPostal_Code().hashCode();
        }
        if (getPostal_Type() != null) {
            _hashCode += getPostal_Type().hashCode();
        }
        if (getCity_Name() != null) {
            _hashCode += getCity_Name().hashCode();
        }
        if (getCity_Type() != null) {
            _hashCode += getCity_Type().hashCode();
        }
        if (getCounty_Name() != null) {
            _hashCode += getCounty_Name().hashCode();
        }
        _hashCode += getCounty_FIPS();
        if (getProvince_Name() != null) {
            _hashCode += getProvince_Name().hashCode();
        }
        if (getProvince_Abbr() != null) {
            _hashCode += getProvince_Abbr().hashCode();
        }
        _hashCode += getState_FIPS();
        _hashCode += getMSA_Code();
        if (getArea_Code() != null) {
            _hashCode += getArea_Code().hashCode();
        }
        if (getTime_Zone() != null) {
            _hashCode += getTime_Zone().hashCode();
        }
        _hashCode += new Double(getUTC()).hashCode();
        if (getDST() != null) {
            _hashCode += getDST().hashCode();
        }
        _hashCode += new Double(getLatitude()).hashCode();
        _hashCode += new Double(getLongitude()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Location.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "Location"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("location_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "Location_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country_Name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "Country_Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postal_Code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "Postal_Code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postal_Type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "Postal_Type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city_Name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "City_Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city_Type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "City_Type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("county_Name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "County_Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("county_FIPS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "County_FIPS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("province_Name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "Province_Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("province_Abbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "Province_Abbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state_FIPS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "State_FIPS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MSA_Code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "MSA_Code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("area_Code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "Area_Code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("time_Zone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "Time_Zone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UTC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "UTC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DST");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "DST"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "Latitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "Longitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	@Override
	public String toString() {
		return "Location [location_ID=" + location_ID + ", country_Name=" + country_Name + ", postal_Code="
				+ postal_Code + ", postal_Type=" + postal_Type + ", city_Name=" + city_Name + ", city_Type=" + city_Type
				+ ", county_Name=" + county_Name + ", county_FIPS=" + county_FIPS + ", province_Name=" + province_Name
				+ ", province_Abbr=" + province_Abbr + ", state_FIPS=" + state_FIPS + ", MSA_Code=" + MSA_Code
				+ ", area_Code=" + area_Code + ", time_Zone=" + time_Zone + ", UTC=" + UTC + ", DST=" + DST
				+ ", latitude=" + latitude + ", longitude=" + longitude + ", __equalsCalc=" + __equalsCalc
				+ ", __hashCodeCalc=" + __hashCodeCalc + "]";
	}
    
    

}
