/**
 * BW_Location_Geo_CodeLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.BW_Location_Geo_Code;

public class BW_Location_Geo_CodeLocator extends org.apache.axis.client.Service implements com.adm.webpart.webservices.BW_Location_Geo_Code.BW_Location_Geo_Code {

    public BW_Location_Geo_CodeLocator() {
    }


    public BW_Location_Geo_CodeLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BW_Location_Geo_CodeLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BW_Location_Geo_CodeSoap
    private java.lang.String BW_Location_Geo_CodeSoap_address = "http://dev.webservices.adm.com/BW_Location_Geo_Code/BW_Location_Geo_Code.asmx";

    public java.lang.String getBW_Location_Geo_CodeSoapAddress() {
        return BW_Location_Geo_CodeSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BW_Location_Geo_CodeSoapWSDDServiceName = "BW_Location_Geo_CodeSoap";

    public java.lang.String getBW_Location_Geo_CodeSoapWSDDServiceName() {
        return BW_Location_Geo_CodeSoapWSDDServiceName;
    }

    public void setBW_Location_Geo_CodeSoapWSDDServiceName(java.lang.String name) {
        BW_Location_Geo_CodeSoapWSDDServiceName = name;
    }

    public com.adm.webpart.webservices.BW_Location_Geo_Code.BW_Location_Geo_CodeSoap getBW_Location_Geo_CodeSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BW_Location_Geo_CodeSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBW_Location_Geo_CodeSoap(endpoint);
    }

    public com.adm.webpart.webservices.BW_Location_Geo_Code.BW_Location_Geo_CodeSoap getBW_Location_Geo_CodeSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.adm.webpart.webservices.BW_Location_Geo_Code.BW_Location_Geo_CodeSoapStub _stub = new com.adm.webpart.webservices.BW_Location_Geo_Code.BW_Location_Geo_CodeSoapStub(portAddress, this);
            _stub.setPortName(getBW_Location_Geo_CodeSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBW_Location_Geo_CodeSoapEndpointAddress(java.lang.String address) {
        BW_Location_Geo_CodeSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.adm.webpart.webservices.BW_Location_Geo_Code.BW_Location_Geo_CodeSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.adm.webpart.webservices.BW_Location_Geo_Code.BW_Location_Geo_CodeSoapStub _stub = new com.adm.webpart.webservices.BW_Location_Geo_Code.BW_Location_Geo_CodeSoapStub(new java.net.URL(BW_Location_Geo_CodeSoap_address), this);
                _stub.setPortName(getBW_Location_Geo_CodeSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BW_Location_Geo_CodeSoap".equals(inputPortName)) {
            return getBW_Location_Geo_CodeSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "BW_Location_Geo_Code");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webservices.adm.com/BW_Location_Geo_Code", "BW_Location_Geo_CodeSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BW_Location_Geo_CodeSoap".equals(portName)) {
            setBW_Location_Geo_CodeSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
