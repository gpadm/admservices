/**
 * GetLiveQuotesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.eSignalQuotes;

public class GetLiveQuotesResponse  implements java.io.Serializable {
    private com.adm.webpart.webservices.eSignalQuotes.GetLiveQuotesResponseGetLiveQuotesResult getLiveQuotesResult;

    public GetLiveQuotesResponse() {
    }

    public GetLiveQuotesResponse(
           com.adm.webpart.webservices.eSignalQuotes.GetLiveQuotesResponseGetLiveQuotesResult getLiveQuotesResult) {
           this.getLiveQuotesResult = getLiveQuotesResult;
    }


    /**
     * Gets the getLiveQuotesResult value for this GetLiveQuotesResponse.
     * 
     * @return getLiveQuotesResult
     */
    public com.adm.webpart.webservices.eSignalQuotes.GetLiveQuotesResponseGetLiveQuotesResult getGetLiveQuotesResult() {
        return getLiveQuotesResult;
    }


    /**
     * Sets the getLiveQuotesResult value for this GetLiveQuotesResponse.
     * 
     * @param getLiveQuotesResult
     */
    public void setGetLiveQuotesResult(com.adm.webpart.webservices.eSignalQuotes.GetLiveQuotesResponseGetLiveQuotesResult getLiveQuotesResult) {
        this.getLiveQuotesResult = getLiveQuotesResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetLiveQuotesResponse)) return false;
        GetLiveQuotesResponse other = (GetLiveQuotesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getLiveQuotesResult==null && other.getGetLiveQuotesResult()==null) || 
             (this.getLiveQuotesResult!=null &&
              this.getLiveQuotesResult.equals(other.getGetLiveQuotesResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetLiveQuotesResult() != null) {
            _hashCode += getGetLiveQuotesResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetLiveQuotesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetLiveQuotesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getLiveQuotesResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetLiveQuotesResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetLiveQuotesResponse>GetLiveQuotesResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
