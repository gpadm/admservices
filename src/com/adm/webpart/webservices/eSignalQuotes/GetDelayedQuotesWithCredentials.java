/**
 * GetDelayedQuotesWithCredentials.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.eSignalQuotes;

public class GetDelayedQuotesWithCredentials  implements java.io.Serializable {
    private java.lang.String symbols;

    private java.lang.String cusip;

    private java.lang.String fields;

    private java.lang.String type;

    private boolean dispfullname;

    private java.lang.String datefmt;

    private java.lang.String timefmt;

    private java.lang.String userAccount;

    private java.lang.String teamName;

    private java.lang.String applicationName;

    private java.lang.String webPageName;

    public GetDelayedQuotesWithCredentials() {
    }

    public GetDelayedQuotesWithCredentials(
           java.lang.String symbols,
           java.lang.String cusip,
           java.lang.String fields,
           java.lang.String type,
           boolean dispfullname,
           java.lang.String datefmt,
           java.lang.String timefmt,
           java.lang.String userAccount,
           java.lang.String teamName,
           java.lang.String applicationName,
           java.lang.String webPageName) {
           this.symbols = symbols;
           this.cusip = cusip;
           this.fields = fields;
           this.type = type;
           this.dispfullname = dispfullname;
           this.datefmt = datefmt;
           this.timefmt = timefmt;
           this.userAccount = userAccount;
           this.teamName = teamName;
           this.applicationName = applicationName;
           this.webPageName = webPageName;
    }


    /**
     * Gets the symbols value for this GetDelayedQuotesWithCredentials.
     * 
     * @return symbols
     */
    public java.lang.String getSymbols() {
        return symbols;
    }


    /**
     * Sets the symbols value for this GetDelayedQuotesWithCredentials.
     * 
     * @param symbols
     */
    public void setSymbols(java.lang.String symbols) {
        this.symbols = symbols;
    }


    /**
     * Gets the cusip value for this GetDelayedQuotesWithCredentials.
     * 
     * @return cusip
     */
    public java.lang.String getCusip() {
        return cusip;
    }


    /**
     * Sets the cusip value for this GetDelayedQuotesWithCredentials.
     * 
     * @param cusip
     */
    public void setCusip(java.lang.String cusip) {
        this.cusip = cusip;
    }


    /**
     * Gets the fields value for this GetDelayedQuotesWithCredentials.
     * 
     * @return fields
     */
    public java.lang.String getFields() {
        return fields;
    }


    /**
     * Sets the fields value for this GetDelayedQuotesWithCredentials.
     * 
     * @param fields
     */
    public void setFields(java.lang.String fields) {
        this.fields = fields;
    }


    /**
     * Gets the type value for this GetDelayedQuotesWithCredentials.
     * 
     * @return type
     */
    public java.lang.String getType() {
        return type;
    }


    /**
     * Sets the type value for this GetDelayedQuotesWithCredentials.
     * 
     * @param type
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }


    /**
     * Gets the dispfullname value for this GetDelayedQuotesWithCredentials.
     * 
     * @return dispfullname
     */
    public boolean isDispfullname() {
        return dispfullname;
    }


    /**
     * Sets the dispfullname value for this GetDelayedQuotesWithCredentials.
     * 
     * @param dispfullname
     */
    public void setDispfullname(boolean dispfullname) {
        this.dispfullname = dispfullname;
    }


    /**
     * Gets the datefmt value for this GetDelayedQuotesWithCredentials.
     * 
     * @return datefmt
     */
    public java.lang.String getDatefmt() {
        return datefmt;
    }


    /**
     * Sets the datefmt value for this GetDelayedQuotesWithCredentials.
     * 
     * @param datefmt
     */
    public void setDatefmt(java.lang.String datefmt) {
        this.datefmt = datefmt;
    }


    /**
     * Gets the timefmt value for this GetDelayedQuotesWithCredentials.
     * 
     * @return timefmt
     */
    public java.lang.String getTimefmt() {
        return timefmt;
    }


    /**
     * Sets the timefmt value for this GetDelayedQuotesWithCredentials.
     * 
     * @param timefmt
     */
    public void setTimefmt(java.lang.String timefmt) {
        this.timefmt = timefmt;
    }


    /**
     * Gets the userAccount value for this GetDelayedQuotesWithCredentials.
     * 
     * @return userAccount
     */
    public java.lang.String getUserAccount() {
        return userAccount;
    }


    /**
     * Sets the userAccount value for this GetDelayedQuotesWithCredentials.
     * 
     * @param userAccount
     */
    public void setUserAccount(java.lang.String userAccount) {
        this.userAccount = userAccount;
    }


    /**
     * Gets the teamName value for this GetDelayedQuotesWithCredentials.
     * 
     * @return teamName
     */
    public java.lang.String getTeamName() {
        return teamName;
    }


    /**
     * Sets the teamName value for this GetDelayedQuotesWithCredentials.
     * 
     * @param teamName
     */
    public void setTeamName(java.lang.String teamName) {
        this.teamName = teamName;
    }


    /**
     * Gets the applicationName value for this GetDelayedQuotesWithCredentials.
     * 
     * @return applicationName
     */
    public java.lang.String getApplicationName() {
        return applicationName;
    }


    /**
     * Sets the applicationName value for this GetDelayedQuotesWithCredentials.
     * 
     * @param applicationName
     */
    public void setApplicationName(java.lang.String applicationName) {
        this.applicationName = applicationName;
    }


    /**
     * Gets the webPageName value for this GetDelayedQuotesWithCredentials.
     * 
     * @return webPageName
     */
    public java.lang.String getWebPageName() {
        return webPageName;
    }


    /**
     * Sets the webPageName value for this GetDelayedQuotesWithCredentials.
     * 
     * @param webPageName
     */
    public void setWebPageName(java.lang.String webPageName) {
        this.webPageName = webPageName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetDelayedQuotesWithCredentials)) return false;
        GetDelayedQuotesWithCredentials other = (GetDelayedQuotesWithCredentials) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.symbols==null && other.getSymbols()==null) || 
             (this.symbols!=null &&
              this.symbols.equals(other.getSymbols()))) &&
            ((this.cusip==null && other.getCusip()==null) || 
             (this.cusip!=null &&
              this.cusip.equals(other.getCusip()))) &&
            ((this.fields==null && other.getFields()==null) || 
             (this.fields!=null &&
              this.fields.equals(other.getFields()))) &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType()))) &&
            this.dispfullname == other.isDispfullname() &&
            ((this.datefmt==null && other.getDatefmt()==null) || 
             (this.datefmt!=null &&
              this.datefmt.equals(other.getDatefmt()))) &&
            ((this.timefmt==null && other.getTimefmt()==null) || 
             (this.timefmt!=null &&
              this.timefmt.equals(other.getTimefmt()))) &&
            ((this.userAccount==null && other.getUserAccount()==null) || 
             (this.userAccount!=null &&
              this.userAccount.equals(other.getUserAccount()))) &&
            ((this.teamName==null && other.getTeamName()==null) || 
             (this.teamName!=null &&
              this.teamName.equals(other.getTeamName()))) &&
            ((this.applicationName==null && other.getApplicationName()==null) || 
             (this.applicationName!=null &&
              this.applicationName.equals(other.getApplicationName()))) &&
            ((this.webPageName==null && other.getWebPageName()==null) || 
             (this.webPageName!=null &&
              this.webPageName.equals(other.getWebPageName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSymbols() != null) {
            _hashCode += getSymbols().hashCode();
        }
        if (getCusip() != null) {
            _hashCode += getCusip().hashCode();
        }
        if (getFields() != null) {
            _hashCode += getFields().hashCode();
        }
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        _hashCode += (isDispfullname() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getDatefmt() != null) {
            _hashCode += getDatefmt().hashCode();
        }
        if (getTimefmt() != null) {
            _hashCode += getTimefmt().hashCode();
        }
        if (getUserAccount() != null) {
            _hashCode += getUserAccount().hashCode();
        }
        if (getTeamName() != null) {
            _hashCode += getTeamName().hashCode();
        }
        if (getApplicationName() != null) {
            _hashCode += getApplicationName().hashCode();
        }
        if (getWebPageName() != null) {
            _hashCode += getWebPageName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetDelayedQuotesWithCredentials.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetDelayedQuotesWithCredentials"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("symbols");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "symbols"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cusip");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "cusip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fields");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "fields"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dispfullname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "dispfullname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datefmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "datefmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timefmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "timefmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userAccount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "userAccount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("teamName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "teamName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("applicationName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "applicationName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("webPageName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "webPageName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
