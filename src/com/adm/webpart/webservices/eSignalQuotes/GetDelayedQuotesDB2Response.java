/**
 * GetDelayedQuotesDB2Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */


package com.adm.webpart.webservices.eSignalQuotes;

public class GetDelayedQuotesDB2Response  implements java.io.Serializable {
    private com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2ResponseGetDelayedQuotesDB2Result getDelayedQuotesDB2Result;

    public GetDelayedQuotesDB2Response() {
    }

    public GetDelayedQuotesDB2Response(
           com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2ResponseGetDelayedQuotesDB2Result getDelayedQuotesDB2Result) {
           this.getDelayedQuotesDB2Result = getDelayedQuotesDB2Result;
    }


    /**
     * Gets the getDelayedQuotesDB2Result value for this GetDelayedQuotesDB2Response.
     * 
     * @return getDelayedQuotesDB2Result
     */
    public com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2ResponseGetDelayedQuotesDB2Result getGetDelayedQuotesDB2Result() {
        return getDelayedQuotesDB2Result;
    }


    /**
     * Sets the getDelayedQuotesDB2Result value for this GetDelayedQuotesDB2Response.
     * 
     * @param getDelayedQuotesDB2Result
     */
    public void setGetDelayedQuotesDB2Result(com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2ResponseGetDelayedQuotesDB2Result getDelayedQuotesDB2Result) {
        this.getDelayedQuotesDB2Result = getDelayedQuotesDB2Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetDelayedQuotesDB2Response)) return false;
        GetDelayedQuotesDB2Response other = (GetDelayedQuotesDB2Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getDelayedQuotesDB2Result==null && other.getGetDelayedQuotesDB2Result()==null) || 
             (this.getDelayedQuotesDB2Result!=null &&
              this.getDelayedQuotesDB2Result.equals(other.getGetDelayedQuotesDB2Result())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetDelayedQuotesDB2Result() != null) {
            _hashCode += getGetDelayedQuotesDB2Result().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetDelayedQuotesDB2Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetDelayedQuotesDB2Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getDelayedQuotesDB2Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetDelayedQuotesDB2Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetDelayedQuotesDB2Response>GetDelayedQuotesDB2Result"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
