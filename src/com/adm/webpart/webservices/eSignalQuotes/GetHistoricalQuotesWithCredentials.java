/**
 * GetHistoricalQuotesWithCredentials.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.eSignalQuotes;

public class GetHistoricalQuotesWithCredentials  implements java.io.Serializable {
    private java.lang.String symbol;

    private java.lang.String period;

    private int recordnum;

    private java.lang.String startdate;

    private java.lang.String enddate;

    private java.lang.String range;

    private boolean decimalNumbers;

    private java.lang.String userAccount;

    private java.lang.String teamName;

    private java.lang.String applicationName;

    private java.lang.String webPageName;

    public GetHistoricalQuotesWithCredentials() {
    }

    public GetHistoricalQuotesWithCredentials(
           java.lang.String symbol,
           java.lang.String period,
           int recordnum,
           java.lang.String startdate,
           java.lang.String enddate,
           java.lang.String range,
           boolean decimalNumbers,
           java.lang.String userAccount,
           java.lang.String teamName,
           java.lang.String applicationName,
           java.lang.String webPageName) {
           this.symbol = symbol;
           this.period = period;
           this.recordnum = recordnum;
           this.startdate = startdate;
           this.enddate = enddate;
           this.range = range;
           this.decimalNumbers = decimalNumbers;
           this.userAccount = userAccount;
           this.teamName = teamName;
           this.applicationName = applicationName;
           this.webPageName = webPageName;
    }


    /**
     * Gets the symbol value for this GetHistoricalQuotesWithCredentials.
     * 
     * @return symbol
     */
    public java.lang.String getSymbol() {
        return symbol;
    }


    /**
     * Sets the symbol value for this GetHistoricalQuotesWithCredentials.
     * 
     * @param symbol
     */
    public void setSymbol(java.lang.String symbol) {
        this.symbol = symbol;
    }


    /**
     * Gets the period value for this GetHistoricalQuotesWithCredentials.
     * 
     * @return period
     */
    public java.lang.String getPeriod() {
        return period;
    }


    /**
     * Sets the period value for this GetHistoricalQuotesWithCredentials.
     * 
     * @param period
     */
    public void setPeriod(java.lang.String period) {
        this.period = period;
    }


    /**
     * Gets the recordnum value for this GetHistoricalQuotesWithCredentials.
     * 
     * @return recordnum
     */
    public int getRecordnum() {
        return recordnum;
    }


    /**
     * Sets the recordnum value for this GetHistoricalQuotesWithCredentials.
     * 
     * @param recordnum
     */
    public void setRecordnum(int recordnum) {
        this.recordnum = recordnum;
    }


    /**
     * Gets the startdate value for this GetHistoricalQuotesWithCredentials.
     * 
     * @return startdate
     */
    public java.lang.String getStartdate() {
        return startdate;
    }


    /**
     * Sets the startdate value for this GetHistoricalQuotesWithCredentials.
     * 
     * @param startdate
     */
    public void setStartdate(java.lang.String startdate) {
        this.startdate = startdate;
    }


    /**
     * Gets the enddate value for this GetHistoricalQuotesWithCredentials.
     * 
     * @return enddate
     */
    public java.lang.String getEnddate() {
        return enddate;
    }


    /**
     * Sets the enddate value for this GetHistoricalQuotesWithCredentials.
     * 
     * @param enddate
     */
    public void setEnddate(java.lang.String enddate) {
        this.enddate = enddate;
    }


    /**
     * Gets the range value for this GetHistoricalQuotesWithCredentials.
     * 
     * @return range
     */
    public java.lang.String getRange() {
        return range;
    }


    /**
     * Sets the range value for this GetHistoricalQuotesWithCredentials.
     * 
     * @param range
     */
    public void setRange(java.lang.String range) {
        this.range = range;
    }


    /**
     * Gets the decimalNumbers value for this GetHistoricalQuotesWithCredentials.
     * 
     * @return decimalNumbers
     */
    public boolean isDecimalNumbers() {
        return decimalNumbers;
    }


    /**
     * Sets the decimalNumbers value for this GetHistoricalQuotesWithCredentials.
     * 
     * @param decimalNumbers
     */
    public void setDecimalNumbers(boolean decimalNumbers) {
        this.decimalNumbers = decimalNumbers;
    }


    /**
     * Gets the userAccount value for this GetHistoricalQuotesWithCredentials.
     * 
     * @return userAccount
     */
    public java.lang.String getUserAccount() {
        return userAccount;
    }


    /**
     * Sets the userAccount value for this GetHistoricalQuotesWithCredentials.
     * 
     * @param userAccount
     */
    public void setUserAccount(java.lang.String userAccount) {
        this.userAccount = userAccount;
    }


    /**
     * Gets the teamName value for this GetHistoricalQuotesWithCredentials.
     * 
     * @return teamName
     */
    public java.lang.String getTeamName() {
        return teamName;
    }


    /**
     * Sets the teamName value for this GetHistoricalQuotesWithCredentials.
     * 
     * @param teamName
     */
    public void setTeamName(java.lang.String teamName) {
        this.teamName = teamName;
    }


    /**
     * Gets the applicationName value for this GetHistoricalQuotesWithCredentials.
     * 
     * @return applicationName
     */
    public java.lang.String getApplicationName() {
        return applicationName;
    }


    /**
     * Sets the applicationName value for this GetHistoricalQuotesWithCredentials.
     * 
     * @param applicationName
     */
    public void setApplicationName(java.lang.String applicationName) {
        this.applicationName = applicationName;
    }


    /**
     * Gets the webPageName value for this GetHistoricalQuotesWithCredentials.
     * 
     * @return webPageName
     */
    public java.lang.String getWebPageName() {
        return webPageName;
    }


    /**
     * Sets the webPageName value for this GetHistoricalQuotesWithCredentials.
     * 
     * @param webPageName
     */
    public void setWebPageName(java.lang.String webPageName) {
        this.webPageName = webPageName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetHistoricalQuotesWithCredentials)) return false;
        GetHistoricalQuotesWithCredentials other = (GetHistoricalQuotesWithCredentials) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.symbol==null && other.getSymbol()==null) || 
             (this.symbol!=null &&
              this.symbol.equals(other.getSymbol()))) &&
            ((this.period==null && other.getPeriod()==null) || 
             (this.period!=null &&
              this.period.equals(other.getPeriod()))) &&
            this.recordnum == other.getRecordnum() &&
            ((this.startdate==null && other.getStartdate()==null) || 
             (this.startdate!=null &&
              this.startdate.equals(other.getStartdate()))) &&
            ((this.enddate==null && other.getEnddate()==null) || 
             (this.enddate!=null &&
              this.enddate.equals(other.getEnddate()))) &&
            ((this.range==null && other.getRange()==null) || 
             (this.range!=null &&
              this.range.equals(other.getRange()))) &&
            this.decimalNumbers == other.isDecimalNumbers() &&
            ((this.userAccount==null && other.getUserAccount()==null) || 
             (this.userAccount!=null &&
              this.userAccount.equals(other.getUserAccount()))) &&
            ((this.teamName==null && other.getTeamName()==null) || 
             (this.teamName!=null &&
              this.teamName.equals(other.getTeamName()))) &&
            ((this.applicationName==null && other.getApplicationName()==null) || 
             (this.applicationName!=null &&
              this.applicationName.equals(other.getApplicationName()))) &&
            ((this.webPageName==null && other.getWebPageName()==null) || 
             (this.webPageName!=null &&
              this.webPageName.equals(other.getWebPageName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSymbol() != null) {
            _hashCode += getSymbol().hashCode();
        }
        if (getPeriod() != null) {
            _hashCode += getPeriod().hashCode();
        }
        _hashCode += getRecordnum();
        if (getStartdate() != null) {
            _hashCode += getStartdate().hashCode();
        }
        if (getEnddate() != null) {
            _hashCode += getEnddate().hashCode();
        }
        if (getRange() != null) {
            _hashCode += getRange().hashCode();
        }
        _hashCode += (isDecimalNumbers() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getUserAccount() != null) {
            _hashCode += getUserAccount().hashCode();
        }
        if (getTeamName() != null) {
            _hashCode += getTeamName().hashCode();
        }
        if (getApplicationName() != null) {
            _hashCode += getApplicationName().hashCode();
        }
        if (getWebPageName() != null) {
            _hashCode += getWebPageName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetHistoricalQuotesWithCredentials.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetHistoricalQuotesWithCredentials"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("symbol");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "symbol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("period");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "period"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordnum");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "recordnum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "startdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("enddate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "enddate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("range");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "range"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("decimalNumbers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "decimalNumbers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userAccount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "userAccount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("teamName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "teamName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("applicationName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "applicationName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("webPageName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "webPageName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
