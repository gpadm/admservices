/**
 * GetHistoricalQuotesWithCredentialsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.eSignalQuotes;

public class GetHistoricalQuotesWithCredentialsResponse  implements java.io.Serializable {
    private com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesWithCredentialsResponseGetHistoricalQuotesWithCredentialsResult getHistoricalQuotesWithCredentialsResult;

    public GetHistoricalQuotesWithCredentialsResponse() {
    }

    public GetHistoricalQuotesWithCredentialsResponse(
           com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesWithCredentialsResponseGetHistoricalQuotesWithCredentialsResult getHistoricalQuotesWithCredentialsResult) {
           this.getHistoricalQuotesWithCredentialsResult = getHistoricalQuotesWithCredentialsResult;
    }


    /**
     * Gets the getHistoricalQuotesWithCredentialsResult value for this GetHistoricalQuotesWithCredentialsResponse.
     * 
     * @return getHistoricalQuotesWithCredentialsResult
     */
    public com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesWithCredentialsResponseGetHistoricalQuotesWithCredentialsResult getGetHistoricalQuotesWithCredentialsResult() {
        return getHistoricalQuotesWithCredentialsResult;
    }


    /**
     * Sets the getHistoricalQuotesWithCredentialsResult value for this GetHistoricalQuotesWithCredentialsResponse.
     * 
     * @param getHistoricalQuotesWithCredentialsResult
     */
    public void setGetHistoricalQuotesWithCredentialsResult(com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesWithCredentialsResponseGetHistoricalQuotesWithCredentialsResult getHistoricalQuotesWithCredentialsResult) {
        this.getHistoricalQuotesWithCredentialsResult = getHistoricalQuotesWithCredentialsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetHistoricalQuotesWithCredentialsResponse)) return false;
        GetHistoricalQuotesWithCredentialsResponse other = (GetHistoricalQuotesWithCredentialsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getHistoricalQuotesWithCredentialsResult==null && other.getGetHistoricalQuotesWithCredentialsResult()==null) || 
             (this.getHistoricalQuotesWithCredentialsResult!=null &&
              this.getHistoricalQuotesWithCredentialsResult.equals(other.getGetHistoricalQuotesWithCredentialsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetHistoricalQuotesWithCredentialsResult() != null) {
            _hashCode += getGetHistoricalQuotesWithCredentialsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetHistoricalQuotesWithCredentialsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetHistoricalQuotesWithCredentialsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getHistoricalQuotesWithCredentialsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetHistoricalQuotesWithCredentialsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetHistoricalQuotesWithCredentialsResponse>GetHistoricalQuotesWithCredentialsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
