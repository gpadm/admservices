package com.adm.webpart.webservices.eSignalQuotes;

public class ESignalQuotesSoapProxy implements com.adm.webpart.webservices.eSignalQuotes.ESignalQuotesSoap {
  private String _endpoint = null;
  private com.adm.webpart.webservices.eSignalQuotes.ESignalQuotesSoap eSignalQuotesSoap = null;
  
  public ESignalQuotesSoapProxy() {
    _initESignalQuotesSoapProxy();
  }
  
  public ESignalQuotesSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initESignalQuotesSoapProxy();
  }
  
  private void _initESignalQuotesSoapProxy() {
    try {
      eSignalQuotesSoap = (new com.adm.webpart.webservices.eSignalQuotes.ESignalQuotesLocator()).geteSignalQuotesSoap();
      if (eSignalQuotesSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)eSignalQuotesSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)eSignalQuotesSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (eSignalQuotesSoap != null)
      ((javax.xml.rpc.Stub)eSignalQuotesSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.adm.webpart.webservices.eSignalQuotes.ESignalQuotesSoap getESignalQuotesSoap() {
    if (eSignalQuotesSoap == null)
      _initESignalQuotesSoapProxy();
    return eSignalQuotesSoap;
  }
  
  public com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesResponseGetDelayedQuotesResult getDelayedQuotes(java.lang.String symbols, java.lang.String cusip, java.lang.String fields, java.lang.String type, boolean dispfullname, java.lang.String datefmt, java.lang.String timefmt) throws java.rmi.RemoteException{
    if (eSignalQuotesSoap == null)
      _initESignalQuotesSoapProxy();
    return eSignalQuotesSoap.getDelayedQuotes(symbols, cusip, fields, type, dispfullname, datefmt, timefmt);
  }
  
  public com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesWithCredentialsResponseGetDelayedQuotesWithCredentialsResult getDelayedQuotesWithCredentials(java.lang.String symbols, java.lang.String cusip, java.lang.String fields, java.lang.String type, boolean dispfullname, java.lang.String datefmt, java.lang.String timefmt, java.lang.String userAccount, java.lang.String teamName, java.lang.String applicationName, java.lang.String webPageName) throws java.rmi.RemoteException{
    if (eSignalQuotesSoap == null)
      _initESignalQuotesSoapProxy();
    return eSignalQuotesSoap.getDelayedQuotesWithCredentials(symbols, cusip, fields, type, dispfullname, datefmt, timefmt, userAccount, teamName, applicationName, webPageName);
  }
  
  public com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2ResponseGetDelayedQuotesDB2Result getDelayedQuotesDB2(java.lang.String symbols, java.lang.String symbolType) throws java.rmi.RemoteException{
    if (eSignalQuotesSoap == null)
      _initESignalQuotesSoapProxy();
    return eSignalQuotesSoap.getDelayedQuotesDB2(symbols, symbolType);
  }
  
  public com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2WithCredentialsResponseGetDelayedQuotesDB2WithCredentialsResult getDelayedQuotesDB2WithCredentials(java.lang.String symbols, java.lang.String symbolType, java.lang.String userAccount, java.lang.String teamName, java.lang.String applicationName, java.lang.String webPageName) throws java.rmi.RemoteException{
    if (eSignalQuotesSoap == null)
      _initESignalQuotesSoapProxy();
    return eSignalQuotesSoap.getDelayedQuotesDB2WithCredentials(symbols, symbolType, userAccount, teamName, applicationName, webPageName);
  }
  
  public com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesResponseGetHistoricalQuotesResult getHistoricalQuotes(java.lang.String symbol, java.lang.String period, int recordnum, java.lang.String startdate, java.lang.String enddate, java.lang.String range, boolean decimalNumbers) throws java.rmi.RemoteException{
    if (eSignalQuotesSoap == null)
      _initESignalQuotesSoapProxy();
    return eSignalQuotesSoap.getHistoricalQuotes(symbol, period, recordnum, startdate, enddate, range, decimalNumbers);
  }
  
  public com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesWithCredentialsResponseGetHistoricalQuotesWithCredentialsResult getHistoricalQuotesWithCredentials(java.lang.String symbol, java.lang.String period, int recordnum, java.lang.String startdate, java.lang.String enddate, java.lang.String range, boolean decimalNumbers, java.lang.String userAccount, java.lang.String teamName, java.lang.String applicationName, java.lang.String webPageName) throws java.rmi.RemoteException{
    if (eSignalQuotesSoap == null)
      _initESignalQuotesSoapProxy();
    return eSignalQuotesSoap.getHistoricalQuotesWithCredentials(symbol, period, recordnum, startdate, enddate, range, decimalNumbers, userAccount, teamName, applicationName, webPageName);
  }
  
  public com.adm.webpart.webservices.eSignalQuotes.GetLiveQuotesResponseGetLiveQuotesResult getLiveQuotes(java.lang.String symbols, java.lang.String cusip, java.lang.String fields, java.lang.String type, boolean dispfullname, java.lang.String datefmt, java.lang.String timefmt, java.lang.String userName, java.lang.String teamName, java.lang.String applicationName) throws java.rmi.RemoteException{
    if (eSignalQuotesSoap == null)
      _initESignalQuotesSoapProxy();
    return eSignalQuotesSoap.getLiveQuotes(symbols, cusip, fields, type, dispfullname, datefmt, timefmt, userName, teamName, applicationName);
  }
  
  
}