/**
 * ESignalQuotes.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.eSignalQuotes;

public interface ESignalQuotes extends javax.xml.rpc.Service {
    public java.lang.String geteSignalQuotesSoapAddress();

    public com.adm.webpart.webservices.eSignalQuotes.ESignalQuotesSoap geteSignalQuotesSoap() throws javax.xml.rpc.ServiceException;

    public com.adm.webpart.webservices.eSignalQuotes.ESignalQuotesSoap geteSignalQuotesSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
