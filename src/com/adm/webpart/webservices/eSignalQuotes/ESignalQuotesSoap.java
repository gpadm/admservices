/**
 * ESignalQuotesSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.eSignalQuotes;

public interface ESignalQuotesSoap extends java.rmi.Remote {
    public com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesResponseGetDelayedQuotesResult getDelayedQuotes(java.lang.String symbols, java.lang.String cusip, java.lang.String fields, java.lang.String type, boolean dispfullname, java.lang.String datefmt, java.lang.String timefmt) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesWithCredentialsResponseGetDelayedQuotesWithCredentialsResult getDelayedQuotesWithCredentials(java.lang.String symbols, java.lang.String cusip, java.lang.String fields, java.lang.String type, boolean dispfullname, java.lang.String datefmt, java.lang.String timefmt, java.lang.String userAccount, java.lang.String teamName, java.lang.String applicationName, java.lang.String webPageName) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2ResponseGetDelayedQuotesDB2Result getDelayedQuotesDB2(java.lang.String symbols, java.lang.String symbolType) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2WithCredentialsResponseGetDelayedQuotesDB2WithCredentialsResult getDelayedQuotesDB2WithCredentials(java.lang.String symbols, java.lang.String symbolType, java.lang.String userAccount, java.lang.String teamName, java.lang.String applicationName, java.lang.String webPageName) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesResponseGetHistoricalQuotesResult getHistoricalQuotes(java.lang.String symbol, java.lang.String period, int recordnum, java.lang.String startdate, java.lang.String enddate, java.lang.String range, boolean decimalNumbers) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesWithCredentialsResponseGetHistoricalQuotesWithCredentialsResult getHistoricalQuotesWithCredentials(java.lang.String symbol, java.lang.String period, int recordnum, java.lang.String startdate, java.lang.String enddate, java.lang.String range, boolean decimalNumbers, java.lang.String userAccount, java.lang.String teamName, java.lang.String applicationName, java.lang.String webPageName) throws java.rmi.RemoteException;
    public com.adm.webpart.webservices.eSignalQuotes.GetLiveQuotesResponseGetLiveQuotesResult getLiveQuotes(java.lang.String symbols, java.lang.String cusip, java.lang.String fields, java.lang.String type, boolean dispfullname, java.lang.String datefmt, java.lang.String timefmt, java.lang.String userName, java.lang.String teamName, java.lang.String applicationName) throws java.rmi.RemoteException;
}
