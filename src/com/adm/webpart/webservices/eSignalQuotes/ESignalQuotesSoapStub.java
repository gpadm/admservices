/**
 * ESignalQuotesSoapStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.eSignalQuotes;

public class ESignalQuotesSoapStub extends org.apache.axis.client.Stub implements com.adm.webpart.webservices.eSignalQuotes.ESignalQuotesSoap {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[7];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetDelayedQuotes");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "symbols"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "cusip"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "fields"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "type"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "dispfullname"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "datefmt"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "timefmt"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetDelayedQuotesResponse>GetDelayedQuotesResult"));
        oper.setReturnClass(com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesResponseGetDelayedQuotesResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetDelayedQuotesResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetDelayedQuotesWithCredentials");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "symbols"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "cusip"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "fields"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "type"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "dispfullname"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "datefmt"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "timefmt"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "userAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "teamName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "applicationName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "webPageName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetDelayedQuotesWithCredentialsResponse>GetDelayedQuotesWithCredentialsResult"));
        oper.setReturnClass(com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesWithCredentialsResponseGetDelayedQuotesWithCredentialsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetDelayedQuotesWithCredentialsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetDelayedQuotesDB2");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "symbols"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "symbolType"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetDelayedQuotesDB2Response>GetDelayedQuotesDB2Result"));
        oper.setReturnClass(com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2ResponseGetDelayedQuotesDB2Result.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetDelayedQuotesDB2Result"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetDelayedQuotesDB2WithCredentials");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "symbols"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "symbolType"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "userAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "teamName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "applicationName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "webPageName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetDelayedQuotesDB2WithCredentialsResponse>GetDelayedQuotesDB2WithCredentialsResult"));
        oper.setReturnClass(com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2WithCredentialsResponseGetDelayedQuotesDB2WithCredentialsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetDelayedQuotesDB2WithCredentialsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetHistoricalQuotes");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "symbol"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "period"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "recordnum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "startdate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "enddate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "range"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "decimalNumbers"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetHistoricalQuotesResponse>GetHistoricalQuotesResult"));
        oper.setReturnClass(com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesResponseGetHistoricalQuotesResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetHistoricalQuotesResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetHistoricalQuotesWithCredentials");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "symbol"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "period"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "recordnum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "startdate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "enddate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "range"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "decimalNumbers"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "userAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "teamName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "applicationName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "webPageName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetHistoricalQuotesWithCredentialsResponse>GetHistoricalQuotesWithCredentialsResult"));
        oper.setReturnClass(com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesWithCredentialsResponseGetHistoricalQuotesWithCredentialsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetHistoricalQuotesWithCredentialsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLiveQuotes");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "symbols"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "cusip"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "fields"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "type"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "dispfullname"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "datefmt"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "timefmt"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "userName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "teamName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "applicationName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetLiveQuotesResponse>GetLiveQuotesResult"));
        oper.setReturnClass(com.adm.webpart.webservices.eSignalQuotes.GetLiveQuotesResponseGetLiveQuotesResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetLiveQuotesResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

    }

    public ESignalQuotesSoapStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public ESignalQuotesSoapStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public ESignalQuotesSoapStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetDelayedQuotesDB2Response>GetDelayedQuotesDB2Result");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2ResponseGetDelayedQuotesDB2Result.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetDelayedQuotesDB2WithCredentialsResponse>GetDelayedQuotesDB2WithCredentialsResult");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2WithCredentialsResponseGetDelayedQuotesDB2WithCredentialsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetDelayedQuotesResponse>GetDelayedQuotesResult");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesResponseGetDelayedQuotesResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetDelayedQuotesWithCredentialsResponse>GetDelayedQuotesWithCredentialsResult");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesWithCredentialsResponseGetDelayedQuotesWithCredentialsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetHistoricalQuotesResponse>GetHistoricalQuotesResult");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesResponseGetHistoricalQuotesResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetHistoricalQuotesWithCredentialsResponse>GetHistoricalQuotesWithCredentialsResult");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesWithCredentialsResponseGetHistoricalQuotesWithCredentialsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetLiveQuotesResponse>GetLiveQuotesResult");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetLiveQuotesResponseGetLiveQuotesResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetDelayedQuotesDB2");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetDelayedQuotesDB2Response");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2Response.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetDelayedQuotesDB2WithCredentials");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2WithCredentials.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetDelayedQuotesDB2WithCredentialsResponse");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2WithCredentialsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetDelayedQuotesWithCredentials");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesWithCredentials.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetDelayedQuotesWithCredentialsResponse");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesWithCredentialsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetHistoricalQuotes");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotes.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetHistoricalQuotesResponse");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetHistoricalQuotesWithCredentials");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesWithCredentials.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetHistoricalQuotesWithCredentialsResponse");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesWithCredentialsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetLiveQuotes");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetLiveQuotes.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetLiveQuotesResponse");
            cachedSerQNames.add(qName);
            cls = com.adm.webpart.webservices.eSignalQuotes.GetLiveQuotesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesResponseGetDelayedQuotesResult getDelayedQuotes(java.lang.String symbols, java.lang.String cusip, java.lang.String fields, java.lang.String type, boolean dispfullname, java.lang.String datefmt, java.lang.String timefmt) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/eSignalQuotes/GetDelayedQuotes");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetDelayedQuotes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {symbols, cusip, fields, type, new java.lang.Boolean(dispfullname), datefmt, timefmt});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesResponseGetDelayedQuotesResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesResponseGetDelayedQuotesResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesResponseGetDelayedQuotesResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesWithCredentialsResponseGetDelayedQuotesWithCredentialsResult getDelayedQuotesWithCredentials(java.lang.String symbols, java.lang.String cusip, java.lang.String fields, java.lang.String type, boolean dispfullname, java.lang.String datefmt, java.lang.String timefmt, java.lang.String userAccount, java.lang.String teamName, java.lang.String applicationName, java.lang.String webPageName) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/eSignalQuotes/GetDelayedQuotesWithCredentials");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetDelayedQuotesWithCredentials"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {symbols, cusip, fields, type, new java.lang.Boolean(dispfullname), datefmt, timefmt, userAccount, teamName, applicationName, webPageName});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesWithCredentialsResponseGetDelayedQuotesWithCredentialsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesWithCredentialsResponseGetDelayedQuotesWithCredentialsResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesWithCredentialsResponseGetDelayedQuotesWithCredentialsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2ResponseGetDelayedQuotesDB2Result getDelayedQuotesDB2(java.lang.String symbols, java.lang.String symbolType) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/eSignalQuotes/GetDelayedQuotesDB2");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetDelayedQuotesDB2"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {symbols, symbolType});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2ResponseGetDelayedQuotesDB2Result) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2ResponseGetDelayedQuotesDB2Result) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2ResponseGetDelayedQuotesDB2Result.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2WithCredentialsResponseGetDelayedQuotesDB2WithCredentialsResult getDelayedQuotesDB2WithCredentials(java.lang.String symbols, java.lang.String symbolType, java.lang.String userAccount, java.lang.String teamName, java.lang.String applicationName, java.lang.String webPageName) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/eSignalQuotes/GetDelayedQuotesDB2WithCredentials");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetDelayedQuotesDB2WithCredentials"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {symbols, symbolType, userAccount, teamName, applicationName, webPageName});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2WithCredentialsResponseGetDelayedQuotesDB2WithCredentialsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2WithCredentialsResponseGetDelayedQuotesDB2WithCredentialsResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2WithCredentialsResponseGetDelayedQuotesDB2WithCredentialsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesResponseGetHistoricalQuotesResult getHistoricalQuotes(java.lang.String symbol, java.lang.String period, int recordnum, java.lang.String startdate, java.lang.String enddate, java.lang.String range, boolean decimalNumbers) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/eSignalQuotes/GetHistoricalQuotes");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetHistoricalQuotes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {symbol, period, new java.lang.Integer(recordnum), startdate, enddate, range, new java.lang.Boolean(decimalNumbers)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesResponseGetHistoricalQuotesResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesResponseGetHistoricalQuotesResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesResponseGetHistoricalQuotesResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesWithCredentialsResponseGetHistoricalQuotesWithCredentialsResult getHistoricalQuotesWithCredentials(java.lang.String symbol, java.lang.String period, int recordnum, java.lang.String startdate, java.lang.String enddate, java.lang.String range, boolean decimalNumbers, java.lang.String userAccount, java.lang.String teamName, java.lang.String applicationName, java.lang.String webPageName) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/eSignalQuotes/GetHistoricalQuotesWithCredentials");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetHistoricalQuotesWithCredentials"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {symbol, period, new java.lang.Integer(recordnum), startdate, enddate, range, new java.lang.Boolean(decimalNumbers), userAccount, teamName, applicationName, webPageName});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesWithCredentialsResponseGetHistoricalQuotesWithCredentialsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesWithCredentialsResponseGetHistoricalQuotesWithCredentialsResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.eSignalQuotes.GetHistoricalQuotesWithCredentialsResponseGetHistoricalQuotesWithCredentialsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.adm.webpart.webservices.eSignalQuotes.GetLiveQuotesResponseGetLiveQuotesResult getLiveQuotes(java.lang.String symbols, java.lang.String cusip, java.lang.String fields, java.lang.String type, boolean dispfullname, java.lang.String datefmt, java.lang.String timefmt, java.lang.String userName, java.lang.String teamName, java.lang.String applicationName) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.adm.com/eSignalQuotes/GetLiveQuotes");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetLiveQuotes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {symbols, cusip, fields, type, new java.lang.Boolean(dispfullname), datefmt, timefmt, userName, teamName, applicationName});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.adm.webpart.webservices.eSignalQuotes.GetLiveQuotesResponseGetLiveQuotesResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.adm.webpart.webservices.eSignalQuotes.GetLiveQuotesResponseGetLiveQuotesResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.adm.webpart.webservices.eSignalQuotes.GetLiveQuotesResponseGetLiveQuotesResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
