/**
 * GetDelayedQuotesWithCredentialsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.eSignalQuotes;

public class GetDelayedQuotesWithCredentialsResponse  implements java.io.Serializable {
    private com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesWithCredentialsResponseGetDelayedQuotesWithCredentialsResult getDelayedQuotesWithCredentialsResult;

    public GetDelayedQuotesWithCredentialsResponse() {
    }

    public GetDelayedQuotesWithCredentialsResponse(
           com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesWithCredentialsResponseGetDelayedQuotesWithCredentialsResult getDelayedQuotesWithCredentialsResult) {
           this.getDelayedQuotesWithCredentialsResult = getDelayedQuotesWithCredentialsResult;
    }


    /**
     * Gets the getDelayedQuotesWithCredentialsResult value for this GetDelayedQuotesWithCredentialsResponse.
     * 
     * @return getDelayedQuotesWithCredentialsResult
     */
    public com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesWithCredentialsResponseGetDelayedQuotesWithCredentialsResult getGetDelayedQuotesWithCredentialsResult() {
        return getDelayedQuotesWithCredentialsResult;
    }


    /**
     * Sets the getDelayedQuotesWithCredentialsResult value for this GetDelayedQuotesWithCredentialsResponse.
     * 
     * @param getDelayedQuotesWithCredentialsResult
     */
    public void setGetDelayedQuotesWithCredentialsResult(com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesWithCredentialsResponseGetDelayedQuotesWithCredentialsResult getDelayedQuotesWithCredentialsResult) {
        this.getDelayedQuotesWithCredentialsResult = getDelayedQuotesWithCredentialsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetDelayedQuotesWithCredentialsResponse)) return false;
        GetDelayedQuotesWithCredentialsResponse other = (GetDelayedQuotesWithCredentialsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getDelayedQuotesWithCredentialsResult==null && other.getGetDelayedQuotesWithCredentialsResult()==null) || 
             (this.getDelayedQuotesWithCredentialsResult!=null &&
              this.getDelayedQuotesWithCredentialsResult.equals(other.getGetDelayedQuotesWithCredentialsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetDelayedQuotesWithCredentialsResult() != null) {
            _hashCode += getGetDelayedQuotesWithCredentialsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetDelayedQuotesWithCredentialsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetDelayedQuotesWithCredentialsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getDelayedQuotesWithCredentialsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetDelayedQuotesWithCredentialsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetDelayedQuotesWithCredentialsResponse>GetDelayedQuotesWithCredentialsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
