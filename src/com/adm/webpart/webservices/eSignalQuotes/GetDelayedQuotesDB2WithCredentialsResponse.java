/**
 * GetDelayedQuotesDB2WithCredentialsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.eSignalQuotes;

public class GetDelayedQuotesDB2WithCredentialsResponse  implements java.io.Serializable {
    private com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2WithCredentialsResponseGetDelayedQuotesDB2WithCredentialsResult getDelayedQuotesDB2WithCredentialsResult;

    public GetDelayedQuotesDB2WithCredentialsResponse() {
    }

    public GetDelayedQuotesDB2WithCredentialsResponse(
           com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2WithCredentialsResponseGetDelayedQuotesDB2WithCredentialsResult getDelayedQuotesDB2WithCredentialsResult) {
           this.getDelayedQuotesDB2WithCredentialsResult = getDelayedQuotesDB2WithCredentialsResult;
    }


    /**
     * Gets the getDelayedQuotesDB2WithCredentialsResult value for this GetDelayedQuotesDB2WithCredentialsResponse.
     * 
     * @return getDelayedQuotesDB2WithCredentialsResult
     */
    public com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2WithCredentialsResponseGetDelayedQuotesDB2WithCredentialsResult getGetDelayedQuotesDB2WithCredentialsResult() {
        return getDelayedQuotesDB2WithCredentialsResult;
    }


    /**
     * Sets the getDelayedQuotesDB2WithCredentialsResult value for this GetDelayedQuotesDB2WithCredentialsResponse.
     * 
     * @param getDelayedQuotesDB2WithCredentialsResult
     */
    public void setGetDelayedQuotesDB2WithCredentialsResult(com.adm.webpart.webservices.eSignalQuotes.GetDelayedQuotesDB2WithCredentialsResponseGetDelayedQuotesDB2WithCredentialsResult getDelayedQuotesDB2WithCredentialsResult) {
        this.getDelayedQuotesDB2WithCredentialsResult = getDelayedQuotesDB2WithCredentialsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetDelayedQuotesDB2WithCredentialsResponse)) return false;
        GetDelayedQuotesDB2WithCredentialsResponse other = (GetDelayedQuotesDB2WithCredentialsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getDelayedQuotesDB2WithCredentialsResult==null && other.getGetDelayedQuotesDB2WithCredentialsResult()==null) || 
             (this.getDelayedQuotesDB2WithCredentialsResult!=null &&
              this.getDelayedQuotesDB2WithCredentialsResult.equals(other.getGetDelayedQuotesDB2WithCredentialsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetDelayedQuotesDB2WithCredentialsResult() != null) {
            _hashCode += getGetDelayedQuotesDB2WithCredentialsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetDelayedQuotesDB2WithCredentialsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">GetDelayedQuotesDB2WithCredentialsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getDelayedQuotesDB2WithCredentialsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "GetDelayedQuotesDB2WithCredentialsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", ">>GetDelayedQuotesDB2WithCredentialsResponse>GetDelayedQuotesDB2WithCredentialsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
