/**
 * ESignalQuotesLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.adm.webpart.webservices.eSignalQuotes;

public class ESignalQuotesLocator extends org.apache.axis.client.Service implements com.adm.webpart.webservices.eSignalQuotes.ESignalQuotes {

    public ESignalQuotesLocator() {
    }


    public ESignalQuotesLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ESignalQuotesLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for eSignalQuotesSoap
    private java.lang.String eSignalQuotesSoap_address = "http://webservices.adm.com/eSignalQuotes/eSignalQuotes.asmx";

    public java.lang.String geteSignalQuotesSoapAddress() {
        return eSignalQuotesSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String eSignalQuotesSoapWSDDServiceName = "eSignalQuotesSoap";

    public java.lang.String geteSignalQuotesSoapWSDDServiceName() {
        return eSignalQuotesSoapWSDDServiceName;
    }

    public void seteSignalQuotesSoapWSDDServiceName(java.lang.String name) {
        eSignalQuotesSoapWSDDServiceName = name;
    }

    public com.adm.webpart.webservices.eSignalQuotes.ESignalQuotesSoap geteSignalQuotesSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(eSignalQuotesSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return geteSignalQuotesSoap(endpoint);
    }

    public com.adm.webpart.webservices.eSignalQuotes.ESignalQuotesSoap geteSignalQuotesSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.adm.webpart.webservices.eSignalQuotes.ESignalQuotesSoapStub _stub = new com.adm.webpart.webservices.eSignalQuotes.ESignalQuotesSoapStub(portAddress, this);
            _stub.setPortName(geteSignalQuotesSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void seteSignalQuotesSoapEndpointAddress(java.lang.String address) {
        eSignalQuotesSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.adm.webpart.webservices.eSignalQuotes.ESignalQuotesSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.adm.webpart.webservices.eSignalQuotes.ESignalQuotesSoapStub _stub = new com.adm.webpart.webservices.eSignalQuotes.ESignalQuotesSoapStub(new java.net.URL(eSignalQuotesSoap_address), this);
                _stub.setPortName(geteSignalQuotesSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("eSignalQuotesSoap".equals(inputPortName)) {
            return geteSignalQuotesSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "eSignalQuotes");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webservices.adm.com/eSignalQuotes/", "eSignalQuotesSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("eSignalQuotesSoap".equals(portName)) {
            seteSignalQuotesSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
