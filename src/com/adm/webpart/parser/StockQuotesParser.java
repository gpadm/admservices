package com.adm.webpart.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.adm.webpart.beans.stock.IRXML;
import com.adm.webpart.beans.stock.StockQuote;
import com.adm.webpart.beans.stock.StockQuotes;

public class StockQuotesParser {
    
	private static String SERVICE_URL = "http://xml.corporate-ir.net/irxmlclient.asp?compid=82972&reqtype=quotes";
	
    private static Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("proxy.admworld.com", 80));
	
    public static IRXML getStockQuote() {
    
        IRXML parsedQuotes = null;
        
        BufferedReader reader = null;
        try {
            JAXBContext jc = JAXBContext.newInstance("com.adm.webpart.beans.stock");
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            
        	// create the HttpURLConnection
            URL url = new URL(SERVICE_URL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection(proxy);
             
            // just want to do an HTTP GET here
            connection.setRequestMethod("GET");

            // give it 15 seconds to respond
            connection.setReadTimeout(15*1000);
            connection.connect();
       
            // read the output from the server
            reader  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            
            IRXML stockElement = (IRXML)unmarshaller.unmarshal(reader);

            parsedQuotes = stockElement;
        } catch (JAXBException jaxbe) {
            jaxbe.printStackTrace();
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
        finally
        {
          // close the reader; this can throw an exception too, so
          // wrap it in another try/catch block.
          if (reader != null)
          {
            try
            {
              reader.close();
            }
            catch (IOException ioe)
            {
              ioe.printStackTrace();
            }
          }
        }
        return parsedQuotes;
    }
    
    public static void main(String[] args) throws Exception {

        IRXML xmlObj = StockQuotesParser.getStockQuote();
        StockQuotes qts = xmlObj.getStockQuotes();
        List<StockQuote> qtL = qts.getStockQuote();
        
        System.out.printf("IRXML.getCorpMasterID -> %s, ", xmlObj.getCorpMasterID());
        System.out.printf("StockQuotes.getPubDate -> %s, ", qts.getPubDate());
        System.out.printf("StockQuotes.getPubTime -> %s\n", qts.getPubTime());
        
        for(StockQuote qt : qtL) {
            System.out.printf("PrimaryTicker for %s is -> %s\n",qt.getTicker(), qt.getPrimaryTicker());
            if(qt.getPrimaryTicker() !=null && qt.getPrimaryTicker().equalsIgnoreCase("YES")) {
                System.out.printf("StockQuote.getHigh -> %s", qt.getHigh());
                System.out.printf(", StockQuote.getLow -> %s", qt.getLow());
                System.out.printf(", StockQuote.getChange -> %s\n", qt.getChange());
                break;
            }
        }       
    }    
}