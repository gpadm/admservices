package com.adm.webpart.factory;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DBConnectionSingleton {

	private static DBConnectionSingleton singleInstance;
	private static DataSource dataSource;
	private static Connection dbConnect;

	public static Connection getInstance(String environment) {
		try {

			Context initialContext = new InitialContext();
			dataSource = (DataSource)initialContext.lookup("jdbc/ADMDBDatasource");			
			
			try {
				
				dbConnect = dataSource.getConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (NamingException e) {
			e.printStackTrace();
		}

		return dbConnect;
	}
	
	public static Connection getOracInstance() {
		try {

			Context initialContext = new InitialContext();
			dataSource = (DataSource)initialContext.lookup("csDataSource");		
			try {				
				dbConnect = dataSource.getConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (NamingException e) {
			e.printStackTrace();
		}

		return dbConnect;
	}
}
