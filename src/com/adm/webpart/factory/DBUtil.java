package com.adm.webpart.factory;

import java.sql.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DBUtil {

	private static Connection connection;

	String result = "";
	InputStream inputStream = null;

	private String getPropValue(String propertyKey) {

		try {
			Properties prop = new Properties();
			String propFileName = "/resources/environment.properties";
			prop.load(getClass().getResourceAsStream("/resources/environment.properties"));

			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

			if (inputStream == null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
			result = prop.getProperty("user");

		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	public Statement getStatement(String env_dev) throws SQLException {

		connection = DBConnectionSingleton.getInstance("Dev");
		return connection.createStatement();
	}

	public PreparedStatement getOraPreparedStatement(String query) throws SQLException {

		connection = DBConnectionSingleton.getOracInstance();
		return connection.prepareStatement(query);
	}
	
	public Statement getOrcStatement() throws SQLException {

		connection = DBConnectionSingleton.getOracInstance();
		return connection.createStatement();
	}
	
	public static void connectionClose() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {

			}
		}
	}

	public static void close(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				/* log or print or ignore */
			}
		}
	}

	public static void close(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				/* log or print or ignore */
			}
		}
	}

	public static void close(PreparedStatement preparedStatement) {
		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				/* log or print or ignore */
			}
		}
	}
}