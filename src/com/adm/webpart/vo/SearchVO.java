package com.adm.webpart.vo;

public class SearchVO {
	
	
	private String productID;
	private String productName;
	private String productText;
	
	
	/**
	 * @return the productID
	 */
	public String getProductID() {
		return productID;
	}
	/**
	 * @param productID the productID to set
	 */
	public void setProductID(String productID) {
		this.productID = productID;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the productText
	 */
	public String getProductText() {
		return productText;
	}
	/**
	 * @param productText the productText to set
	 */
	public void setProductText(String productText) {
		this.productText = productText;
	}
	
	
	
	

}
