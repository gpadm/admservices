/**
 * 
 */
package com.adm.webpart.vo;

/**
 * @author s.satya.yellamelli
 *
 */
public class TechnicalDocumentationVO {
	
	private String documentID;
	private String documentSource;
	private String documentFileName;
	private String documentTitle;
	private byte[] PdfBlob;
	
	
	/**
	 * @return the documentID
	 */
	public String getDocumentID() {
		return documentID;
	}
	/**
	 * @param documentID the documentID to set
	 */
	public void setDocumentID(String documentID) {
		this.documentID = documentID;
	}
	/**
	 * @return the documentSource
	 */
	public String getDocumentSource() {
		return documentSource;
	}
	/**
	 * @param documentSource the documentSource to set
	 */
	public void setDocumentSource(String documentSource) {
		this.documentSource = documentSource;
	}
	/**
	 * @return the documentFileName
	 */
	public String getDocumentFileName() {
		return documentFileName;
	}
	/**
	 * @param documentFileName the documentFileName to set
	 */
	public void setDocumentFileName(String documentFileName) {
		this.documentFileName = documentFileName;
	}
	/**
	 * @return the documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}
	/**
	 * @param documentTitle the documentTitle to set
	 */
	public void setDocumentTitle(String documentTitle) {
		this.documentTitle = documentTitle;
	}
	/**
	 * @return the pdfBlob
	 */
	public byte[] getPdfBlob() {
		return PdfBlob;
	}
	/**
	 * @param pdfBlob the pdfBlob to set
	 */
	public void setPdfBlob(byte[] pdfBlob) {
		PdfBlob = pdfBlob;
	}
	
	

}
