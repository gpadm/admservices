package com.adm.webpart.beans;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.adm.webpart.webservices.BW_Weather.ForecastDayExtended;

public class FiveDayForecast extends ForecastDayExtended {
	private static final long serialVersionUID = 442836793274923398L;
	private static Map<String, String> MONTHS = new HashMap<String, String>();
	
	static{
		MONTHS.put("1", "Jan");
		MONTHS.put("2", "Feb");
		MONTHS.put("3", "Mar");
		MONTHS.put("4", "Apr");
		MONTHS.put("5", "May");
		MONTHS.put("6", "Jun");
		MONTHS.put("7", "Jul");
		MONTHS.put("8", "Aug");
		MONTHS.put("9", "Sep");
		MONTHS.put("10", "Oct");
		MONTHS.put("11", "Nov");
		MONTHS.put("12", "Dec");
	}
	
	protected String formatedDate;
	
	protected ForecastDayExtended extendedForecast;	

	public FiveDayForecast(ForecastDayExtended extendedForecast) {
		super();
		this.extendedForecast = extendedForecast;
		setFormatedDate(extendedForecast.getDate());
	}

	public String getFormatedDate() {
		return formatedDate;
	}

	public void setFormatedDate(String date) {
		String dateParts [] = date.split("/");
		StringBuilder sb = new StringBuilder(MONTHS.get(dateParts[0]));
		sb.append(" ").append(dateParts[1]);
		formatedDate =  sb.toString();
	}

	@Override
	public int getOrderNumber() {
		return extendedForecast.getOrderNumber();
	}

	@Override
	public String getDisplayName() {
		return extendedForecast.getDisplayName();
	}

	@Override
	public String getDay() {
		return extendedForecast.getDay();
	}

	@Override
	public String getHighTemp() {
		return extendedForecast.getHighTemp();
	}

	@Override
	public String getLowTemp() {
		return extendedForecast.getLowTemp();
	}

	@Override
	public String getIconURL() {
		return extendedForecast.getIconURL();
	}

	@Override
	public String getDate() {
		return extendedForecast.getDate();
	}

	@Override
	public String getCurrentTemp() {
		return extendedForecast.getCurrentTemp();
	}

	@Override
	public String getFeelTemp() {
		return extendedForecast.getFeelTemp();
	}

	@Override
	public String getPrecipChance() {
		return extendedForecast.getPrecipChance();
	}

	@Override
	public String getWindSpeed() {
		return extendedForecast.getWindSpeed();
	}

	@Override
	public String getHumidity() {
		return extendedForecast.getHumidity();
	}

	@Override
	public String getPanEvaporation() {
		return extendedForecast.getPanEvaporation();
	}

	@Override
	public String getPrecipAmount() {
		return extendedForecast.getPrecipAmount();
	}

	@Override
	public String getGrowingDegreeDays50() {
		return extendedForecast.getGrowingDegreeDays50();
	}

	@Override
	public String getGrowingDegreeDays60() {
		return extendedForecast.getGrowingDegreeDays60();
	}

	@Override
	public String getSunrise() {
		return extendedForecast.getSunrise();
	}

	@Override
	public String getSunset() {
		return extendedForecast.getSunset();
	}

	@Override
	public String getCountry() {
		return extendedForecast.getCountry();
	}

	@Override
	public String getRegion() {
		return extendedForecast.getRegion();
	}

	@Override
	public String getDewPoint() {
		return extendedForecast.getDewPoint();
	}

	@Override
	public String getEvaporation() {
		return extendedForecast.getEvaporation();
	}

	@Override
	public String getPressure() {
		return extendedForecast.getPressure();
	}

	@Override
	public String getWindDirection() {
		return extendedForecast.getWindDirection();
	}

	@Override
	public String getWeatherType() {
		return extendedForecast.getWeatherType();
	}

	@Override
	public String getFeelsLikeHigh() {
		return extendedForecast.getFeelsLikeHigh();
	}

	@Override
	public String getFeelsLikeLow() {
		return extendedForecast.getFeelsLikeLow();
	}

	@Override
	public String getPostalCode() {
		return extendedForecast.getPostalCode();
	}

	@Override
	public String toString() {
		return "FiveDayForecast [formatedDate=" + formatedDate
				+ ", extendedForecast=" + extendedForecast + "]";
	}
}
