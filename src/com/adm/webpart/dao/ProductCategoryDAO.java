package com.adm.webpart.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import com.adm.webpart.factory.DBUtil;
import com.adm.webpart.vo.ProductCategoryVO;
import com.adm.webpart.vo.ProductNameVO;

public class ProductCategoryDAO {

	private Connection connection;
	private Statement statement;
	private ResultSet result;
	private static String env_dev = "development";
	@SuppressWarnings("unused")
	private static String env_prod = "production";

	public ProductCategoryDAO() {
	}

	public List<ProductCategoryVO> productCategoryList(String leadingCharacter,
			int regionId, int langId) {
		return productCategoryList(leadingCharacter);
	}

	// need to return product category object

	public List<ProductCategoryVO> productCategoryList(String leadingCharacter) {
		
		ArrayList<ProductCategoryVO> productList = new ArrayList<ProductCategoryVO>();
		ProductCategoryVO productvo;		
	
		
		String querySelect = Messages.getString("ProductCategoryDAO.query2");
		String queryFrom = Messages.getString("ProductCategoryDAO.query3")
				+ Messages.getString("ProductCategoryDAO.query4");
		StringBuffer queryWhere = new StringBuffer();
		queryWhere.append(Messages.getString("ProductCategoryDAO.query5"));
		String queryOrderBy = Messages.getString("ProductCategoryDAO.query6");

		if (leadingCharacter.equalsIgnoreCase("#")) {
			queryWhere.append(Messages.getString("ProductCategoryDAO.query8"));
			for (int i = 0; i < 10; ++i) {
				queryWhere.append(Messages
						.getString("ProductCategoryDAO.query9")
						+ i
						+ Messages.getString("ProductCategoryDAO.query10"));
				if (i < 9)
					queryWhere.append(Messages
							.getString("ProductCategoryDAO.query11"));
			}
			queryWhere.append(Messages.getString("ProductCategoryDAO.query12"));

		} else {
			queryWhere.append(Messages.getString("ProductCategoryDAO.query13")
					+ leadingCharacter
					+ Messages.getString("ProductCategoryDAO.query14"));
		}
		try {
			statement = new DBUtil().getStatement(env_dev);
			String queryStatement = querySelect + queryFrom + queryWhere
					+ queryOrderBy;
			
			result = statement.executeQuery(queryStatement);
			while (result.next()) {

				productvo = new ProductCategoryVO();
				productvo.setGroupID(result.getString(Messages
						.getString("ProductCategoryDAO.column1")));
				productvo.setGroupName(result.getString(Messages
						.getString("ProductCategoryDAO.column2")));
				productList.add(productvo);
			}
		}
		catch (SQLException sqlexe) {

		} finally {
			DBUtil.close(result);
			DBUtil.close(statement);
			DBUtil.connectionClose();
		}
		return productList;
	}

}
