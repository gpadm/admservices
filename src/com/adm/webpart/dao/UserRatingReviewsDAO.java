package com.adm.webpart.dao;

import com.adm.webpart.factory.DBUtil;
import com.adm.webpart.vo.UserReviewsVO;
import java.sql.*;
import java.util.*;


public class UserRatingReviewsDAO {
	
	private PreparedStatement preparedStatement;
	private Statement statement;
	private ResultSet result;
	
	public UserRatingReviewsDAO() {
	}

	public void updateUserReviewAndRating(UserReviewsVO userReviewVO) {

		String InsertReviewComments = "INSERT INTO USERREVIEW" + "(RECIPEID, RATING, REVIEWCOMMENTS, FIRSTNAME) VALUES"
				+ "(?,?,?,?)";

		try {

			preparedStatement = new DBUtil().getOraPreparedStatement(InsertReviewComments);
			preparedStatement.setString(1, userReviewVO.getRecipeId());
			preparedStatement.setString(2, userReviewVO.getRating());
			preparedStatement.setString(3, userReviewVO.getReview());
			preparedStatement.setString(4, userReviewVO.getfName());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.close(preparedStatement);
			DBUtil.connectionClose();
		}
	}

	public List<UserReviewsVO> getUserReviewAndRating(String recipeId) {
		
		String query = "SELECT REVIEWCOMMENTS,FIRSTNAME FROM RECIPEUSERREVIEW WHERE RECIPEID="+recipeId;
		List <UserReviewsVO>userList = new ArrayList<UserReviewsVO>();
		
		try {
			statement = new DBUtil().getOrcStatement();
			result = statement.executeQuery(query);
			UserReviewsVO userReviewsVO;
			
			while (result.next()) {
				userReviewsVO = new UserReviewsVO();
				userReviewsVO.setReview(result.getString("REVIEWCOMMENTS")); 
				userReviewsVO.setfName(result.getString("FIRSTNAME"));
				userList.add(userReviewsVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}	finally {
			DBUtil.close(statement);
			DBUtil.connectionClose();
		}		
		
		return userList;

	}
}
