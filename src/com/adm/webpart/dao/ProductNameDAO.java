package com.adm.webpart.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import com.adm.webpart.factory.DBUtil;
import com.adm.webpart.vo.ProductNameVO;

public class ProductNameDAO {

	private Connection connection;
	private Statement statement;
	private ResultSet result;
	private static String env_dev = "development"; //$NON-NLS-1$
	@SuppressWarnings("unused")
	private static String env_prod = "production"; //$NON-NLS-1$

	public List<ProductNameVO> productNameList(String leadingCharacter, int regionId, int langId) {

		return productNameList(leadingCharacter);
	}

	public List<ProductNameVO> productNameList(String leadingCharacter) {

		ArrayList<ProductNameVO> productList = new ArrayList<ProductNameVO>();
		ProductNameVO productvo;
		
		String querySelect = Messages.getString("ProductNameDAO.Query2"); 
		String queryFrom = Messages.getString("ProductNameDAO.Query3"); 

		StringBuffer queryWhere = new StringBuffer();
		queryWhere.append(Messages.getString("ProductNameDAO.Query4")); 
		String queryOrderBy = Messages.getString("ProductNameDAO.Query5");

		if (leadingCharacter.equalsIgnoreCase("#")) {
			queryWhere.append(Messages.getString("ProductNameDAO.Query7")); 
			for (int i = 0; i < 10; ++i) {
				queryWhere.append(Messages.getString("ProductNameDAO.Query8") + i + Messages.getString("ProductNameDAO.Query9")); 
				if (i < 9)
					queryWhere.append(Messages.getString("ProductNameDAO.Query10")); 
			}
			queryWhere.append(Messages.getString("ProductNameDAO.Query11")); 
		} else {
			queryWhere.append(Messages.getString("ProductNameDAO.Query12") 
					+ leadingCharacter + Messages.getString("ProductNameDAO.Query13")); 
		}

		
		

		try {
			String queryStatement = querySelect + queryFrom + queryWhere
					+ queryOrderBy;		
			
			
			statement = new DBUtil().getStatement(env_dev);			
			result = statement.executeQuery(queryStatement);			
			while (result.next()) {

				productvo = new ProductNameVO();
				productvo.setProductID(result.getString(Messages.getString("ProductNameDAO.Column1"))); 
				productvo.setProductName(result.getString(Messages.getString("ProductNameDAO.Column2")));
				productList.add(productvo);
			}

		}
		// need to catch specific exception list
		catch (SQLException sqlexe) {

		} finally {
			DBUtil.close(result);
			DBUtil.close(statement);
			DBUtil.connectionClose();
		}
		return productList;
	
	}

}
