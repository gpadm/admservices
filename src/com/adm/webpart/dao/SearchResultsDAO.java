package com.adm.webpart.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.adm.webpart.factory.DBUtil;
import com.adm.webpart.vo.SearchVO;

public class SearchResultsDAO {

	private Connection connection;
	private Statement statement;
	private ResultSet result;
	//private static String env_dev = Messages.getString("SearchResultsDAO.Query0"); 
	@SuppressWarnings("unused")
	private static String env_prod = "production"; 

	public SearchResultsDAO() {
	}

	public List<SearchVO> getSearchResults(int industryId, int segmentId,
			int applicationId, int productTypeId, int subgroupId,
			String keywords, int regionId, int langId) {
		return getSearchResults(subgroupId, keywords);
	}

	public List<SearchVO> getSearchResults(int subGroupID, String keywords) {

		// always subGroupID = -1
		
		ArrayList<SearchVO> searchList = new ArrayList<SearchVO>();
		SearchVO searchvo;
		String strQuery;
		if(null == keywords){
			strQuery = PDCreateQuery(subGroupID, "", Messages.getString("SearchResultsDAO.Query2")); 
		}else{
		strQuery = PDCreateQuery(subGroupID, keywords.trim(), Messages.getString("SearchResultsDAO.Query2"));
		}
	
		try {
			
			statement = new DBUtil().getStatement("Dev");
			result = statement.executeQuery(strQuery);
			
		
			while (result.next()) {
				searchvo = new SearchVO();
				searchvo.setProductID(result.getString(Messages.getString("SearchResultsDAO.Column1"))); 
				searchvo.setProductName(result.getString(Messages.getString("SearchResultsDAO.Column2"))); 
				searchvo.setProductText(result.getString(Messages.getString("SearchResultsDAO.Column3")));
				searchList.add(searchvo);
			}
		}		
		catch (SQLException sqlexe) {

		} finally {
			DBUtil.close(result);
			DBUtil.close(statement);
			DBUtil.connectionClose();
		}
		
		return searchList;

	}

	private String PDCreateQuery(int subgroupId, String keywords,
			String queryOption) {

		StringBuilder sqlSelect = new StringBuilder("SELECT DISTINCT PRDT_ID AS PING_CODE, PRDT_NAME AS PING_DESCRIPTION, SUBSTR(PRDT_DESCRIPTION, 1, 100) AS PING_TEXT"); 
		StringBuilder sqlFrom = new StringBuilder(" "+"FROM"); 
		StringBuilder sqlWhere = new StringBuilder(" WHERE "); 
		String sqlOrderBy = " ORDER BY PING_DESCRIPTION ASC";
		

		if (keywords == null){
			keywords = "";
		}
		else {
			keywords = keywords.trim();
			keywords = keywords.replace("%20", "%");
			keywords = keywords.replace("+", "%");
			keywords = keywords.replace(" ", "%");
			keywords = keywords.replace("'", "''");
		}
		// General FROM statement
		sqlFrom.append(" MISDB2A.OPPDPRDT_PRODUCT_DETAIL"); 

		// General WHERE conditions
		sqlWhere.append("PRDT_EXTERNAL = 'Y'"); 
		sqlWhere.append(" AND PRDT_SAMPLED = 'N' "); 

		// Keywords additions
		if (keywords.length() > 0) {
			sqlWhere.append("AND (UPPER(PRDT_KEYWORDS) LIKE '%" 
					+ keywords.toUpperCase() + "%'"); 
			sqlWhere.append(" OR UPPER(PRDT_NAME) LIKE '%"
					+ keywords.toUpperCase() + "%'"); 
			sqlWhere.append(" OR UPPER(PRDT_DESCRIPTION) LIKE '%"
					+ keywords.toUpperCase() + "%')" ); 

		}

		// Group additions
		if (subgroupId > 0) {
			sqlFrom.append(" JOIN MISDB2A.OPPDPDGR_PRODUCT_DETAIL_GROUPS ON PRDT_ID = PDGR_PRDT_ID "); 
			sqlWhere.append("AND PDGR_GROP_ID =" + subgroupId + " ");  
		}

		return sqlSelect.toString() + sqlFrom.toString() + sqlWhere.toString()
				+ sqlOrderBy;
	}

}
