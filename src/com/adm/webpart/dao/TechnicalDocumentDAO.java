
package com.adm.webpart.dao;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.adm.webpart.factory.DBUtil;
import com.adm.webpart.vo.TechnicalDocumentationVO;

/**
 * @author s.satya.yellamelli
 * 
 */
public class TechnicalDocumentDAO {

	private Connection connection;
	private Statement statement;
	private ResultSet result;
	private static String env_dev = "development";

	public TechnicalDocumentDAO() {

	}

	public List<TechnicalDocumentationVO> getTechnicalDocumentList(int productId, String productCode, String group) {

		ArrayList<TechnicalDocumentationVO> documentationListVo = new ArrayList<TechnicalDocumentationVO>();

		TechnicalDocumentationVO documentationVO = null;

		String strQuery = Messages.getString("TechnicalDocumentDAO.Query1")
				+ Messages.getString("TechnicalDocumentDAO.Query2") + Messages.getString("TechnicalDocumentDAO.Query3")
				+ Messages.getString("TechnicalDocumentDAO.Query4") + Messages.getString("TechnicalDocumentDAO.Query5")
				+ Messages.getString("TechnicalDocumentDAO.Query6") + Messages.getString("TechnicalDocumentDAO.Query7")
				+ Messages.getString("TechnicalDocumentDAO.Query8") + Messages.getString("TechnicalDocumentDAO.Query9")
				+ Messages.getString("TechnicalDocumentDAO.Query10")
				+ Messages.getString("TechnicalDocumentDAO.Query11")
				+ Messages.getString("TechnicalDocumentDAO.Query12")
				+ Messages.getString("TechnicalDocumentDAO.Query13") + productCode
				+ Messages.getString("TechnicalDocumentDAO.Query14")
				+ Messages.getString("TechnicalDocumentDAO.Query15") + productCode
				+ Messages.getString("TechnicalDocumentDAO.Query16")
				+ Messages.getString("TechnicalDocumentDAO.Query17") + group
				+ Messages.getString("TechnicalDocumentDAO.Query18")
				+ Messages.getString("TechnicalDocumentDAO.Query19")
				+ Messages.getString("TechnicalDocumentDAO.Query20")
				+ Messages.getString("TechnicalDocumentDAO.Query21")
				+ Messages.getString("TechnicalDocumentDAO.Query22")
				+ Messages.getString("TechnicalDocumentDAO.Query23")
				+ Messages.getString("TechnicalDocumentDAO.Query24")
				+ Messages.getString("TechnicalDocumentDAO.Query25")
				+ Messages.getString("TechnicalDocumentDAO.Query26")
				+ Messages.getString("TechnicalDocumentDAO.Query27")
				+ Messages.getString("TechnicalDocumentDAO.Query28")
				+ Messages.getString("TechnicalDocumentDAO.Query29") + productId + " "
				+ Messages.getString("TechnicalDocumentDAO.Query31")
				+ Messages.getString("TechnicalDocumentDAO.Query32")
				+ Messages.getString("TechnicalDocumentDAO.Query33")
				+ Messages.getString("TechnicalDocumentDAO.Query34")
				+ Messages.getString("TechnicalDocumentDAO.Query35")
				+ Messages.getString("TechnicalDocumentDAO.Query36")
				+ Messages.getString("TechnicalDocumentDAO.Query37")
				+ Messages.getString("TechnicalDocumentDAO.Query38") + productId + " "
				+ Messages.getString("TechnicalDocumentDAO.Query40")
				+ Messages.getString("TechnicalDocumentDAO.Query41")
				+ Messages.getString("TechnicalDocumentDAO.Query42");
		
		try {
			statement = new DBUtil().getStatement(env_dev);
			result = statement.executeQuery(strQuery);
			while (result.next()) {

				documentationVO = new TechnicalDocumentationVO();
				documentationVO.setDocumentID(result.getString(Messages.getString("TechnicalDocumentDAO.Column1")));
				documentationVO.setDocumentSource(result.getString(Messages.getString("TechnicalDocumentDAO.Column2")));
				documentationVO
						.setDocumentFileName(result.getString(Messages.getString("TechnicalDocumentDAO.Column3")));
				documentationVO.setDocumentTitle(result.getString(Messages.getString("TechnicalDocumentDAO.Column4")));
				documentationListVo.add(documentationVO);
			}

		} catch (SQLException e) { // TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBUtil.close(result);
			DBUtil.close(statement);
			DBUtil.connectionClose();
		}

		return documentationListVo;
	}

	public TechnicalDocumentationVO getTechnicalDocument(int technicalDocumentId, String technicalDocumentSource) {

		TechnicalDocumentationVO techDocVo;

		techDocVo = getTechnicalDocumentFromDB2(technicalDocumentId, technicalDocumentSource.charAt(0));

		return techDocVo;
	}

	public TechnicalDocumentationVO getTechnicalDocumentFromDB2(int technicalDocumentId, char docSource) {

		TechnicalDocumentationVO techDocVO = new TechnicalDocumentationVO();
		String strQuery = null;

		if (docSource == 'E') {

			strQuery = Messages.getString("TechnicalDocumentDAO.Query47")
					+ Messages.getString("TechnicalDocumentDAO.Query48")
					+ Messages.getString("TechnicalDocumentDAO.Query49")
					+ Messages.getString("TechnicalDocumentDAO.Query50")
					+ Messages.getString("TechnicalDocumentDAO.Query51")
					+ Messages.getString("TechnicalDocumentDAO.Query52") + technicalDocumentId
					+ Messages.getString("TechnicalDocumentDAO.Query53")
					+ Messages.getString("TechnicalDocumentDAO.Query54")
					+ Messages.getString("TechnicalDocumentDAO.Query55")
					+ Messages.getString("TechnicalDocumentDAO.Query56");
		} else {

			strQuery = Messages.getString("TechnicalDocumentDAO.Query57")
					+ Messages.getString("TechnicalDocumentDAO.Query58")
					+ Messages.getString("TechnicalDocumentDAO.Query59") + technicalDocumentId
					+ Messages.getString("TechnicalDocumentDAO.Query60")
					+ Messages.getString("TechnicalDocumentDAO.Query61");
		}

		try {
			statement = new DBUtil().getStatement(env_dev);
			System.out.println("Query ::::"+strQuery);
			result = statement.executeQuery(strQuery);
			while (result.next()) {
				Blob pdfData = result.getBlob("ORIGINAL_FILE");
				int blobLength = (int) pdfData.length();  
				byte[] blobAsBytes = pdfData.getBytes(1, blobLength);
				techDocVO.setPdfBlob(blobAsBytes);
				techDocVO.setDocumentFileName(result.getString("FILE_NAME"));
			}			
			
		} catch (SQLException e) { // TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBUtil.close(result);
			DBUtil.close(statement);
			DBUtil.connectionClose();
		}
		return techDocVO;

	}
	
/*	private byte[] getByteArrary(Blob pdfData) throws SQLException {
		byte[] pdf = null;
		try {
			pdf = IOUtils.toByteArray(pdfData.getBinaryStream());
		
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return pdf;
	}*/

	public void getTechnicalDocumentFromDB2(int technicalDocumentId) {
		// return getTechnicalDocumentFromDB2(technicalDocumentId, 'E');
	}

}
