/**
 * 
 */
package com.adm.webpart.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.adm.webpart.factory.DBUtil;
import com.adm.webpart.vo.ProductCategoryVO;
import com.adm.webpart.vo.ProductInformationVO;
import com.adm.webpart.vo.ProductNameVO;

/**
 * @author s.satya.yellamelli
 * 
 */
public class ProductInformationDAO {

	private Connection connection;
	private Statement statement;
	private ResultSet result;
	private static String env_dev = "development";

	public ProductInformationDAO() {
	}

	public ProductInformationVO productInformation(int productId,
			int regionId, int langId) {
		return productInformation(productId);
	}

	public ProductInformationVO productInformation(int productID) {
		
		ArrayList<ProductInformationVO> productInformationList = new ArrayList<ProductInformationVO>();
		ProductInformationVO productInformationvo = null;
	

		String query = Messages.getString("ProductInformationDAO.query1")
				+ Messages.getString("ProductInformationDAO.query2")
				+ Messages.getString("ProductInformationDAO.query3")
				+ Messages.getString("ProductInformationDAO.query4")
				+ Messages.getString("ProductInformationDAO.query5")
				+ Messages.getString("ProductInformationDAO.query6")
				+ Messages.getString("ProductInformationDAO.query7")
				+ productID+" "
				+ Messages.getString("ProductInformationDAO.query9");

		try {
			statement = new DBUtil().getStatement(env_dev);
			result = statement.executeQuery(query);
			

			while (result.next()) {
				productInformationvo = new ProductInformationVO();
				productInformationvo.setProductCode(result.getString(Messages
						.getString("ProductInformationDAO.column1")));
				productInformationvo.setProductName(result.getString(Messages
						.getString("ProductInformationDAO.column2")));
				productInformationvo.setProductDescription(result
						.getString(Messages
								.getString("ProductInformationDAO.column3")));
				productInformationvo.setProductGroup(result.getString(Messages
						.getString("ProductInformationDAO.column4")));
				productInformationList.add(productInformationvo);
			}

		} catch (SQLException sqlexe) {
		} finally {
			DBUtil.close(result);
			DBUtil.close(statement);
			DBUtil.connectionClose();
		}
		return productInformationvo;
	}

	public String productToEmailAddresses(int productId) {

		String strQuery = "SELECT PTCN_CONTACT_EMAIL "
				+ "FROM MISDB2A.OPPDPTCN_PRODUCT_DETAIL_CONTACTS "
				+ "WHERE ptcn_prdt_id = " + productId;
		StringBuffer strEmailIDs = new StringBuffer();

		try {
			statement = new DBUtil().getStatement(env_dev);
			result = statement.executeQuery(strQuery);
			int count = 0;
			while (result.next()) {
				if (count > 0) {
					strEmailIDs.append(',');
				}
				strEmailIDs.append(result.getString("PTCN_CONTACT_EMAIL"));
				count++;
			}
		} catch (SQLException sqlexe) {
		} finally {
			DBUtil.close(result);
			DBUtil.close(statement);
			DBUtil.connectionClose();
		}

		return strEmailIDs.toString();
	}

	public List<String> getProductIdList() {
		List<String> products = new ArrayList<String>();
		String strQuery = "SELECT PRDT_ID AS PING_CODE "
				+ "FROM MISDB2A.OPPDPRDT_PRODUCT_DETAIL JOIN "
				+ "MISDB2A.OPPDPRPD_PDPDROOT_PRODUCT_DETAIL ON PRDT_ID = PRPD_PRDT_ID JOIN "
				+ "MISDB2A.PDPDROOT ON PRPD_PRODUCT_ID = PRODUCT_ID "
				+ "WHERE PRDT_EXTERNAL = 'Y' " + "AND PRDT_SAMPLED = 'N' "
				+ "ORDER BY PRDT_NAME";

		try {
			statement = new DBUtil().getStatement(env_dev);
			result = statement.executeQuery(strQuery);

			while (result.next()) {

				products.add(result.getString("PING_CODE"));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBUtil.close(result);
			DBUtil.close(statement);
			DBUtil.connectionClose();
		}

		return products;
	}

	public List<ProductNameVO> getSimilarProducts(int productId, int regionId,
			int langId) {
		return getSimilarProducts(productId);
	}

	public List<ProductNameVO> getSimilarProducts(int productId) {

		List<ProductNameVO> productListVo = new ArrayList<ProductNameVO>();
		ProductNameVO productVO;
		
		
		String strQuery = "SELECT PRDT_ID AS PING_CODE, PRDT_NAME AS PING_DESCRIPTION "
				+ "FROM MISDB2A.OPPDPRDT_PRODUCT_DETAIL JOIN "
				+ "MISDB2A.OPPDPDGR_PRODUCT_DETAIL_GROUPS ON PRDT_ID = PDGR_PRDT_ID "
				+ "WHERE PRDT_EXTERNAL = 'Y' "
				+ "AND PRDT_SAMPLED = 'N' "
				+ "AND PDGR_GROP_ID IN(SELECT PDGR_GROP_ID "
				+ "FROM MISDB2A.OPPDPRDT_PRODUCT_DETAIL JOIN "
				+ "MISDB2A.OPPDPDGR_PRODUCT_DETAIL_GROUPS ON PRDT_ID = PDGR_PRDT_ID "
				+ "WHERE PRDT_EXTERNAL = 'Y' "
				+ "AND PRDT_SAMPLED = 'N' "
				+ "AND PRDT_ID = "
				+ productId
				+ " "
				+ ") "
				+ "AND PRDT_ID != "
				+ productId + " " + "ORDER BY PRDT_NAME";		

		try {
			statement = new DBUtil().getStatement(env_dev);
			result = statement.executeQuery(strQuery);
			

			while (result.next()) {

				productVO = new ProductNameVO();
				productVO.setProductID(result.getString("PING_CODE"));
				productVO.setProductName(result.getString("PING_DESCRIPTION"));
				productListVo.add(productVO);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBUtil.close(result);
			DBUtil.close(statement);
			DBUtil.connectionClose();
		}    	
		return productListVo;
	}
}
